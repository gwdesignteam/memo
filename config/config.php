<?php
ob_start();
session_start();
define('DB_SERVER','localhost');
define('DB_USERNAME','memouser.dev');
define('DB_PASSWORD','memopass123');
define('DB_DATABASE','db_memo');
define('DB_DRIVER', 'mysqli');

//$conn = mysql_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD);
//mysql_select_db(DB_DATABASE,$conn);

include_once("constants.php");
include_once(BASE_PATH."adodb/adodb.inc.php");
include_once(BASE_PATH."classes/DAO.inc.php");
include_once(BASE_PATH."classes/HTTPRequest.inc.php");
include_once(BASE_PATH."adodb/adodb-pager.inc.php");
include_once(BASE_PATH."classes/query.php");
include_once(BASE_PATH."classes/queries.inc.php");
include_once(BASE_PATH."phpmailer/class.phpmailer.php");
include_once(BASE_PATH."classes/functions.php");

error_reporting(E_ALL);
ini_set("display_errors",0);
//Log errors to log file
ini_set('log_errors', 1);
date_default_timezone_set('Asia/Kolkata');
$file = 'errors_log'.DATE('Ymd');
define('ERROR_LOG_PATH', BASE_PATH . '/logs/'.$file);
define('QUERY_LOG_PATH', BASE_PATH . '/logs/'.$file);
//Set the file path to log the errors, this file should be writable
ini_set('error_log', ERROR_LOG_PATH);
error_log('entered');
$global_req = HTTPRequest::getInstance()->get_vars();
$funObj = new functions();
$sessuid = "";
if(isset($_SESSION['userid']))
{
	$sessuid = $_SESSION['userid'];
	$sessutype = $_SESSION['usertype'];
	$sessfbuser = $_SESSION['fbuser'];
}
$admsessuid = "";
if(isset($_SESSION['admuserid']))
{
	$admsessuid = $_SESSION['admuserid'];
}
?>
