<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Cache-control" content="public">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $pageTitle; ?></title>
	<meta name="description" content="<?php if(trim($pageDesc) != "") { $pageDesc; } else { $pageDesc = 'Holiday package'; } echo $pageDesc; ?>">
	<meta name="keywords" content="<?php echo $pageKeyword; ?>">
	<meta name="author" content="<?php echo $pageAuthor; ?>">
    <?php if($pagename == 'package_details'){?>
    <!-- Open Graph meta tags -->
   
    <!---META END-->
    <link rel="shortcut icon" href="<?php echo BASE_URL;?>favicon.ico" type="image/x-icon" />
    <?php }?>    
	 <?php include("top_links.php");
	
	 ?>
    
     
  </head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W2G7Z6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W2G7Z6');</script>
<!-- End Google Tag Manager -->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '404985299676386',//1688602478034089
	  status: true,
	  cookie: true,
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
	 js.async = true;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Navigation -->


<div class="clear"></div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
    <div class="signup" style="width:250px; margin:15px auto;">
        <h1>Login</h1>
        <?php if(isset($global_req['signupdone']) && trim($global_req['signupdone']) != '') { echo "A confirmation email has been sent to ".$global_req['signupdone'].". Click on the activation link in the email to activate your account."; } ?>
		<?php //if(isset($global_req['verifydone']) && $global_req['verifydone'] == 1) { echo "Verification successful, please login."; } ?>
        <?php //if(isset($global_req['verifydone']) && $global_req['verifydone'] == 2) { echo "Verification link is expired."; } ?>
        <div class="fb-login">
            <a href="javascript:;" onClick="fblogin()"><img src="<?php echo BASE_URL;?>images/login-fb-icon.png">Log in with Facebook</a>
        </div>
        <div class="or-text"><span class="text-center">&nbsp;</span></div>
        <form role="form" class="form-signin" action="<?php echo BASE_URL;?>login" method="post">
            <div class="form-group">
                <label class="control-label sr-only" for="inputSuccess5">Hidden label</label>
                <?php
                $inputEmail = $global_req['inputEmail'];
		if(empty($inputEmail) && isset($_COOKIE['remember_me']) && !empty($_COOKIE['remember_me'])) { $inputEmail = $_COOKIE['remember_me']; }
                ?>
                <input type="email" class="form-control" placeholder="Email" name="inputEmail" id="inputEmail" value="<?php echo $inputEmail;?>" required title="Please enter username">
                <span class="form-icon form-control-feedback"><img src="<?php echo BASE_URL;?>images/icon-mail.png"></span>
            </div>
            <div class="form-group">
                <label class="control-label sr-only" for="inputSuccess5">Hidden label</label>
                <input type="password" class="form-control" placeholder="Password" name="inputPassword" id="inputPassword" required>
                <span class="form-icon form-control-feedback"><img src="<?php echo BASE_URL;?>images/icon-password.png"></span>
            </div>
            <div class="links">
                <label>
                    <input type="checkbox" name="remme" value="1" <?php if(isset($_COOKIE['remember_me']) && !empty($_COOKIE['remember_me'])) { echo "checked='checked'"; }?>> Remember me
                </label>
                <a href="<?php echo BASE_URL?>login?forgotpass=1">Forgot Password</a>
            </div>
            <button class="btn btn-primary login-butt" type="submit" name="submit" value="Login">Login</button>
            <div class="register">
                <p>Don't have an account? <a href="<?php echo BASE_URL?>signup">Sign Up</a></p>
            </div>
          </form>
      <div class="clearfix"></div>
	</div>
	</div>
  </div>
</div>
<?php if(isset($global_req['signupdone']) && trim($global_req['signupdone']) != "") { echo "<script>$(document).ready(function(){ $( '#loginform' ).trigger( 'click' ); });</script>"; }?>
<?php if(isset($global_req['verifydone']) && $global_req['verifydone'] == 1) { ?><!--Pop up for message to add details-->
<div class="modal fade bs-example-modal-sm3" data-backdrop="false" data-keyboard="false" id="review_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
		<div class="signup" style="width:94%; border:1px solid rgba(227,0,3,1.00) ; margin:30px auto; padding:20px; border-radius:10px;">
            <h4>Welcome <?php echo $global_req['nm'];?>! Your email has been verified</h4><br>
            <?php if($global_req['tp'] == 1) { ?>We'll be sending you an email shortly requesting information to authenticate your travel agency. If all is good, we'll send you an email confirmation so you can start logging in and increasing your business!<?php } else { ?>You can login now and start saving, comparing and sharing the best holiday package deals!<?php } ?><br><br>For questions, please contact:<br>info@tripdaddy.co
            <div class="clear"></div> 
            <!--<div class="col-md-4"><button class="btn btn-primary login-butt" type="button" name="ok" value="ok" onclick="window.location.href='<?php //echo BASE_URL;?>dashboard'">Ok</button></div>
            <div class="col-md-8">--><button class="btn btn-primary login-butt" type="button" name="goto" value="goto" onclick="window.location.href='<?php echo BASE_URL;?>'">Continue to our website</button><!--</div>-->
            <div class="clearfix"></div>
       </div>
    </div>
  </div> 
</div>
<script>
openpopupvr();
function openpopupvr()
{	
	$(".bs-example-modal-sm3").modal("show"); 
}
</script><?php /*echo "<script>$(document).ready(function(){ $( '#loginform' ).trigger( 'click' ); });</script>";*/ }?>
<?php if(isset($global_req['verifydone']) && $global_req['verifydone'] == 2) { ?><!--Pop up for message to add details-->
<div class="modal fade bs-example-modal-sm3" data-backdrop="false" data-keyboard="false" id="review_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
		<div class="signup" style="width:94%; border:1px solid rgba(227,0,3,1.00) ; margin:30px auto; padding:20px; border-radius:10px;">
            <h4>Oops! Looks like your verification link has expired</h4><br>
            Please click the button below, and we'll send you a new verification email.
            <div class="clear"></div> 
            <!--<div class="col-md-4"><button class="btn btn-primary login-butt" type="button" name="ok" value="ok" onclick="window.location.href='<?php //echo BASE_URL;?>dashboard'">Ok</button></div>
            <div class="col-md-8">--><button class="btn btn-primary login-butt" type="button" name="goto" value="goto" onclick="window.location.href='<?php echo BASE_URL;?>login?reverification=1'">Resend Verification</button><!--</div>-->
            <br><br>For questions, please contact:<br>info@tripdaddy.co
            <div class="clearfix"></div>
       </div>
    </div>
  </div> 
</div>
<script>
openpopupvr();
function openpopupvr()
{	
	$(".bs-example-modal-sm3").modal("show"); 
}
</script><?php /*echo "<script>$(document).ready(function(){ $( '#loginform' ).trigger( 'click' ); });</script>";*/ }?>
<!-- Small modal Sign Up -->
<div class="modal fade bs-example-modal-sm1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
		<div class="signup" style="width:250px; margin:15px auto; padding:0px; border-radius:10px;">
            <h1>Sign Up</h1>
            <div class="fb-login">
                <a href="javascript:;" onClick="fblogin()"><img src="<?php echo BASE_URL;?>images/login-fb-icon.png">Sign up with Facebook</a>
            </div>
            <!--<div class="or-text"><span class="text-center">or</span></div>-->
            <form role="form" class="form-signin" action="<?php echo BASE_URL;?>signup" method="post">
            	<div class="mar-10"><div>&nbsp;</div>
                	<div class="pull-left"><label><input type="radio" name="utype" id="utype2" value="2" title="Please select user type" checked="checked" onChange="changeUtype(2)"> User</label></div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="pull-left ml20"><label><input type="radio" name="utype" id="utype1" value="1" title="Please select user type" onChange="changeUtype(1)"> Travel Agency</label></div>
                </div>
                <div class="form-group">
            	<!--<div class="mar-10" id="namefield">Full Name</div>-->
                <input type="text" placeholder="Full Name" name="full_name" maxlength="50" id="full_name" class="form-control" required title="Please enter full name">
                </div>
                <div class="form-group">
                <!--<div class="mar-10">Email</div>-->
                <input type="email" placeholder="Email" name="inputEmail" id="inputEmail" class="form-control" required title="Please enter username"><span class="form-icon form-control-feedback"><img src="<?php echo BASE_URL;?>images/icon-mail.png"></span>
                </div>
                <div class="form-group">
                <!--<div class="mar-10">Password</div>-->
                <input type="password" placeholder="Password" name="inputPassword" id="inputPassword" maxlength="9" class="form-control" required title="Please enter password"><span class="form-icon form-control-feedback"><img src="<?php echo BASE_URL;?>images/icon-password.png"></span>
                </div>
                <div class="form-group">
                <!--<div class="mar-10">Confirm Password</div>-->
                <input type="password" placeholder="Confirm Password" name="inputcPassword" id="inputcPassword" maxlength="9" class="form-control" required><span class="form-icon form-control-feedback"><img src="<?php echo BASE_URL;?>images/icon-password.png"></span>
                </div>
				<div class="pull-left register"><input type="checkbox" name="upri" id="upri" value="1" title="Please select to agree" checked="checked" required>&nbsp; &nbsp;By signing up you agree to our <a href="<?php echo BASE_URL;?>privacy-policy">Privacy Policy</a> and <a href="<?php echo BASE_URL;?>terms">Term & Conditions</a></div>
                <div class="pull-left register"><input type="checkbox" name="usub" id="usub" value="1" title="Please select to agree" checked="checked">&nbsp; &nbsp;I’d like to receive updates and deals from tripdaddy</a></div>
                <button class="btn btn-primary login-butt" type="submit" name="submit" value="signup">Submit</button>
                <div class="register">
                    <p>Already a tripdaddy member? <a href="<?php echo BASE_URL?>login">Log in</a></p>
                </div>
            </form>
            <div class="clearfix"></div>
       </div>
    </div>
  </div>
</div>
<div class="clear"></div>
