<!-- Bootstrap -->
    <link href="<?php echo BASE_URL;?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL;?>css/styleas.css" rel="stylesheet">
    <!-- Owl Carousel Assets -->
    <link href="<?php echo BASE_URL;?>css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo BASE_URL;?>css/owl.theme.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>fonts.css" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>css/style.css" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>css/custom.css" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>css/star-rating.min.css" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>css/slider.css" type="text/css" media="screen" charset="utf-8" />
     <link rel="stylesheet" href="<?php echo BASE_URL;?>css/class.css" type="text/css" media="screen" charset="utf-8" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--> 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php echo BASE_URL;?>js/jquery-latest.min.js"></script>
    <script>
	if(typeof String.prototype.trim !== 'function') {
	  String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, ''); 
	  }
	}
	</script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo BASE_URL;?>js/bootstrap.min.js"></script>
    <script src="<?php echo BASE_URL;?>js/jquery.fcbkcomplete.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo BASE_URL;?>js/star-rating.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo BASE_URL;?>js/bootstrap-slider.js" type="text/javascript" charset="utf-8"></script>
    <script src='<?php echo BASE_URL;?>js/favourite.js' type='text/javascript' charset="utf-8"></script>
     <script src='<?php echo BASE_URL;?>js/editmemo.js' type='text/javascript' charset="utf-8"></script>
      <script src='<?php echo BASE_URL;?>js/myprofileedit.js' type='text/javascript' charset="utf-8"></script>
       
    
    <script type="application/javascript" src="<?php echo BASE_URL;?>js/functions.js"></script>
    <script type="application/javascript" src="<?php echo BASE_URL;?>js/fb.js"></script>
