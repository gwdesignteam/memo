<?php include_once("user_top_links.php");?><script> $("#mnudash").addClass("sele"); </script>
<!--    Main Concant Start   -->
<div id="" class="mb50 container min-600">
	<div class="row">
		<div class="col-md-6 col-sm-8 commonProImg">
			<div class="logota"><img src="<?php echo $picpath;?>" width="200" height="200" alt="" class="circular"/></div>
            <div class="allDtext">
			<h1 class="mb15 mt0"><?php echo substr($full_name,0,13);?></h1>
			<h6 class="mb15">Member since <?php echo $showdate;?></h6>
			<div class="star-d text-center"><input class="rating-input" id="rating-input" type="number" value="<?php echo $rate_total;?>" data-size="sm" data-readonly="true" /></div>
			<h6 class="text-center"><?php echo $rate_total;?> out of 5.0 stars</h6>
            <div class="star-d text-center"><a href="<?php echo BASE_URL;?>reviews" style="text-decoration:none;"><span>(<?php echo $totrevs;?> Reviews)</span></a></div>
            </div>
		</div> <!--col-md-6 end-->

		<div id="graf" class="col-md-6 col-sm-10 mb15">
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load('visualization', '1.1', {packages: ['line']});
    google.setOnLoadCallback(drawChart);
	
	jQuery(document).ready(function() {
jQuery(window).resize(function() { 
/* code start */
var width1 = $(window).width();
if(width1 > 560){
			width1 = 450;
		}
		else if(width1 > 460){
        	width1 = 350;
		}
		else {
        	width1 = 250;
		}
		drawChart(width1);
/* code end */
}).resize(); // Trigger resize handlers. 
	});
	

    function drawChart(width1) {

      var data = new google.visualization.DataTable();
      data.addColumn('string', '');
      data.addColumn('number', '<?php echo $pktitle[0];?>');
	  <?php if(!empty($pktitle[1])) {?> data.addColumn('number', '<?php echo $pktitle[1];?>'); <?php } ?>
      <?php if(!empty($pktitle[2])) {?> data.addColumn('number', '<?php echo $pktitle[2];?>'); <?php } ?>

      data.addRows([ 
        ['Last Week',  <?php echo $pkviews1[0];?><?php if(!empty($pktitle[1])) { echo ", ".$pkviews1[1]; } if(!empty($pktitle[2])) { echo ", ".$pkviews1[2]; }?>],
        ['Last Month', <?php echo $pkviews2[0];?><?php if(!empty($pktitle[1])) { echo ", ".$pkviews2[1]; } if(!empty($pktitle[2])) { echo ", ".$pkviews2[2]; }?>],
        ['Last Year',  <?php echo $pkviews3[0];?><?php if(!empty($pktitle[1])) { echo ", ".$pkviews3[1]; } if(!empty($pktitle[2])) { echo ", ".$pkviews3[2]; }?>]
      ]);


		
		

      var options = {
        chart: {
          title: ''
        },
		axes: {
          y: {
            0: {label: 'Views'}
          }
        },
vAxis:{direction:1, minValue:0},

		
		legend: { position: 'none', maxLines: 3 },
		//var width1 = $(window).width();
		//if(width1 > 768){
			
			width: width1,
		/*}
		else{
        	width: 250,
		}*/
        height: 280      };

      var chart = new google.charts.Line(document.getElementById('linechart_material'));

      chart.draw(data, options);
    }
	/*google.load('visualization', '1.1', {packages: ['line']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', '');
      data.addColumn('number', '<?php //echo $package_name1;?>');
      data.addColumn('number', '<?php //echo $package_name2;?>');
      data.addColumn('number', '<?php //echo $package_name3;?>');

      data.addRows([ <?php //echo $graph_week;?> ]);

      var options = {
        chart: {
          title: ''
        },
		axes: {
          y: {
            0: {label: 'Views'}
          },
		  x: {
            0: {label: 'Last Week'}
          }
        },
vAxis:{direction:1, minValue:0},

		
		legend: { position: 'none', maxLines: 3 },
        width: 450,
        height: 280      };

      var chart = new google.charts.Line(document.getElementById('linechart_material_week'));

      chart.draw(data, options);
    }
	google.load('visualization', '1.1', {packages: ['line']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', '');
      data.addColumn('number', '<?php //echo $package_name1;?>');
      data.addColumn('number', '<?php //echo $package_name2;?>');
      data.addColumn('number', '<?php //echo $package_name3;?>');

      data.addRows([ <?php //echo $graph_month;?> ]);

      var options = {
        chart: {
          title: ''
        },
		axes: {
          y: {
            0: {label: 'Views'}
          },
		  x: {
            0: {label: 'Last Month'}
          }
        },
vAxis:{direction:1, minValue:0},

		
		legend: { position: 'none', maxLines: 3 },
        width: 450,
        height: 280      };

      var chart = new google.charts.Line(document.getElementById('linechart_material_month'));

      chart.draw(data, options);
    }
	
	google.load('visualization', '1.1', {packages: ['line']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', '');
      data.addColumn('number', '<?php //echo $package_name1;?>');
      data.addColumn('number', '<?php //echo $package_name2;?>');
      data.addColumn('number', '<?php //echo $package_name3;?>');

      data.addRows([ <?php //echo $graph_year;?> ]);

      var options = {
        chart: {
          title: ''
        },
		axes: {
          y: {
            0: {label: 'Views'}
          },
		  x: {
            0: {label: 'Last Year'}
          }
        },
vAxis:{direction:1, minValue:0},

		
		legend: { position: 'none', maxLines: 3 },
        width: 450,
        height: 280      };

      var chart = new google.charts.Line(document.getElementById('linechart_material_year'));

      chart.draw(data, options);
    }*/
  </script>

  <div class="rateGraphBox">
  	<h3 class="text-center mt0">Top 3 Most-viewed Ads</h3>
    <?php
    if(count($pktitle) > 0 && count($pkviews1) > 0)
	{
	?>
  	<div id="linechart_material"></div>
    <div class="col-md-12 mb10 mt15 dashpack">
    	<div class="col-md-4">
        	<?php if(!empty($pktitle[0])){?>
        	<div class="pull-left" style="width: 12px; height: 12px; background: none repeat scroll 0% 0% rgb(66, 133, 244); margin: 4px;"></div>
            <div><?php if(strlen($pktitle[0]) > 20) { echo substr($pktitle[0],0,17)."..."; } else { echo $pktitle[0]; }?></div>
            <?php } ?>
    	</div>
        <div class="col-md-4">
        	<?php if(!empty($pktitle[1])){?>
        	<div style="width:12px; height:12px; background:#DB4437; margin: 4px;" class="pull-left"></div>
       		<div><?php if(strlen($pktitle[1]) > 20) { echo substr($pktitle[1],0,17)."..."; } else { echo $pktitle[1]; }?></div>
            <?php } ?>
    	</div>
    	<div class="col-md-4">
        	<?php if(!empty($pktitle[2])){?>
        	<div style="width:12px; height:12px; background:#F4B400;margin: 4px;" class="pull-left"></div>
            <div><?php if(strlen($pktitle[2]) > 20) { echo substr($pktitle[2],0,17)."..."; } else { echo $pktitle[2]; }?></div>
            <?php } ?>
        </div></div>
    	<div class="clear"></div>
	</div>
    <?php
	}
	else
	{
		echo "<div class='blank-graph'>No package views to display yet</div><div class='clear'></div></div>";
	}
	?>
		</div> <!--col-md-6 end-->
		<div class="mb50 clear"></div>
<div class="col-sm-12">
		<h1 class="text-left pull-left mt10 fullIn620 h1In480">Highlights in the last&nbsp;&nbsp;&nbsp;&nbsp;</h1>
		<button class="btn bor-butt3 pull-left" id="hiid7" type="button" onclick="showHightlights(7,'<?php echo $sessuid;?>','<?php echo BASE_URL;?>')">Week</button>
		<button class="btn bor-butt3 pull-left" id="hiid30" type="button" onclick="showHightlights(30,'<?php echo $sessuid;?>','<?php echo BASE_URL;?>')">Month</button>
		<button class="btn bor-butt3 pull-left" id="hiid365" type="button" onclick="showHightlights(365,'<?php echo $sessuid;?>','<?php echo BASE_URL;?>')">Year</button>
        <script>$(document).ready(function(){ showHightlights(7,'<?php echo $sessuid;?>','<?php echo BASE_URL;?>'); });</script>
		<div class="clear mb25"></div>

		<div class="d-m-text">
			<div class="d-m-text-1st d-m-text1">
				<h4 id="actlst">100</h4>
				<h6>Active Listings</h6>
			</div>

			<div class="d-m-text-2nd d-m-text1">
				<h4 id="inqs">1,318</h4>
				<h6>Enquiries</h6>
			</div>

			<div class="d-m-text-3th d-m-text1">
				<h4 id="favs">300</h4>
				<h6>Favorites</h6>
			</div>

			<div class="d-m-text-4th d-m-text1">
				<h4 id="vws">500</h4>
				<h6>Views</h6>
			</div>

			<div class="d-m-text-5th d-m-text1">
				<h4 id="shrs">1,318</h4>
				<h6>Shares</h6>
			</div>

		</div>

		<h1 class="text-left pull-left">Recent Enquiries</h1>
		<div class="clear"></div>
        <?php
        if(count($selinq) > 0)
		{
		?>
         <div id="no-more-tables">
         <table class="table-striped table-condensed cf table-2">
        <thead class="cf">         
          <tr>
            <th>Enquiry Date</th>
            <th>Interested Ad</th>
            <th>Customer's Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Notes</th>
          </tr>
          </thead>
          <tbody>
          <?php
          	foreach($selinq as $inqrows)
		  	{
				$inqdate = date('d/m/Y',strtotime($inqrows['inqdate']));
				if(strlen($inqrows['packagetitle']) > 20) { $packagetitle = substr($inqrows['packagetitle'],0,17)."..."; } else { $packagetitle = $inqrows['packagetitle']; }
				//$packagetitle = $inqrows['packagetitle'];
				$packagetheme = $inqrows['packagetheme'];
				$userfullname = $inqrows['userfullname'];
				$userphone = $inqrows['userphone'];
				$useremail = $inqrows['useremail'];
				$inqdescr = $inqrows['inqdescr'];
		  ?> 
            <tr>
              <td data-title="Enquiry Date" width="14%"><?php echo $inqdate;?></td>
              <td data-title="Interested Ad" width="17%"><a href="#"><?php echo $packagetitle;?> (<?php echo $packagetheme;?>)</a></td>
              <td data-title="Customer's Name" width="19%"><?php echo $userfullname;?></td>
              <td data-title="Phone" width="12%"><?php echo $userphone;?></td>
              <td data-title="Email" width="16%" class="enqMail"><a href="mailto:<?php echo $useremail;?>" target="_blank"><?php echo $useremail;?></a></td>
              <td data-title="Notes" width="22%"><?php echo $inqdescr;?></td>
            </tr>
           <?php
           }
		   ?>
          </tbody>
          </table>
          </div>
          <div class="clear"></div>
		<a href="<?php echo BASE_URL;?>inquiries" style="text-decoration:none;"><button class="center-block btn bor-butt2 mt25" type="button">See More</button></a>
        <?php
		}
		else
		{
			echo "No records found";
		}
		?>
		<div class="clear mb50"></div>

		<h1 class="text-left pull-left">Recent Reviews</h1>
		<div class="clear"></div>
        <?php
        if(count($selrev) > 0)
		{
		?>
<div id="no-more-tables">
         <table class="table-striped table-condensed cf table-2">
        <thead class="cf">     
          <tr>
            <th>Date Reviewed</th>
            <th>Customer's Name</th>
            <th>Overall Rating</th>
            <th>Review</th>
          </tr>
          <tbody>
          <?php
          	foreach($selrev as $revrows)
		  	{
				$revdate = date('d/m/Y',strtotime($revrows['revdate']));
				$userfullname = $revrows['userfullname'];
				$revrate = $revrows['revrate'];
				$revreview = $revrows['revreview'];
				$revrate = $revrows['revrate'];
		  ?>
           <tr>
              <td data-title="Date Reviewed" width="12%"><?php echo $revdate;?></td>
              <td data-title="Customer's Name" width="12%"><?php echo $userfullname;?></td>
              <td data-title="Overall Rating" width="15%"><input class="rating-input" id="rating-input" type="number" value="<?php echo $revrate;?>" data-size="sm" data-readonly="true" /></td>
              <td data-title="Review" width="24%"><?php echo $revreview;?></td>
            </tr>
          <?php
			}
			?>
          </tbody>
        </thead>
        </table>
        </div>
        <div class="clear"></div>
		<a href="<?php echo BASE_URL;?>reviews" style="text-decoration:none;"><button class="center-block btn bor-butt2 mt25" type="button">See More</button></a>
        <?php
		}
		else
		{
			echo "No records found";
		}
		?>
	</div>
</div>
</div>
 <!--main-content end-->
<!--    Main Concant End -->
<div class="clear"></div>