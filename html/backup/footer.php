<?php /* if($pagename != 'add_package_admin' && $pagename != 'add_package_duration_admin' && $pagename != 'add_package_pricing_admin'  && $pagename != 'add_package_listing_info_admin' && $pagename != 'add_package_photos_admin' && $pagename != 'add_package_inclusion_admin' && $pagename != 'add_package_attraction_admin' && $pagename != 'add_package_summary_admin'){ ?>
<div id="footer">
<div class="container">
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-4">
        <h2 class="text-center">CONTACT US</h2>
        <hr>
        <p class="text-left">Email: <span class="orange"><?php echo $selcntr[0]['contact_email']; ?></span></p>
        <p class="text-left">Call Support: <span class="orange"><?php echo $selcntr[0]['contact_number']; ?></span></p>
        <!--<div class="read-more flRight"><a href="#">Read More</a></div> -->
    </div>

<div class="col-md-4 col-sm-4 col-xs-4">
<h2 class="text-center">QUICK LINKS</h2>
<hr>
<div class="padlr0 col-sm-6 col-xs-12">
    <div class="list-style1">
        <ul>
        	<li><a href="<?php echo BASE_URL.'about';?>">About Us</a></li>
            <li><a href="<?php echo BASE_URL.'contact-us';?>">Contact Us</a></li>
            <li><a href="#">Mobile Site</a></li>
            <li><a href="<?php echo BASE_URL.'faq';?>">FAQ</a></li>
            <li><a href="<?php echo BASE_URL.'press';?>">Press</a></li>
            
            
        </ul>
    </div>
</div>
<div class="padlr0 col-sm-6 col-xs-12">
    <div class="list-style1">
        <ul>
        	<li><a href="https://tripdaddyco.wordpress.com/">Blog</a></li>
            <!--<li><a href="#">Invite Friends</a></li>
            <li><a href="#">tripdaddy picks</a></li>-->
            <li><a href="<?php echo BASE_URL.'jobs';?>">Jobs</a></li>
            <li><a href="<?php echo BASE_URL.'terms';?>" rel="nofollow">Terms & Conditions</a></li>
            <li><a href="<?php echo BASE_URL.'privacy-policy';?>" rel="nofollow">Privacy Policy</a></li>
        </ul>
    </div>
</div>
<!--<div class="col-sm-6">
    <div class="list-style1">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">About us</a></li>
            <li><a href="#">Tours </a></li>
            <li><a href="#">Services </a></li>
            <li><a href="#">Contacts</a></li>
        </ul>
    </div>
</div>-->
</div>
<div class="col-md-4 col-sm-4 col-xs-4">
    <h2 class="text-center">SUBSCRIBE</h2>
    <hr>
    Sign up for our newsletter for special deals, featured holiday packages, and travel inspirations
    <!--<input id="suscriber" name="suscriber" class="text-are" placeholder="Enter Your Email" type="email">
    <div class="read-more flLeft"><a href="javascript:;" onclick="addSubscriber('<?php //echo BASE_URL;?>')">Submit</a></div>-->
    <!-- Begin MailChimp Signup Form -->
        <link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            #mc_embed_signup{background:#434343; clear:left; font:14px Helvetica,Arial,sans-serif; }
			#mc_embed_signup input{color:#363636;}
            /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
               We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
        </style>
        <div id="mc_embed_signup">
        <form action="//tripdaddy.us10.list-manage.com/subscribe/post?u=e090482d97618554a87985066&amp;id=e31f8bbd64" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
            <!--<h2>Subscribe</h2>-->
        <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
        <div class="mc-field-group">
            <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
        </label>
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
        </div>
        <div class="mc-field-group">
            <label for="mce-FNAME">First Name  <span class="asterisk">*</span>
        </label>
            <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
        </div>
        <div class="mc-field-group input-group">
	    <strong>User Type  <span class="asterisk">*</span>
	</strong>
	    <ul><li style="float:left; margin-right:10px;"><input type="radio" value="User" name="UTYPE" id="mce-UTYPE-0" checked='checked'><label for="mce-UTYPE-0">User</label></li>
	<li><input type="radio" value="Travel Agency" name="UTYPE" id="mce-UTYPE-1"><label for="mce-UTYPE-1">Travel Agency</label></li>
	</ul>
	</div>

        <!--<div class="mc-field-group">
            <div class="pull-left"><input type="radio" name="UTYPE" id="mce-UTYPE2" value="User" title="Please select user type" checked="checked"><label for="mce-UTYPE2"> User</label></div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="pull-left ml20"><input type="radio" name="UTYPE" id="mce-UTYPE1" value="Agency" title="Please select user type"><label for="mce-UTYPE1"> Travel Agency</label></div>
                </div>-->
            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;"><input type="text" name="b_e090482d97618554a87985066_e31f8bbd64" tabindex="-1" value=""></div>
            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
            </div>
        </form>
        </div>
        <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='UTYPE';ftypes[3]='radio'}(jQuery));var $mcj = jQuery.noConflict(true);</script>
        <!--End mc_embed_signup-->
    <div class="clear mb10"></div>
    <div id="subscriber_feedback"></div>
</div>
<div class="clear"></div>
<div class="bor-redi3" id="footer2">
    <div class="container" style="text-align:center;">
    	<div class="row fs25 mb10"> 
            FOLLOW US
        </div>
        <div class="row"> 
            <div class="col-md-12">
                <a href="https://www.facebook.com/tripdaddy.co" target="_blank"><img width="75" height="75" alt="Facebook" src="<?php echo BASE_URL.'images/facebook500.png';?>"></a>
                <a href="https://twitter.com/tripdaddyco" target="_blank" class="ml15"><img width="75" height="75" alt="Twitter" src="<?php echo BASE_URL.'images/twitter.png';?>"></a>
                <a href="https://plus.google.com/+Tripdaddyco/about" target="_blank" class="ml15"><img width="75" height="75" alt="gPlus" src="<?php echo BASE_URL.'images/gp.png';?>"></a>
                <a href="https://instagram.com/tripdaddy.co/" target="_blank" class="ml15"><img width="75" height="75" alt="Instagram" src="<?php echo BASE_URL.'images/insta.png';?>"></a>
            </div>
        </div>
    </div><!--footer2 End-->
</div>
<div class="clear"></div>
<div class="col-md-12">
    <div class="copy text-center">&copy; tripdaddy Corp. <?php echo date('Y');?>. All rights reserved.</div>
</div>
</div>
</div>
</div> <!---------------------------Footer End --------------------------------->
<?php } ?>
<!--<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
<script type="text/javascript" src="slick/slick.min.js"></script>-->

<script src="<?php echo BASE_URL;?>js/owl.carousel.js"></script>
    <script>
	$(document).ready(function() {
      $("#owl-demo").owlCarousel({
      nav : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true,
	  autoplay: true,
	  navText : false,
	   items : 1, 
	  loop : true,
	  dots: false
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
      });
	  $("#images-scroller").height("auto");
    });
    $(document).ready(function() {
      $(".owl-demo1").owlCarousel({
      //navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true,
	  autoPlay: true,
	  navText : false,
	  loop : true,
	  dots: false
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
      });
    });
	
	$(document).ready(function() {     
		/*$("#owl-demo-home").owlCarousel({     
		autoplay: true, //Set AutoPlay to 3 seconds 
		autoplayTimeout:5000,    
		items : 3,
		itemsDesktop : [1199,3],
		itemsDesktopSmall : [970,2],
		nav : true,
		navText : false,
		loop : true,
		dots: false
		});*/
		$('#owl-demo-home').owlCarousel({
			loop:true,
			navText : false,
			dots: false,
			margin:10,
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:false
				},
				600:{
					items:2,
					nav:false
				},
				1000:{
					items:3,
					nav:true,
					loop:false
				}
			}
		});
		
		/*$('#owl-demo-home1').owlCarousel({
			loop:true,
			navText : false,
			dots: false,
			margin:10,
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:false
				},
				600:{
					items:2,
					nav:false
				},
				1000:{
					items:3,
					nav:true,
					loop:false
				}
			}
		});*/  
    });
	
	

	$(document).ready(function() {     
		$("#owl-demo-home1").owlCarousel({  
			   
		navText : false,
		loop : true,
		dots: false,
		
		responsive:{
				0:{
					items:1,
					nav:false
				},
				600:{
					items:2,
					nav:false
				},
				1000:{
					items:3,
					nav:true,
					loop:false
				}
			}
		});
    });
	
	$('.rating-input').rating({
              min: 0,
              max: 5,
              step: 1,
              size: 'sm',
			  showCaption: false,
			  showClear: false
           });

        $('.rating-input').on('rating.change', function() {
            //alert($('#rating-input').val());
     });
	 $(document).ready(function() {
		$(".flipacc").click(function() {
			$(".panelacc").slideToggle("fast");
			event.stopPropagation();
		});
	});
	$(document).click(function(event) { 
		if(!$(event.target).closest('.flipacc').length) {
			if($('.panelacc').is(":visible")) {
				$('.panelacc').hide()
			}
		}
	})
	$(document).ready(function() {
		var text_max = 500;
		$('.textareafrm_feedback').html(text_max + ' characters remaining');
	
		$('.textareafrm').keyup(function() {
			var text_length = $('.textareafrm').val().length;
			var text_remaining = text_max - text_length;
	
			$('.textareafrm_feedback').html(text_remaining + ' characters remaining');
		});
			
	});
	/*
(function($) {
    var $window = $(window),
        $mrp = $('#rev_3 .m-r-p div');

    function resize() {
        if ($window.width() < 480) {
            return $mrp.removeClass('pull-left ml100 pull-right');
        }


    }

    $window
        .resize(resize)
        .trigger('resize');
})(jQuery);*/

$(window).load(function() {
var width = $(window).width();
resizeContainer(width);
});

$(window).resize(function() {
var width = $(window).width();
resizeContainer(width);

});

function resizeContainer(width) {
	var header = $('nav div:first');
	var mrp = $('#rev_3 .m-r-p div');
	if (width < 1200) {
		header.removeClass('top-container');
		header.addClass('container');
		//alert('window was resized!');
	} else {
		header.removeClass('container');
		header.addClass('top-container');
	}
	
	if (width < 480) {
           mrp.removeClass('pull-left ml100 pull-right');
    }
}
$(document).ready(function(){
	$('.srpFiterClick').click(function(){
		$('.mobFilters').show();
		$('.srpFiterClick1').show();
		$('.srpFiterClick').hide();
	});	
	$('.srpFiterClick1').click(function(){
		$('.mobFilters').hide();
		$('.srpFiterClick').show();
		$('.srpFiterClick1').hide();
	});
	
	$('.addPkgClick').click(function(){
		$('.mobAddPkgNavigate').show();
		$('.addPkgClick1').show();
		$('.addPkgClick').hide();
	});	
	$('.addPkgClick1').click(function(){
		$('.mobAddPkgNavigate').hide();
		$('.addPkgClick').show();
		$('.addPkgClick1').hide();
	});		
	
	$('.mobSearchIcon').click(function(){
		//$('.mobSearchField').fadeIn();
		$( ".mobSearchField" ).toggle( "slow", function() {
			// Animation complete.
		});
	});
	
	
	$('.jb1').click(function(){
		$( ".hiddenDiv1" ).toggle( "slow", function() {
			// Animation complete.
		});
		$('.hiddenDiv2').hide();
		$('.hiddenDiv3').hide();
		$('.hiddenDiv4').hide();
	});
	$('.jb2').click(function(){
		$( ".hiddenDiv2" ).toggle( "slow", function() {
			// Animation complete.
		});
		$('.hiddenDiv1').hide();
		$('.hiddenDiv3').hide();
		$('.hiddenDiv4').hide();
	});
	$('.jb3').click(function(){
		$( ".hiddenDiv3" ).toggle( "slow", function() {
			// Animation complete.
		});
		$('.hiddenDiv1').hide();
		$('.hiddenDiv2').hide();
		$('.hiddenDiv4').hide();
	});
	$('.jb4').click(function(){
		$( ".hiddenDiv4" ).toggle( "slow", function() {
			// Animation complete.
		});
		$('.hiddenDiv1').hide();
		$('.hiddenDiv2').hide();
		$('.hiddenDiv3').hide();
	});
	
	$('html').click(function() {
		$('.hiddenDiv1').hide();
		$('.hiddenDiv2').hide();
		$('.hiddenDiv3').hide();
		$('.hiddenDiv4').hide();
	 })
	 $('.jb1').click(function(e){
		 e.stopPropagation();
	 });
	 $('.jb2').click(function(e){
		 e.stopPropagation();
	 });
	 $('.jb3').click(function(e){
		 e.stopPropagation();
	 });
	 $('.jb4').click(function(e){
		 e.stopPropagation();
	 });
	
});
function goBack() {
    window.history.back();
}
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
$('#accordion1').on('hidden.bs.collapse', toggleChevron);
$('#accordion1').on('shown.bs.collapse', toggleChevron);

// With JQuery
$("#ex2").slider({});

// Without JQuery
//var slider = new Slider('#ex2', {});
    </script>
 </body>
 <script type="text/javascript">if(typeof wabtn4fg==="undefined"){wabtn4fg=1;h=document.head||document.getElementsByTagName("head")[0],s=document.createElement("script");s.type="text/javascript";s.src="<?php echo BASE_URL;?>whatsapp/dist/whatsapp-button.js";h.appendChild(s);}</script>
</html>
<?php
//mail("mahesh413@gmail.com","test from tripdaddy.co","Test message");
// <script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us10.list-manage.com","uuid":"e090482d97618554a87985066","lid":"e31f8bbd64"}) })</script>

*/
?>
