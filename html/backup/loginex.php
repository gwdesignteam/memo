<div class="container">
	<div class="row topnav2"><ul>&nbsp;</ul></div>
    <div class="row"><br><br>
    <?php
	if(isset($global_req['reverification']) && trim($global_req['reverification']) == 1)
	{
	?>
    <form role="form" class="form-signin col-md-3" action="" method="post">
        <?php echo $error;?>
        <h2 class="form-signin-heading">Resend verification link</h2>
        <label class="sr-only" for="inputEmail">Email address</label>
        <input type="email" autofocus="" required="" placeholder="Email address" class="form-control" id="inputEmail" name="inputEmail" value="<?php echo $inputEmail;?>" /><br />
		<br />
        <button type="submit" name="submit" value="Resend" class="btn btn-lg btn-primary btn-block">Submit</button>
    </form>
    <?php
	}
    else if(isset($global_req['forgotpass']) && trim($global_req['forgotpass']) == 1)
	{
	?>
    <form role="form" class="form-signin col-md-3" action="" method="post">
        <?php echo $error;?>
        <h2 class="form-signin-heading">Forgot Password?</h2>
        <label class="sr-only" for="inputEmail">Email address</label>
        <input type="email" autofocus="" required="" placeholder="Email address" class="form-control" id="inputEmail" name="inputEmail" value="<?php echo $inputEmail;?>" /><br />
		<br />
        <button type="submit" name="submit" value="Forgot" class="btn btn-lg btn-primary btn-block">Submit</button>
    </form>
    <?php
	}
	else if((isset($global_req['forgothash']) && trim($global_req['forgothash']) != "") || $forgoterror == 1)
	{
	?>
    <form role="form" class="form-signin col-md-3" action="" method="post">
        <?php echo $error;?>
        <h2 class="form-signin-heading">Forgot Password?</h2>
        <label class="sr-only" for="inputPassword">Password</label>
        <input type="password" required="" placeholder="Password" class="form-control" id="uinputPassword" name="uinputPassword" /><br />
        <label class="sr-only" for="inputPassword">Confirm Password</label>
        <input type="password" required="" placeholder="Confirm Password" class="form-control" id="cinputPassword" name="cinputPassword" />
		<br />
        <button type="submit" name="submit" value="Forgot1" class="btn btn-lg btn-primary btn-block">Submit</button>
    </form>
    <?php
	}
	else
	{
	?>
    <form role="form" class="form-signin col-md-3" action="" method="post">
        <?php echo $error;?>
        <h2 class="form-signin-heading">Please Login</h2>
        <label class="sr-only" for="inputEmail">Email address</label>
        <input type="email" autofocus="" required="" placeholder="Email address" class="form-control" id="inputEmail" name="inputEmail" value="<?php echo $inputEmail;?>" /><br />
        <label class="sr-only" for="inputPassword">Password</label>
        <input type="password" required="" placeholder="Password" class="form-control" id="inputPassword" name="inputPassword" />
        
            <label>
                <input type="checkbox" name="remme" value="1" <?php if(isset($_COOKIE['remember_me']) && !empty($_COOKIE['remember_me'])) { echo "checked='checked'"; }?>> Remember me
            </label>
        <br />
        <button type="submit" name="submit" value="Login" class="btn btn-lg btn-primary btn-block">Login</button>
        <div class="clear mb10"></div>
        <label class="lable"><a href="<?php echo BASE_URL?>login?forgotpass=1" style="color:#e75c05;">Forgot Password?</a></label>
    </form>
    <div class="col-md-9"><h4><?php $errorinner = $_SESSION['error']; unset($_SESSION['error']); echo $errorinner;?></h4></div>
    <?php
	}
	?>
    </div>
    <div class="row topnav2"><ul>&nbsp;</ul></div>
</div>
