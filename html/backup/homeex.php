<div id="Home" class="container">
    <div class="row">
        <!--search Start -->
        <h1 class="text-center" id="search-box1"><?php echo $selsettings[0]['top_text_big'];?><br/><span><?php echo $selsettings[0]['top_text_small'];?></span></h1>
        <div class="search-box">
            <div class="search-box-in">
            <form action="<?php echo BASE_URL?>search" method="post" name="homefrm">
                <input type="search" name="hmsearch" placeholder="What are you looking for?" value="" />
                <input class="search-butt" name="hmsearchbtn" value="" type="submit" />
            </form>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>

<div id="images-scroller" class="hideIn480" style="height:610px;">
    <div id="owl-demo" class="owl-carousel">
    	<?php
        foreach($selslider as $sliderimg)
		{
			if(file_exists(BASE_PATH."slider/".$sliderimg['slider_image']))
			{
				$sliderpath = BASE_URL.'timthumb/timthumb.php?src='.BASE_URL."slider/".$sliderimg['slider_image'].'&q=70&w=1100';
			}
		?>
        	<div><a href="<?php echo $sliderimg['link_url'] ?>"><img class="img-responsive" src="<?php echo $sliderpath;?>" alt="vacation honeymoon holiday tour package"></a></div>
        <?php
		}
		?>
    </div>
</div>
<div class="clear"></div>

<!---------------------------4 box Start -------------------------------->

<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12">
<div id="text-home-ce">
<h1>BROWSE</h1>
<p class="text-center">Get inspiration for your next dream vacation</p>
</div>
<div class="clear"></div>
</div>
</div>
</div>

<div class="container"> 
<div class="row">
<div class="col-md-12 col-sm-12">
<div id="back-out" class="hideIn768" align="center">
<div class="col-xs-3">
<div id="jb1" class="jb1-justpull-left" onmouseover="document.getElementById('hiddenDiv').style.display = 'block'" onmouseout="document.getElementById('hiddenDiv').style.display ='none'">
  <div class="jb1-img-all"><img src="<?php echo BASE_URL.'timthumb/timthumb.php?src='.BASE_URL.'images/jb1.png&q=70&w=120';?>" width="120" height="120" alt="Region"/></div>
  <h6>Region</h6>
</div>
</div>
<div id="hiddenDiv" class="all-pop-con col-md-12 col-sm-12 col-xs-12" style="display:none;" onmouseover="document.getElementById('hiddenDiv').style.display = 'block'" onmouseout="document.getElementById('hiddenDiv').style.display ='none'">
<a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=africa">
<div class="box-home-l-1-1 box-home1"></div>
<h3>Africa</h3>
</a>

<a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=asia">
<div class="box-home-l-1-1 box-home2"></div>
<h3>Asia</h3>
</a>

<a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=europe">
<div class="box-home-l-1-1 box-home3"></div>
<h3>Europe</h3>
</a>

<a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=north_america">
<div class="box-home-l-1-1 box-home4"></div>
<h3>North America</h3>
</a>

<a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=oceania">
<div class="box-home-l-1-1 box-home5"></div>
<h3>Oceania</h3>
</a>

<a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=south_america">
<div class="box-home-l-1-1 box-home6"></div>
<h3>South America</h3>
</a>


</div>

<!--  1st end  -->
<div class="col-xs-3">
<div id="jb1" onmouseover="document.getElementById('hiddenDiv1').style.display = 'block'" onmouseout="document.getElementById('hiddenDiv1').style.display ='none'">
  <div class="jb1-img-all"><img src="<?php echo BASE_URL.'timthumb/timthumb.php?src='.BASE_URL.'images/jb2.png&q=70&w=120';?>" width="120" height="120" alt="Duration"/></div>
  <h6>Duration</h6>
</div>
</div>
<div id="hiddenDiv1" class="all-pop-con col-md-12 col-sm-12 col-xs-12" style="display:block;" onmouseover="document.getElementById('hiddenDiv1').style.display = 'block'" onmouseout="document.getElementById('hiddenDiv1').style.display ='none'">
<div class="text-center">

<br /><br />
<span id="daymin" class="pull-left">1</span><span class="pull-left mt15" id="daymint">&nbsp;day</span><input type="text" class="span3" value="5" data-slider-min="1" data-slider-max="30" data-slider-step="1" data-slider-value="[1,30]" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide"><span id="daymax" class="ml30">30</span><span class="mt15" id="daymaxt">+&nbsp;days</span>
<button class="btn orange-butt pull-right" type="button" onclick="searchthis(1)">Search</button>
    <script>$('.span3').slider()
     .on('slide', function(ev){
		var opts = (this.value).split(',');
		var daymin = opts[0]+' days';
		var daymax = opts[1]+' Days';
		$("#daymin").html(opts[0]);
		$("#daymax").html(opts[1]);
		});
        $('.span3').slider()
		.on('slideStop', function(ev){
			var opts = (this.value).split(',');
			var daymin = opts[0]+' days';
			var daymax = opts[1]+' Days'
			$("#daymin").html(opts[0]);
			$("#daymax").html(opts[1]);
			if(opts[0] >= 30) { $("#daymint").html('+&nbsp;days'); } else { if(opts[0] <= 1) { $("#daymint").html('&nbsp;day'); } else { $("#daymint").html('&nbsp;days'); } }
			if(opts[1] >= 30) { $("#daymaxt").html('+&nbsp;days'); } else { if(opts[1] <= 1) { $("#daymaxt").html('&nbsp;day'); } else { $("#daymaxt").html('&nbsp;days'); } }
		});
		function searchthis(con)
		{
			if(con == 1)
			{
				var daymin = $("#daymin").html();
				var daymax = $("#daymax").html();
				window.location.href="<?php echo BASE_URL?>search?daymin="+daymin+"&daymax="+daymax;
			}
			if(con == 2)
			{
				var primin = $("#primin").html();
				var primax = $("#primax").html();
				window.location.href="<?php echo BASE_URL?>search?primin="+primin+"&primax="+primax;
			}
		}
		function getIndianRupee(repee)
		{
			var x = repee.toString();
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			return res;
		}
        </script>
        <script>
		$(document).ready(function(){ $("#hiddenDiv1").hide(); $("#hiddenDiv1 .slider").css('width','70%'); });
		</script>

</div>
</div>
<!--   2st end  -->

<div class="col-xs-3">
<div id="jb1" onmouseover="document.getElementById('hiddenDiv2').style.display = 'block'" onmouseout="document.getElementById('hiddenDiv2').style.display ='none'">
  <div class="jb1-img-all"><img src="<?php echo BASE_URL.'timthumb/timthumb.php?src='.BASE_URL.'images/jb3.png&q=70&w=120';?>" width="120" height="120" alt="Price"/></div>
  <h6>Price</h6>
</div>
</div>
<div id="hiddenDiv2" class="all-pop-con col-md-12 col-sm-12 col-xs-12" style="display:block;" onmouseover="document.getElementById('hiddenDiv2').style.display = 'block'" onmouseout="document.getElementById('hiddenDiv2').style.display ='none'">
<div class="text-center">
<br /><br />
<span class="pull-left mt15">&#8377;&nbsp; </span><span id="primin" class="pull-left">1,000</span><input type="text" class="span4" value="5" data-slider-min="1000" data-slider-max="600000" data-slider-step="1" data-slider-value="[1000,600000]" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide"><span class="mt15 ml30">&#8377;&nbsp;<span id="primax">6,00,000</span>
<button class="btn orange-butt pull-right" type="button" onclick="searchthis(2)">Search</button>
    <script>$('.span4').slider()
     .on('slide', function(ev){
		var opts = (this.value).split(',');
		var primin = getIndianRupee(parseInt(opts[0]));
		var primax = getIndianRupee(parseInt(opts[1]));

		$("#primin").html(primin);
		$("#primax").html(primax);
		});
        $('.span4').slider()
		.on('slideStop', function(ev){
			var opts = (this.value).split(',');
			var primin = getIndianRupee(parseInt(opts[0]));
			var primax = getIndianRupee(parseInt(opts[1]));
		});
        </script>
        <script>
		$(document).ready(function(){ $("#hiddenDiv2").hide(); $("#hiddenDiv2 .slider").css('width','70%'); });
		</script>
  </div>
</div>
<!--   3st end   -->

<div class="col-xs-3 ">
<div id="jb4" onmouseover="document.getElementById('hiddenDiv4').style.display = 'block'" onmouseout="document.getElementById('hiddenDiv4').style.display ='none'">
  <div class="jb4-img-all"><img src="<?php echo BASE_URL.'timthumb/timthumb.php?src='.BASE_URL.'images/jb4.png&q=70&w=120';?>" width="120" height="120" alt="Theme"/></div>
  <h6>Theme</h6>
</div>
</div>

<div id="hiddenDiv4" class="all-pop-con col-md-12 col-sm-12 col-xs-12" style="display:none;" onmouseover="document.getElementById('hiddenDiv4').style.display = 'block'" onmouseout="document.getElementById('hiddenDiv4').style.display ='none'">

<a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=2">
<div class="box-home-l-1-1 box-home11"></div>
<h3>Adventure</h3>
</a>

<a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=3">
<div class="box-home-l-1-1 box-home12"></div>
<h3>Beach</h3>
</a>

<a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=4">
<div class="box-home-l-1-1 box-home13"></div>
<h3>Cultural</h3>
</a>

<a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=5">
<div class="box-home-l-1-1 box-home14"></div>
<h3>Family</h3>
</a>

<a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=1">
<div class="box-home-l-1-1 box-home15"></div>
<h3>Romantic</h3>
</a>

<a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=6">
<div class="box-home-l-1-1 box-home16"></div>
<h3>Shopping/Nightlife</h3>
</a>



</div>
<!--   4st end    -->



</div><!--back-out end-->

<div id="back-out" class="showIn768" align="center">
    <div class="col-xs-3 jb1">
    <div id="jb1" class="jb1-justpull-left">
      <div class="jb1-img-all"><img src="<?php echo BASE_URL.'timthumb/timthumb.php?src='.BASE_URL.'images/jb1.png&q=70&w=120';?>" width="120" height="120" alt="Region"/></div>
      <h6>Region</h6>
    </div>
    </div>
    <div id="hiddenDiv" class="all-pop-con col-md-12 col-sm-12 col-xs-12 hiddenDiv1">
    <a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=africa">
    <div class="box-home-l-1-1 box-home1"></div>
    <h3>Africa</h3>
    </a>
    
    <a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=asia">
    <div class="box-home-l-1-1 box-home2"></div>
    <h3>Asia</h3>
    </a>
    
    <a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=europe">
    <div class="box-home-l-1-1 box-home3"></div>
    <h3>Europe</h3>
    </a>
    
    <a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=north_america">
    <div class="box-home-l-1-1 box-home4"></div>
    <h3>North America</h3>
    </a>
    
    <a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=oceania">
    <div class="box-home-l-1-1 box-home5"></div>
    <h3>Oceania</h3>
    </a>
    
    <a class="box-home col-md-2" href="<?php echo BASE_URL?>search?region=south_america">
    <div class="box-home-l-1-1 box-home6"></div>
    <h3>South America</h3>
    </a>
    
    
    </div>
    
    <!--  1st end  -->
    <div class="col-xs-3 jb2">
    <div id="jb1">
      <div class="jb1-img-all"><img src="<?php echo BASE_URL.'timthumb/timthumb.php?src='.BASE_URL.'images/jb2.png&q=70&w=120';?>" width="120" height="120" alt="Duration"/></div>
      <h6>Duration</h6>
    </div>
    </div>
    <div id="hiddenDiv1" class="all-pop-con col-md-12 col-sm-12 col-xs-12 hiddenDiv2">
    <div class="text-center">
    
    <br /><br />
    <span id="daymin" class="pull-left">1</span><span class="pull-left mt15" id="daymint">&nbsp;day</span><input type="text" class="span3" value="5" data-slider-min="1" data-slider-max="30" data-slider-step="1" data-slider-value="[1,30]" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide"><span id="daymax" class="ml30">30</span><span class="mt15" id="daymaxt">+&nbsp;days</span>
    <button class="btn orange-butt pull-right" type="button" onclick="searchthis(1)">Search</button>
        <script>$('.span3').slider()
         .on('slide', function(ev){
            var opts = (this.value).split(',');
            var daymin = opts[0]+' days';
            var daymax = opts[1]+' Days';
            $("#daymin").html(opts[0]);
            $("#daymax").html(opts[1]);
            });
            $('.span3').slider()
            .on('slideStop', function(ev){
                var opts = (this.value).split(',');
                var daymin = opts[0]+' days';
                var daymax = opts[1]+' Days'
                $("#daymin").html(opts[0]);
                $("#daymax").html(opts[1]);
                if(opts[0] >= 30) { $("#daymint").html('+&nbsp;days'); } else { if(opts[0] <= 1) { $("#daymint").html('&nbsp;day'); } else { $("#daymint").html('&nbsp;days'); } }
                if(opts[1] >= 30) { $("#daymaxt").html('+&nbsp;days'); } else { if(opts[1] <= 1) { $("#daymaxt").html('&nbsp;day'); } else { $("#daymaxt").html('&nbsp;days'); } }
            });
            function searchthis(con)
            {
                if(con == 1)
                {
                    var daymin = $("#daymin").html();
                    var daymax = $("#daymax").html();
                    window.location.href="<?php echo BASE_URL?>search?daymin="+daymin+"&daymax="+daymax;
                }
                if(con == 2)
                {
                    var primin = $("#primin").html();
                    var primax = $("#primax").html();
                    window.location.href="<?php echo BASE_URL?>search?primin="+primin+"&primax="+primax;
                }
            }
            function getIndianRupee(repee)
            {
                var x = repee.toString();
                var lastThree = x.substring(x.length-3);
                var otherNumbers = x.substring(0,x.length-3);
                if(otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
                return res;
            }
            </script>
            <script>
            $(document).ready(function(){ $("#hiddenDiv1").hide(); $("#hiddenDiv1 .slider").css('width','70%'); });
            </script>
    
    </div>
    </div>
    <!--   2st end  -->
    
    <div class="col-xs-3 jb3">
    <div id="jb1">
      <div class="jb1-img-all"><img src="<?php echo BASE_URL.'timthumb/timthumb.php?src='.BASE_URL.'images/jb3.png&q=70&w=120';?>" width="120" height="120" alt="Price"/></div>
      <h6>Price</h6>
    </div>
    </div>
    <div id="hiddenDiv2" class="all-pop-con col-md-12 col-sm-12 col-xs-12 hiddenDiv3">
    <div class="text-center">
    <br /><br />
    <span class="pull-left mt15">&#8377;&nbsp; </span><span id="primin" class="pull-left">1,000</span><input type="text" class="span4" value="5" data-slider-min="1000" data-slider-max="600000" data-slider-step="1" data-slider-value="[1000,600000]" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide"><span class="mt15 ml30">&#8377;&nbsp;<span id="primax">6,00,000</span>
    <button class="btn orange-butt pull-right" type="button" onclick="searchthis(2)">Search</button>
        <script>$('.span4').slider()
         .on('slide', function(ev){
            var opts = (this.value).split(',');
            var primin = getIndianRupee(parseInt(opts[0]));
            var primax = getIndianRupee(parseInt(opts[1]));
    
            $("#primin").html(primin);
            $("#primax").html(primax);
            });
            $('.span4').slider()
            .on('slideStop', function(ev){
                var opts = (this.value).split(',');
                var primin = getIndianRupee(parseInt(opts[0]));
                var primax = getIndianRupee(parseInt(opts[1]));
            });
            </script>
            <script>
            $(document).ready(function(){ $("#hiddenDiv2").hide(); $("#hiddenDiv2 .slider").css('width','70%'); });
            </script>
      </div>
    </div>
    <!--   3st end   -->

    <div class="col-xs-3 jb4">
    <div id="jb4">
      <div class="jb4-img-all"><img src="<?php echo BASE_URL.'timthumb/timthumb.php?src='.BASE_URL.'images/jb4.png&q=70&w=120';?>" width="120" height="120" alt="Theme"/></div>
      <h6>Theme</h6>
    </div>
    </div>
    
    <div id="hiddenDiv4" class="all-pop-con col-md-12 col-sm-12 col-xs-12 hiddenDiv4">
    
    <a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=2">
    <div class="box-home-l-1-1 box-home11"></div>
    <h3>Adventure</h3>
    </a>
    
    <a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=3">
    <div class="box-home-l-1-1 box-home12"></div>
    <h3>Beach</h3>
    </a>
    
    <a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=4">
    <div class="box-home-l-1-1 box-home13"></div>
    <h3>Cultural</h3>
    </a>
    
    <a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=5">
    <div class="box-home-l-1-1 box-home14"></div>
    <h3>Family</h3>
    </a>
    
    <a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=1">
    <div class="box-home-l-1-1 box-home15"></div>
    <h3>Romantic</h3>
    </a>
    
    <a class="box-home" href="<?php echo BASE_URL?>search?hmtheme=6">
    <div class="box-home-l-1-1 box-home16"></div>
    <h3>Shopping/Nightlife</h3>
    </a>
    
    
    
    </div>
    <!--   4st end    -->



</div>


<div class="clear"></div>
</div>
</div>
</div>

<!--End all Box-->
<div id="" class="container">
	<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div id="text-home-ce">
<h1>RECENTLY VIEWED</h1>
<p class="text-center">Check out packages others are currently considering<!--Easily review packages you last visited--></p>
</div>
</div>
</div>
</div>

<!--         Main Concant Start   -->
<div id="" class="container">
	<div class="row">
    	<div id="owl-demo-home" class="slick">
        <?php
		if(count($selpackages) > 0)
		{
			foreach($selpackages as $packagerows)
			{
				$destination = $packagerows['destination'];
				//$allDesti = $funObj->getDestinationName($destination);
				$packageid = $packagerows['packid'];
				$status = $packagerows['status'];
				if(strlen($pktitle[1]) > 20) { $package_title = substr($packagerows['package_title'],0,17)."..."; } else { $package_title = $packagerows['package_title']; }
				//$package_title = $packagerows['package_title'];
				/*foreach($allDesti as $packageDesti)
				{
					$country = $packageDesti['country'];
				}*/
				$allCities = $funObj->getPackageCities($packageid);
				$location = "";
				foreach($allCities as $packageCities)
				{
					if(trim($location) != ""){ $location .= ", "; }
					$location .= $packageCities['location'];
					if(strlen($location) > 55) { $location = substr($location,0,55)."..."; break; }
				}
				//if(strlen($location) > 55) { $location = substr($location,0,55)."..."; break; }
				$country = $packagerows['theme'];
				$duration = $packagerows['duration']+1;
				$favourites = $packagerows['favourites'];
				$views = $packagerows['views'];
				$rate_total = $packagerows['rate_total'];
				$inquiries = $packagerows['inquiries'];
				$cost = $packagerows['cost'];
				$discount = $packagerows['discount'];
				$discounted_price = $packagerows['cost'] - ($packagerows['cost'] * $discount / 100);
				$pict = $packagerows['pict'];
				$full_name = $packagerows['full_name'];
				
				$favonclick = '';
				$favimage = 'fe-non';
				if(isset($sessuid) && trim($sessuid) != "")
				{
					if($sessutype == 2)
					{
						$favimage = $funObj->getMyFavorite($packageid,$sessuid,'fe');
						if($favimage == 'fe')
						{
							$favonclick = 'onclick="myfav('.$packageid.','.$sessuid.',\'fe\',0,\''.BASE_URL.'\');"';
						}
						else
						{
							$favonclick = 'onclick="myfav('.$packageid.','.$sessuid.',\'fe\',1,\''.BASE_URL.'\');"';
						}
					}
				}
				else
				{
					$favonclick = 'onclick="checkUserStatus(\'package_details\','.$packageid.',\'\',\''.BASE_URL.'\')"';
				}
				if(file_exists(BASE_PATH."packagepic/".$pict))
				{
					$pkpic = BASE_URL.'timthumb/timthumb.php?src='.BASE_URL."packagepic/".$pict.'&q=70&w=300';
				}
				else
				{
					$pkpic = BASE_URL."images/tileplaceholder.jpg";
				}
				$pkimg = $pkpic;
				?>
				<div class="item slide">
                        <div class="trip-pho"><img class="img-responsive" src="<?php echo $pkimg;?>" alt="honeymoon packages"/>
                            <div class="trip-pho-top">
                                <div class="trip-pho-top-title text-left"><a href="<?php echo BASE_URL.$funObj->getMetaUrl($packageid,'package_details');?>" style="color:inherit; text-decoration:none;"><?php echo $package_title;?></a></div>
                                <a class="trip-pho-heart" href="javascript:;"><img class="flRight favimage<?php echo $packageid;?>" src="<?php echo BASE_URL?>images/<?php echo $favimage;?>.png" id="favimage<?php echo $packageid;?>" <?php echo $favonclick;?> width="25" height="25" alt="Favorite"/></a>
                                <a href="<?php echo BASE_URL.$funObj->getMetaUrl($packageid,'package_details');?>" style="color:#inherit; text-decoration:none;"><div style="height:90%;">
                                    <div class="trip-pho-top-title02"><?php echo $duration;?> Days</div>
                                    <div class="trip-pho-star"><input class="rating-input" id="rating-input" type="number" value="<?php echo $rate_total;?>" data-size="xs" data-readonly="true" /></div>
                                    <div class="trip-pho-star-text" title="<?php echo $full_name;?>"><?php echo substr($full_name,0,21);?></div>
                                    <div class="prize-tag-home"> <div class="prize2-tag-home"><?php if($discount !=0){?>&#8377; <?php echo $funObj->moneyIndian($cost); }?></div> <span>&#8377; <?php echo $funObj->moneyIndian($discounted_price);?></span> </div>
									<div class="view"><?php echo $views;?></div>
                                </div></a>
                            </div>
                        </div><!--/*trip-pho End*/-->
                </div><!--item end-->
				<?php
			}
		}
		?>
        </div><!--owl-demo2 end-->
    </div>
</div>
<div class="clear"></div>
<!--Main Concant End -->

<div class="clear"></div>
<div id="text-home-ce">
    <h1>POPULAR DESTINATIONS</h1>
    <p class="text-center">Discover the most alluring places trending around you</p>
</div>

    <div class="container">
        <div class="row">
        	<?php if(count($selpopular) > 0)
            {
                foreach($selpopular as $popular)
                {
					$pic_type = $popular['pic_type'];
					$w="&w=361&h=334";
					if($pic_type == 1) { $class = 'col-md-4 small-box-im col-sm-4 col-xs-12'; }
					else { $class = 'col-md-8 big-box-im col-sm-8 col-xs-12'; $size = 'width="1000" height="444"'; $w="&w=1000&h=444"; }
					$pic = $popular['pic'];
					$destination = $popular['destination'];
					$link = $popular['link'];
					if(strpos($link, "http://") === false)
					{ $link = BASE_URL."search?hmsearch=".$link; }
					?>
					<div class="<?php echo $class;?>">
                        <a href="<?php echo $link;?>"><img class="img-responsive bor-redi3" src="<?php echo BASE_URL.'timthumb/timthumb.php?src='.BASE_URL.'popularpic/'.$pic.'&q=70'.$w;?>" <?php echo $size;?> alt="honeymoon packages"/>
                        <h3 class="text-center"><?php echo $destination;?></h3></a>
                    </div>
					<?php
				}
			}
			?>

<!---------------------------Row 3 End Concant End --------------------------------->
        </div>
    </div>

<div class="clear"></div>
<!---------------------------pdfy End --------------------------------->
<div class="clear"></div>
