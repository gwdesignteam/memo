<?php
	$path = BASE_URL."list-packages";
?> 
<div class="col-md-3 col-sm-3 col-xs-3 mobAddPkgNavigate">
    
    <div class="Left-list-all">
        <h3>Basics</h3>
        <div class="clear"></div>
        <?php
		$loccls = '';
		$durcls = '';
		$pricls = '';
		$infcls = '';
		$phocls = '';
		$inccls = '';
		$attcls = '';
		$sumcls = '';
		if(isset($_SESSION['lastinsertedstep']) && $_SESSION['lastinsertedstep'] >= 1)
		{
			$durpath = BASE_URL."add-package-duration";
			$loccls = 'class="act"';
			$durcls = 'class="act"';
		}
		else
		{
			$durpath = 'javascript:;';
		}
		if(isset($_SESSION['lastinsertedstep']) && $_SESSION['lastinsertedstep'] >= 2)
		{
			$pripath = BASE_URL."add-package-pricing";
			$pricls = 'class="act"';
		}
		else
		{
			$pripath = 'javascript:;';
		}
		if(isset($_SESSION['lastinsertedstep']) && $_SESSION['lastinsertedstep'] >= 3)
		{
			$pricls = 'class="act"';
			if(isset($_SESSION['infcls']) && $_SESSION['infcls'] == 1)
			{
				$infcls = 'class="act"';
			}
			if(isset($_SESSION['phocls']) && $_SESSION['phocls'] == 1)
			{
				$phocls = 'class="act"';
			}
			if(isset($_SESSION['inccls']) && $_SESSION['inccls'] == 1)
			{
				$inccls = 'class="act"';
			}
			if(isset($_SESSION['attcls']) && $_SESSION['attcls'] == 1)
			{
				$attcls = 'class="act"';
			}
			if(isset($_SESSION['sumcls']) && $_SESSION['sumcls'] == 1)
			{
				$sumcls = 'class="act"';
			}
			$infpath = BASE_URL."add-package-listing-info";
			$phopath = BASE_URL."add-package-photos";
			$incpath = BASE_URL."add-package-inclusion";
			$attpath = BASE_URL."add-package-attraction";
			$sumpath = BASE_URL."add-package-summary";
		}
		else
		{
			$infpath = 'javascript:;';
			$phopath = 'javascript:;';
			$incpath = 'javascript:;';
			$attpath = 'javascript:;';
			$sumpath = 'javascript:;';
		}
		
		if(isset($_SESSION['editpackageid']) && trim($_SESSION['editpackageid']) != "")
		{
			$chk_lnk_qry = 'SELECT id, destination, departure_cities, season, duration, cost, package_title, theme, description, inclusions, attractions FROM tbl_packages WHERE id='.$_SESSION['editpackageid'];//(destination = "" OR departure_cities = "") AND 
			$lnk_chk = $conn->selectSQL($chk_lnk_qry, array());
			//Step 1
			if(count($lnk_chk) > 0) { 
				if($lnk_chk[0]['destination'] != "" && $lnk_chk[0]['departure_cities'] != "")
				{
					$loccls = 'class="act"';
				}
			}
			//Step 2
			if(count($lnk_chk) > 0) { 
				if($lnk_chk[0]['duration'] != "" && $lnk_chk[0]['season'] != "")
				{
					$durcls = 'class="act"';
				}
			}
			//Step 3
			if(count($lnk_chk) > 0) { 
				if($lnk_chk[0]['cost'] != 0)
				{
					$pricls = 'class="act"';
				}
			}
			//Step 4
			if(count($lnk_chk) > 0) { 
				if($lnk_chk[0]['package_title'] != "" && $lnk_chk[0]['theme'] != "" && $lnk_chk[0]['description'] != "")
				{
					$infcls = 'class="act"';
				}
			}
			//Step 5
			$chk_pic_qry = 'SELECT id FROM tbl_pictures WHERE package_id='.$_SESSION['editpackageid'];
			$pic_chk = $conn->selectSQL($chk_pic_qry, array());
			if(count($pic_chk) > 0)
			{
				$phocls = 'class="act"';
			}
			//Step 6
			if(count($lnk_chk) > 0) { 
				if($lnk_chk[0]['inclusions'] != "")
				{
					$inccls = 'class="act"';
				}
			}
			if(count($lnk_chk) > 0) { 
				if($lnk_chk[0]['attractions'] != "")
				{
					$attcls = 'class="act"';
				}
			}
			$durpath = BASE_URL."add-package-duration";
			$pripath = BASE_URL."add-package-pricing";
			$infpath = BASE_URL."add-package-listing-info";
			$phopath = BASE_URL."add-package-photos";
			$incpath = BASE_URL."add-package-inclusion";
			$attpath = BASE_URL."add-package-attraction";
			$sumpath = BASE_URL."add-package-summary";
		}
		?>
        <ul>
            <a href="<?php BASE_URL;?>add-package"><li id="add_pack_loc" <?php echo $loccls;?>>Location</li></a>
            <a href="<?php echo $durpath;?>"><li id="add_pack_dur" <?php echo $durcls;?>>Duration</li></a>
            <a href="<?php echo $pripath;?>"><li id="add_pack_pri" <?php echo $pricls;?>>Pricing</li></a>
        </ul>
    </div>
    
    <div class="Left-list-all">
        <h3>Description</h3>
        <div class="clear"></div>
        <ul>
            <a href="<?php echo $infpath;?>"><li id="add_pack_lis" <?php echo $infcls;?>>Listing Info</li></a>
            <a href="<?php echo $phopath;?>"><li id="add_pack_pho" <?php echo $phocls;?>>Photos</li></a>
            <a href="<?php echo $incpath;?>"><li id="add_pack_inc" <?php echo $inccls;?>>Inclusions</li></a>
            <!--<a href="<?php //echo $attpath;?>"><li id="add_pack_att" <?php //echo $attcls;?>>Attractions</li></a>-->
        </ul>
    </div>
    <div class="clear mb30"></div>
	<a href="<?php echo $sumpath;?>"><button type="button" class="btn bor-butt-green auto">Review & Activate Ad</button></a>
    
</div>
<!--<div><button class="btn btn-lg btn-default pull-left btn-block" type="button" id="add_pack_loc"><a href="">Location</a></button></div>
<div><button class="btn btn-lg btn-default pull-left btn-block" type="button" id="add_pack_dur"><a href="">Duration</a></button></div>
<div><button class="btn btn-lg btn-default pull-left btn-block" type="button" id="add_pack_pri"><a href="">Pricing</a></button></div>
<div><button class="btn btn-lg btn-default pull-left btn-block" type="button" id="add_pack_lis" disabled="disabled"><a href="">Listing Info</a></button></div>
<div><button class="btn btn-lg btn-default pull-left btn-block" type="button" id="add_pack_pho" disabled="disabled"><a href="">Photos</a></button></div>
<div><button class="btn btn-lg btn-default pull-left btn-block" type="button" id="add_pack_inc" disabled="disabled"><a href="">Inclusions</a></button></div>
<div><button class="btn btn-lg btn-default pull-left btn-block" type="button" id="add_pack_att" disabled="disabled"><a href="">Attractions</a></button></div>-->
