<?php include_once("user_top_links.php");?><script> $("#mnuacc").addClass("sele"); </script>
<?php
if($sessutype == 1)
{
	if(count($selagents) <= 0 || empty($address) )
	{
		echo "<script> $(document).ready(function() { openpopup(); }); </script>";
	}
}
?>
<div class="container account">
	
	<?php
		if($sessutype == 1)
		{
			$containersize = 'agentContainer';
			$picsize = 'width="200" height="200"';
			$mt = 'mt38';
			$br = "";
		}
		else
		{
			$containersize = 'userContainer';
			$picsize = 'width="137" height="137"';
			$mt = 'mt20';
			$br = "<br />";
		}
	?>
	<div class="<?php echo $containersize;?> commonProImg">
		<div class="logo-out pull-left">
			<div class="logo-t"><img src="<?php echo $picpath;?>" <?php echo $picsize;?> alt="" class="circular"/></div>
            <form action="" name="propic" id="propic" method="post" enctype="multipart/form-data">
            	<input type="file" name="upld" id="upld" style="display:none" onchange="$('#propic').submit();" />
            </form>
			<div class="logochange ml30" id="logochange"><a href="javascript:;" onclick="uploadpic()">Edit Profile Picture</a></div>
		</div>
		<div class="allitext pull-left <?php echo $mt;?>">
			<h1 class="text-left"><?php echo substr($full_name,0,21);?> <?php echo $br;?><span>Member since <?php echo $showdate;?></span></h1>
            <?php
			if($sessutype == 1)
			{
			?>
			<div class="clear mb15"></div>
			<div class="big-star-n pull-left"><input class="rating-input" id="rating-input" type="number" value="<?php echo $rate_total;?>" data-size="sm" data-readonly="true" /></div>
			<div class="blue pull-left mb50"><a href="<?php echo BASE_URL;?>reviews" style="text-decoration:none;"><?php echo $rate_total;?> stars by <?php echo $total_reviews_by;?> Travellers</a></div>
            <?php
			}
			?>
		</div>
	</div>
</div><!--container End-->
<div class="clear mb50"></div>

<div id="gray">
	<div class="container account">
    	<div class="row" style="padding: 0 30px;">
        	<div id="errorrow"><?php if(isset($error) || trim($error) != ""){ echo $error; } ?></div>
    	</div>
		<div class="bor-bott">
            <div class="col-md-3 col-md-offset-1 mt7 col-sm-4">Login Information</div>
            <form action="#logochange" name="accloginfrm" method="post" class="userLoginFrm">
            
            	 <?php
				if($sessutype == 1)
				{
				?>
                <div class="form-out mb15">
            	<label>Travel Agency Name<span class="mandate">*</span></label>
            	<input class="text-are2" name="username" placeholder="Travel Agency Name" maxlength="50" type="text" value="<?php echo $full_name;?>" required="true" />
                </div>
                <?php
				}
				else
				{
				?>
                <div class="form-out mb15">
            	<label>First Name<span class="mandate">*</span></label>
            	<input class="text-are2" name="username" placeholder="First Name" maxlength="50" type="text" value="<?php echo $full_name;?>" required="true" />
                </div>
                <div class="col-md-offset-8 col-sm-offset-8"></div>
				<div class="clear mb15"></div>
                <div class="col-md-3 col-md-offset-1 col-sm-4"></div>
                <div class="form-out mb15">
                <label>Last Name</label>
            	<input class="text-are2" name="last_name" placeholder="Last Name" maxlength="50" type="text" value="<?php echo $last_name;?>" />
                </div>
                <?php
				}
				?>
            
			<div class="col-md-offset-8 col-sm-offset-8"></div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1 col-sm-4"></div>
			<div class="form-out">
				<label>Email Address<span class="mandate">*</span></label>
				<input class="text-are2" placeholder="test@domain.com" type="email" value="<?php echo $email;?>" required="true" readonly />
				<div class="col-md-offset-8 col-sm-offset-8"></div>
			</div>
			<div class="col-md-offset-8 col-sm-offset-8"></div>
			<div class="clear mb15"></div>
            <div class="row">
            <div class="col-md-3 col-md-offset-7 col-sm-3 col-sm-offset-8"><button class="btn bor-butt5 mt25" type="submit" name="loginfrmsubmit" id="loginfrmsubmit" value="save">Save Changes</button></div>
            </div>
			
            </form>
			<div class="clear mb15"></div>
		</div>

		 <?php
		if($sessutype == 2)
		{
		?>
        <div class="row" style="padding: 0 30px;">
        	<div id="errorrow"><?php if(isset($errorinf) || trim($errorinf) != ""){ echo $errorinf; } ?></div>
    	</div>
        <div class="bor-bott">
        	<form action="#loginfrmsubmit" name="accinfofrm" method="post" class="userContInfo">
            <div class="col-md-3 col-md-offset-1 mt7 col-sm-4">Contact Info</div>
            <!--<div class="form-out">
                <label>Full Name<span class="mandate">*</span></label>
                <input class="text-are2" placeholder="Your Full Name" maxlength="50" type="text" name="full_name" value="<?php //echo $full_name;?>" required="true" />
                <div class="col-md-offset-8"></div>
            </div>
            <div class="clear mb15"></div>
            
            <div class="col-md-3 col-md-offset-1"></div>-->
            <div class="form-out">
            <label>Country</label>
            <select name="country" class="turnintodropdown1" style="width:245px" onchange="countrychange(this.value)" required>
            <option>Select Country</option>
            <?php
			$country_codes_in = '';
			foreach($getCityOptions as $country1)
			{
				if($country == $country1['id']) { $selected = 'selected="selected"'; } else { $selected = ''; }
				echo '<option value="'.$country1['id'].'" '.$selected.'>'.$country1['location'].'</option>';
				$country_codes_in .= '<input type="hidden" id="cnt_'.$country1['id'].'" value="'.$country1['country_code'].'" />';
			}
			?>
            </select>
            <?php echo $country_codes_in;?>
            <script>
			function countrychange(cntryid)
			{
				cntryid = $('#cnt_'+cntryid).val();
				$('#country_code').val(cntryid);
			}
			</script>
            <!--<input class="text-are2" placeholder="Country" type="text" name="country" value="<?php //echo $country;?>" required="true" />-->
            <div class="col-md-offset-8 col-sm-offset-8"></div>
            </div>
            <div class="clear mb15"></div>
            
            <div class="col-md-3 col-md-offset-1 col-sm-4"></div>
            <div class="form-out">
            <label>Phone</label>
            <input class="country_code text-are2" style="width:57px;" placeholder="+91" type="text" name="country_code" id="country_code" value="<?php echo $country_code;?>" readonly />
            <input class="text-are2" style="width:182px;" placeholder="e.g.9811040918" type="number" name="contact" maxlength="16" value="<?php echo $contact;?>" />
            <div class="col-md-offset-8 col-sm-offset-8"></div>
            </div>
            <div class="clear mb15"></div>
            
            <div class="col-md-3 col-md-offset-1 col-sm-4"></div>
            <div class="form-out">
            <label>Email<span class="mandate">*</span></label>
            <input class="text-are2" placeholder="meilinda@tripdaddy.co" type="email" name="email" value="<?php echo $email;?>" required="true" readonly />
            <div class="col-md-offset-8 col-sm-offset-8"></div>
            </div>
            <div class="clear mb15"></div>
            
            <div class="col-md-offset-8 col-sm-offset-8"></div>
            <div class="clear mb15"></div>
            <div class="col-md-3 col-md-offset-7 col-sm-offset-8 col-sm-4"><button class="btn bor-butt5 mt25" type="submit" name="infofrmsubmit" id="infofrmsubmit" value="save">Save Changes</button></div>
            <div class="clear mb15"></div>
            </form>
        </div>
        <?php
		}
		?>
		<div class="row" style="padding: 0 30px;">
        	<div id="errorrow"><?php if(isset($errorp) || trim($errorp) != ""){ echo $errorp; } ?></div>
    	</div>
		<div class="bor-bott">
        	<form action="#loginfrmsubmit" name="passfrm" method="post" class="userPassFrm">
			<div class="col-md-3 col-md-offset-1 mt7 col-sm-4">Change Your Password</div>
			<div class="form-out mb15">
				<label>Old Password<span class="mandate">*</span> </label>
				<input class="text-are2" placeholder="*********" type="password" name="oldpass" required="true">
			</div>
			<div class="col-md-offset-8 col-sm-offset-8"></div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1 col-sm-4"></div>
			<div class="form-out">
				<label>New Password<span class="mandate">*</span></label>
				<input class="text-are2" placeholder="*********" type="password" maxlength="9" name="newpass" required="true">
				<div class="col-md-offset-8 col-sm-offset-8"></div>
			</div>
			<div class="clear mb15"></div>
			<div class="col-md-3 col-md-offset-1 col-sm-4"></div>
			<div class="form-out">
				<label>Confirm Password<span class="mandate">*</span></label>
				<input class="text-are2" placeholder="*********" type="password" maxlength="9" name="confirmpass" required="true">
				<div class="col-md-offset-8 col-sm-offset-8"></div>
			</div>
			<div class="col-md-offset-8 col-sm-offset-8"></div>
			<div class="clear mb15"></div>
			<div class="col-md-3 col-md-offset-7 col-sm-4 col-sm-offset-8"><button class="btn bor-butt5 mt25" type="submit" name="passfrmsubmit" id="passfrmsubmit" value="save">Update Password</button></div>
			<div class="clear mb15"></div>
            </form>
		</div>

		<div class="bor-bott">
			<div class="col-md-3 col-md-offset-1 mt7 col-sm-4">Verifications</div>
			<div class="form-out userVerfication">
				<label>Email Address<span class="mandate">*</span></label>
				<input class="text-are3" placeholder="test@domain.com" type="text" value="<?php echo $email;?>" readonly readonly="readonly" />
				<div class="col-md-offset-8 col-sm-offset-8"></div>
			</div>
			<div class="col-md-offset-8 col-sm-offset-8"></div>
			<div class="clear mb15"></div>
			<div class="col-md-3 col-md-offset-7 col-sm-4 col-sm-offset-8"><div class="btn bor-butt14 mt25" id="verified" style="cursor:default;">Verified</div></div>
			<div class="clear mb15"></div>
		</div>
		<?php
		if($sessutype == 1)
		{
		?>
        <div class="row" style="padding: 0 30px;">
        	<div id="errorrow"><?php if(isset($errorc) || trim($errorc) != ""){ echo $errorc; } ?></div>
    	</div>
		<div class="bor-bott bor-bott2">
        	<form action="#verified" name="contactfrm" method="post" class="userPassFrm">
			<div class="col-md-3 col-md-offset-1 mt7 col-sm-4">Contact Info</div>
			<div class="form-out">
				<label>Website</label>
				<input class="text-are2" placeholder="e.g. http://www.website.com" type="url" name="website" id="website" value="<?php echo $website;?>" />
				<div class="col-md-offset-8 col-sm-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1 col-sm-4"></div>
			<div class="form-out">
				<label>Address<span class="mandate">*</span></label>
				<input class="text-are2" placeholder="e.g. C-35, 1st Floor" type="text" name="address" id="address" value="<?php echo $address;?>" required="true" />
				<div class="col-md-offset-8 col-sm-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1 col-sm-4"></div>
			<div class="form-out">
				<label></label>
				<input class="text-are2" placeholder="e.g. Inner Circle Connaught Place" type="text" name="address2" id="address2" value="<?php echo $address2;?>" required="true" />
				<div class="col-md-offset-8 col-sm-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1 col-sm-4"></div>
			<div class="form-out">
				<label></label>
				<input class="text-are2" placeholder="e.g. Delhi, 110001" type="text" name="city" id="city" value="<?php echo $city;?>" required="true" />
				<div class="col-md-offset-8 col-sm-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1 col-sm-4"></div>
			<div class="form-out">
				<label></label>
				<!--<input class="text-are2" placeholder="e.g. India" type="text" name="country" id="country" value="<?php //echo $country;?>" required="true" />-->
                <select name="country" id="country" class="turnintodropdown1" style="width:245px" required>
                <option value="">Select Country</option>
                <?php
				$country_codes_in = '';
                foreach($getCityOptions as $country1)
                {
                    if($country == $country1['id']) { $selected = 'selected="selected"'; } else { $selected = ''; }
                    echo '<option value="'.$country1['id'].'" '.$selected.'>'.$country1['location'].'</option>';
					$country_codes_in .= '<input type="hidden" id="cnt_'.$country1['id'].'" value="'.$country1['country_code'].'" />';
                }
                ?>
                </select>
                <?php echo $country_codes_in;?>
				<script>
                function countrychange(cntryid,ccid)
                {
                    cntryid = $('#cnt_'+cntryid).val();
                    $('#country_code'+ccid).val(cntryid);
                }
                </script>
				<div class="col-md-offset-8 col-sm-offset-8"></div>
			</div>
			<div class="clear mb25"></div>
			<?php echo $agentdetails;?>
			<!--<div class="col-md-3 col-md-offset-1 mt7">Agent 1</div>
			<div class="form-out">
				<label>Agent 1's Name</label>
				<input class="text-are2" placeholder="Satya Gupta" type="text">
				<div class="col-md-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1"></div>
			<div class="form-out">
				<label>Country</label>
				<input class="text-are2" placeholder="India" type="text">
				<div class="col-md-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1"></div>
			<div class="form-out">
				<label>Phone</label>
				<input class="text-are2" placeholder="+91 1800 2099 100" type="text">
				<div class="col-md-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1"></div>
			<div class="form-out">
				<label>Email</label>
				<input class="text-are2" placeholder="satya@thomascook.in" type="text">
				<div class="col-md-offset-8"></div>
			</div>
			<div class="clear mb25"></div>

			<div class="col-md-3 col-md-offset-1 mt7">Agent 2</div>
			<div class="form-out">
				<label>Agent 2's Name</label>
				<input class="text-are2" placeholder="Aseem Grover" type="text">
				<div class="col-md-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1"><img src="images/dele.png" width="29" height="30" alt=""/> Delete Agent 2</div>
			<div class="form-out">
				<label>Country</label>
				<input class="text-are2" placeholder="India" type="text">
				<div class="col-md-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1"></div>
			<div class="form-out">
				<label>Phone</label>
				<input class="text-are2" placeholder="+91 9811 040 653" type="text">
				<div class="col-md-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1"></div>
			<div class="form-out">
				<label>Email</label>
				<input class="text-are2" placeholder="aseem@thomascook.in" type="text">
				<div class="col-md-offset-8"></div>
			</div>
			<div class="clear mb15"></div>

			<div class="col-md-3 col-md-offset-1"></div>
			<div class="form-out">
				<label><img src="images/pla.png" width="30" height="29" alt=""/> <div style="display:inline-block;">Add Agent</div></label>
			</div>
			<div class="col-md-offset-8"></div>
			<div class="clear mb15"></div>-->
		</div>
			<div class="col-md-offset-8 col-sm-offset-8"></div>
			<div class="clear mb15"></div>
			<div class="col-md-3 col-md-offset-7 col-sm-4 col-sm-offset-8"><button class="btn bor-butt5 mt25" type="submit" name="contactfrmsubmit" id="contactfrmsubmit" value="save">Save Changes</button></div>
            </form>
			<div class="clear mb15"></div>
       <?php
		}
		?>
		
	</div> <!--container end-->
</div>
<!--Pop up for message to add details-->
<div class="modal fade bs-example-modal-sm3" id="review_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
		<div class="signup" style="width:94%; border:1px solid rgba(227,0,3,1.00) ; margin:30px auto; padding:20px; border-radius:10px;">
            Enter mandatory fields (marked *) in your profile account to proceed<!--Please add your basic and required information before start adding packages.<br /><br />Please add at least one agent details then you can add packages.-->
            <button class="btn btn-primary login-butt" data-dismiss="modal" type="button" name="ok" value="ok">Ok</button>
            <div class="clearfix"></div>
       </div>
    </div>
  </div>
</div>
<script>
function openpopup()
{
	$(".bs-example-modal-sm3").modal("show");
}
function uploadpic()
{
	$("#upld").trigger('click');
}
/*$(document).ready(function() {
var email = document.querySelector('input[type="email"]');
email.oninvalid = function(e) {

e.target.setCustomValidity("");

if (!e.target.validity.valid) { e.target.setCustomValidity("Please enter a valid eMail address"); }

};
});*/
</script>