<?php include_once("user_top_links.php");?><script> $("#mnupub").addClass("sele"); </script>
<!---------------------------4 box Start -------------------------------->
<div class="container publicprofile">
<div class="col-md-11 col-md-offset-1 col-sm-12">
	<a class="PrePubProPage" href="<?php echo BASE_URL.$funObj->getMetaUrl($agnid,'agentprofile');?>">Preview Public Profile Page</a>
	<div class="commonProImg">
		<div class="logo-out pull-left">
			<div class="logo-t"><img src="<?php echo $picpath;?>" width="200" height="200" alt="" class="circular"/></div>
            <div class="logochange ml30"><a href="<?php echo BASE_URL;?>myaccount">Edit Profile Picture</a></div>
		</div>
		<div class="allitext pull-left">
			<h1 class="text-left" title="<?php echo $full_name;?>"><?php echo substr($full_name,0,21);?> <span>Member since <?php echo $showdate;?></span><div class="logochange"><a href="<?php echo BASE_URL;?>myaccount">Edit Agency Name</a></div></h1>
			<div class="clear mb15"></div>
			<div class="big-star-n pull-left"><input class="rating-input" id="rating-input" type="number" value="<?php echo $rate_total;?>" data-size="sm" data-readonly="true" /></div>
			<div class="blue pull-left mb25"><a href="<?php echo BASE_URL;?>reviews" style="text-decoration:none;"><?php echo $rate_total;?> out of 5 stars (<?php echo $totrevs;?> reviews)</a></div>
			<div class="clear"></div>
			<div class="pull-left mr15 mb10">
				<div class="add-1"><?php echo $address;?><br/><?php echo $address2;?><br/><?php echo $city;?><br /><?php echo $country[0]['location'];?></div>
				<div class="clear mb10"></div>
				<div class="web-1"><a href="<?php echo $website;?>" target="_blank"><?php echo $website;?></a></div>
			</div>
			<div class="pull-left">
            	<?php
                foreach($selagents as $agnrows)
				{
					$cname = $agnrows['cname'];
					$cemail = $agnrows['cemail'];
					$cphone = $agnrows['cphone'];
					$ccountrycode = $agnrows['ccountrycode'];
				?>
                <div class=""><?php echo $cname;?><br /> <?php echo $ccountrycode.' '.$cphone;?></div>
				<div class="clear mb10"></div>
                <?php
				}
				?>
				<div class="mail-1"><a href="mailto:<?php echo $cemail;?>"><?php echo $cemail;?></a></div>
                <div class="logochange"><a href="<?php echo BASE_URL;?>myaccount">Edit Contact Details</a></div>
			</div>
		</div>
	</div>
    </div><!--/*Col-md-11 end*/-->
</div> <!--container End-->
<div class="clear"></div>
<div class="container">
	<div class="text-center text-cen1"><span>Package Listings (<?php echo count($selpackages);?>)</span></div>
	<?php if(count($selpackages) > 0)
	{
		$i = 0;
		foreach($selpackages as $packagerows)
		{
			$pict = $packagerows['pict'];
			$duration = $packagerows['duration']+1;
			if(strlen($packagerows['package_title']) > 30) { $package_title = substr($packagerows['package_title'],0,27)."..."; } else { $package_title = $packagerows['package_title']; }
			//$package_title = $packagerows['package_title'];
			$cost = $packagerows['cost'];
			$discount = $packagerows['discount'];
			$discounted_price = $packagerows['cost'] - ($packagerows['cost'] * $discount / 100);
			$views = $packagerows['views'];
			$favonclick = '';
			$favimage = 'fe-non';
			if(isset($sessuid) && trim($sessuid) != "" && $sessutype == 2)
			{
				$favimage = $funObj->getMyFavorite($packageid,$sessuid,'fe');
				if($favimage == 'fe')
				{
					$favonclick = 'onclick="myfav('.$packageid.','.$sessuid.',\'fe\',0,\''.BASE_URL.'\')"';
				}
				else
				{
					$favonclick = 'onclick="myfav('.$packageid.','.$sessuid.',\'fe\',1,\''.BASE_URL.'\')"';
				}
			}
	?>
	<div class="col-md-4 col-sm-6 mb30"> 
		<div class="trip-pho"><img class="img-responsive" src="<?php echo BASE_URL;?>packagepic/<?php echo $pict;?>" alt=""/>
			<div class="trip-pho-top">
				<div class="trip-pho-top-title text-left"><?php echo $package_title;?></div>
				<a class="trip-pho-heart" href="javascript:;"><img class="flRight favimage<?php echo $packageid;?>" src="<?php echo BASE_URL;?>images/<?php echo $favimage;?>.png" id="favimage<?php echo $packageid;?>" <?php echo $favonclick;?> width="25" height="25" alt=""/></a>
				<div class="trip-pho-top-title02"><?php echo $duration;?> Days</div>
				<div class="trip-pho-star"><input class="rating-input" id="rating-input" type="number" value="<?php echo $rate_total;?>" data-size="xs" data-readonly="true" /></div>
				<div class="trip-pho-star-text" title="<?php echo $full_name;?>"><?php echo substr($full_name,0,21);?></div>
				<div class="prize-tag-home"> <div class="prize2-tag-home"><?php if($discount !=0){?>&#8377; <?php echo $funObj->moneyIndian($cost); }?></div> <span>&#8377; <?php echo $funObj->moneyIndian($discounted_price);?></span> </div>
				<div class="view"><?php echo $views;?></div>
			</div>
		</div><!--/*trip-pho End*/-->
	</div><!--col-md-4 end-->
    <?php
		$i++;
		if($i >= 6) { break; }
		}
	}
	?>
	<div class="clear"></div>
</div> <!--container End-->
<div class="clear mb50"></div>
<a href="<?php echo BASE_URL;?>list-packages" style="text-decoration:none;"><button class="center-block btn bor-butt2 mt25" type="button">See More</button></a>
<div class="clear"></div>
<div class="container"><div class="text-center text-cen1" id="reviews"><span>Reviews (<?php echo count($selrev);?>)</span></div></div>
<?php
if(count($selrev) > 0)
{
?>
<div class="container">
<div class="col-sm-9 col-sm-offset-3">
	<div class="text-left f-20 displayib"></div> 
	<div class="big-star-n flLeft displayib"><input class="rating-input" id="rating-input" type="number" value="<?php echo $rate_total;?>" data-size="md" data-readonly="true" /></div>
	<div class="text-left f-20 displayib"><a href="<?php echo BASE_URL;?>reviews" style="text-decoration:none;"><?php echo $rate_total;?> out of 5 stars (<?php echo count($selrev);?> reviews)</a></div>
	<button class="btn bor-butt2 displayib" type="button" onclick="$('#revdet').toggle();">Details</button>
</div>
</div>
<div class="clear mb50"></div>
<div class="ml353" id="revdet" style="display:none;">
	<div class="boxt1">
		<div class="mi-star-n auto"><input class="rating-input" id="rating-input" type="number" value="<?php echo $rate_cust;?>" data-size="sm" data-readonly="true" /></div>
		<div class="text-center f-20 black-2 mb10">Customer Service</div>
		<div class="clear mb15"></div>
	</div>
	<div class="boxt1">
		<div class="mi-star-n auto"><input class="rating-input" id="rating-input" type="number" value="<?php echo $rate_service;?>" data-size="sm" data-readonly="true" /></div>
		<div class="text-center f-20 black-2 mb10">Service Efficiency</div>
		<div class="clear mb15"></div>
	</div>
	<div class="boxt1">
		<div class="mi-star-n auto"><input class="rating-input" id="rating-input" type="number" value="<?php echo $rate_recommend;?>" data-size="sm" data-readonly="true" /></div>
		<div class="text-center f-20 black-2 mb10">Recommend or Use Again</div>
		<div class="clear mb15"></div>
	</div>
</div>
<div class="clear mb50"></div>
<div class="container">
	<div class="text-center mb25"><form name="sortfrm" class="sortfrm" id="sortfrm" action="#reviews" method="post">
    	Sort by : <select name="sortby" onchange="$('#sortfrm').submit()">
            <option value="id-desc" <?php if($sortby == 'id-desc') { echo 'selected="selected"';}?>>Most recent</option>
            <option value="rate-desc" <?php if($sortby == 'rate-desc') { echo 'selected="selected"';}?>>Highest rating</option>
            <option value="rate-asc" <?php if($sortby == 'rate-asc') { echo 'selected="selected"';}?>>Lowest rating</option>
        </select>
        </form></div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table-3 newTblFormat">
        	<?php
            foreach($selrev as $revrows)
			{
				$revrate = $revrows['revrate'];
				$revrate_cust = $revrows['revrate_cust'];
				$revrate_service = $revrows['revrate_service'];
				$revrate_recommend = $revrows['revrate_recommend'];
				$revreview = $revrows['revreview'];
				$userfullname = $revrows['userfullname'];
				$usrpic = $revrows['usrpic'];
				$usrfbpic = $revrows['usrfbpic'];
				$revdate = $revrows['revdate'];
				if(trim($usrpic) == "")
				{
					$usrpicpath = BASE_URL."images/default.png";
					if($sessfbuser ==1)
					{
						$picpath = $usrfbpic;
					}
				}
				else
				{
					if(file_exists(BASE_PATH."userpic/".$usrpic))
					{
						$usrpicpath = BASE_URL."userpic/".$usrpic;
					}
					else
					{
						$usrpicpath = BASE_URL."images/default.png";
					}
				}
			?>
          <tr><td width="2%">&nbsp;</td>
          	<td width="10%"><?php echo date('Y-m-d',strtotime($revdate));?></td>
          	<td width="20%">
            <div class="boxt1 boxt2 auto" style="width:100%;"><img src="<?php echo $usrpicpath;?>" width="50" height="50" alt="" class="circular"/><br /><?php echo $userfullname;?>
        
        </div>
        </td>
            <td width="20%">
            <div class="boxt1 auto">
        <div class="mi-star-n auto"><input class="rating-input" id="rating-input" type="number" value="<?php echo $revrate;?>" data-size="sm" data-readonly="true" /></div>
        <div class="text-center f-20 black-2 mb10">Customer Rating</div>
        <div class="clear mb15"></div>
        </div>
        </td>
            <td width="2%">&nbsp;</td>
            <td width="40%" class="boxt2"><?php echo $revreview;?>
            <!--<div class="table-3">
                <div class="boxt1">
                    <div class="mi-star-n auto"><input class="rating-input" id="rating-input" type="number" value="<?php //echo $revrate_cust;?>" data-size="sm" data-readonly="true" /></div>
                    <div class="text-center f-20 black-2 mb10">Customer Service</div>
                    <div class="clear mb15"></div>
                </div>
                
                <div class="boxt1">
                    <div class="mi-star-n auto"><input class="rating-input" id="rating-input" type="number" value="<?php //echo $revrate_service;?>" data-size="sm" data-readonly="true" /></div>
                    <div class="text-center f-20 black-2 mb10">Service Efficiency</div>
                    <div class="clear mb15"></div>
                </div>
                
                <div class="boxt1">
                    <div class="mi-star-n auto"><input class="rating-input" id="rating-input" type="number" value="<?php //echo $revrate_recommend;?>" data-size="sm" data-readonly="true" /></div>
                    <div class="text-center f-20 black-2 mb10">Recommend or Use Again</div>
                    <div class="clear mb15"></div>
                </div>
            </div>-->
            
            </td>
          </tr>
          <?php
			}
		  ?>
        </table>
</div>
<div class="clear mb50"></div>
<?php
}
else
{
	echo "<h4 class='text-center'>No reviews right now</h4>";	
}
?>
<div class="clear"></div>
<!--Main Concant End -->
<div class="clear"></div>