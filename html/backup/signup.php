<div class="container">
	<div class="row topnav2"><ul>&nbsp;</ul></div>
    <div class="row"><br /><br />
    <form role="form" class="form-signin col-md-3" action="" method="post">
        <?php echo $error;?>
        <h2 class="form-signin-heading">Sign Up</h2>
        <div class="mar-10">
                	<div class="pull-left"><input type="radio" name="utype" id="utype2" value="2" title="Please select user type" checked="checked" onChange="changeUtype(2)" <?php if(isset($utype) && $utype == 2) { echo "checked='checked'"; }?>> User</div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="pull-left ml20"><input type="radio" name="utype" id="utype1" value="1" title="Please select user type" onChange="changeUtype(1)" <?php if(isset($utype) && $utype == 1) { echo "checked='checked'"; }?>> Travel Agency</div>
                </div>
        <label class="sr-only" for="full_name" id="namefield1">Travel Agency Name</label>
        <input type="text" autofocus required="true" placeholder="Full Name" class="form-control" id="full_name1" name="full_name" value="<?php echo $full_name;?>" /><br />
        <label class="sr-only" for="inputEmail">Email address</label>
        <input type="email" autofocus required="true" placeholder="Email address" class="form-control" id="inputEmail" name="inputEmail" value="<?php echo $inputEmail;?>" /><br />
        <label class="sr-only" for="inputPassword">Password</label>
        <input type="password" placeholder="Password" class="form-control" id="inputPassword" name="inputPassword" required="true" /><br />
        <label class="sr-only" for="inputcPassword">Confirm Password</label>
        <input type="password" placeholder="Confirm Password" class="form-control" id="inputcPassword" name="inputcPassword" required="true" />
		<div class="pull-left register"><input type="checkbox" name="upri" id="upri" value="1" title="Please select to agree" checked="checked" required="required">&nbsp; &nbsp;By signing up you agree to our <a href="<?php echo BASE_URL;?>privacy-policy">Privacy Policy</a> and <a href="<?php echo BASE_URL;?>terms">Term & Conditions</a></div>
		<div class="pull-left register"><input type="checkbox" name="usub" id="usub" value="1" title="Please select to agree" checked="checked">&nbsp; &nbsp;I’d like to receive updates and deals from tripdaddy</a></div>
	   <!--<div class="checkbox">
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>--><br />
        <button type="submit" name="submit" value="signup" class="btn btn-lg btn-primary btn-block">Submit</button>
    </form>
    <div class="col-md-9"><img src="<?php echo BASE_URL?>images/signupimage.jpg" width="96%" /></div>
    </div>
    <div class="row topnav2"><ul>&nbsp;</ul></div>
</div>
<?php if(isset($utype) && trim($utype) != "") { echo "<script> $(document).ready(function() { changeUtype(".$utype.") }); </script> "; }?>

