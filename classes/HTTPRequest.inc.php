<?php
/**
 *	@Author: Neelkant, modified by shiva
 *	@Usage: $request = HTTPRequest::getInstance()->get_vars();, $request = HTTPRequest::getInstance()->get_vars('post');
 *	@Desc: Returns super globals (GET, POST, REQUEST, COOKIE) based on the name sent, default is request
 *	@Note: Please comment unnecessary functionality your requirement doesn't need it. For now 'request' is only enabled
 */
$conn1 = new DAO();
class HTTPRequest {
	static $_instance;
	private $get, $post, $request, $cookie;

	//Keep the constructor private, not to call this class directly, the only interface should be getInstance
	private function __construct() {
		//Do the necessary data fetching, this is executed only once
		//$this->get = $_GET;
		//$this->post = $_POST;
		$this->request = $_REQUEST;
		//$this->cookie = $_COOKIE;
	}
	private function __clone() {
		//keep it null, so that this object can't be created by cloning
	}
	
	public static function getInstance() {
		if(!(self::$_instance instanceof self)) {
			self::$_instance = new self();	//Constructor should be called only once
		}
		return self::$_instance;
	}
	
	public function get_vars($type = 'request') {
		$conn = $GLOBALS['conn1'];
		$con=mysqli_connect($conn->conn->host,$conn->conn->user,$conn->conn->password,$conn->conn->database);
		switch($type){
			/*case 'get':
						return $this->get;
						break;*/
			/*case 'post':
						return $this->post;
						break;*/
			case 'request':
						$reqVar = $this->request;
						$reqVarReturn = array();
						foreach($reqVar as $key => $val)
						{
							if(is_array($val))
							{
								//$reqVarReturn[$key] = array_map('mysql_real_escape_string', $val);
								//$reqVarReturn[$key] = array_map( "$this->_mysqli->real_escape_string", $val );
								$reqVarReturn[$key] = $val;
							}
							else
							{
								//$reqVarReturn[$key] = mysqli_real_escape_string($con,$val);
								$reqVarReturn[$key] = $val;
							}
						}
						return $reqVarReturn;
						break;
			/*case 'cookie':
						return $this->cookie;
						break;*/
		}
		return false;
	}
	
	public function __destruct(){
		//unset($this->_instance);
		//unset($this->get);
		//unset($this->post);
		unset($this->request);
		//unset($this->cookie);
	}

	/*public function __toString(){
		return serialize($this->request);
	}*/
}

/*
	error_reporting(E_ALL);
	//Test
	$_REQUEST['a'] = 'a';
	$_REQUEST['b'] = 'b';
	$_REQUEST['c'] = 'c';

	//$ip = HTTPRequest::getInstance()->get_vars();
	//print_r($ip);
//*/
?>