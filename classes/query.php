<?php
error_reporting(E_ALL);
require_once("runQuery.php");
class query extends run_query
{
 function query($DBname)
  {
    $this->run_query($DBname);
  }
  
  
 function insert_query($tablename,$valuearray,$fieldarray)
  {
   $countfield = count($fieldarray);
   $countvalue = count($valuearray);
    if($countfield == $countvalue)
    {
      $sql = "insert into `$tablename` (";
      for($i=0;$i<$countfield;$i++)
      {
        if($i == $countfield-1)
         { $sql .= "`".$fieldarray[$i]."`"; }
        else
         { $sql .= "`".$fieldarray[$i]."`,"; }
      }
      $sql .= ") values(";
      for($i=0;$i<$countvalue;$i++)
      {
        if($i == $countvalue-1)
         { $sql .= "'".$valuearray[$i]."'"; }
        else
         {  $sql .= "'".$valuearray[$i]."',"; }
      }
     $sql .= ")";
	// echo $sql;
     $result = $this->insert($sql);
     return $result;
    }
 else
  { return false; }
} // end of insert_query

	
	
	/*
	$TableName = Name of the table (Should not be an array)
	$ValueArray = The value that is to be updated (array)
	$Fieldarray = The field that is to be updated , as per database fields(array)
	$Field = The Field using which the value will be updated (In this case its id) (Not array)
	$id = value  */
	
	function update_query($TableName,$ValueArray,$FieldArray,$Field,$id,$cond="")
	{
	    $CountField = count($FieldArray);
	    $CountValue = count($ValueArray);
		if($CountField == $CountValue)	
		{
		    $sql = "update `$TableName` set ";
			
		    for($fi=0;$fi<$CountField;$fi++)
			{
			    if($fi == $CountField-1)
				{
					$sql .= "`" . $FieldArray[$fi] . "` = ";
					$sql .= "'" . $ValueArray[$fi] . "'";	
				}
				else
				{
					$sql .= "`" . $FieldArray[$fi] . "` = ";
					$sql .= "'" . $ValueArray[$fi] . "',";	
				}
			}
			
		    $sql .= " where `$Field` = '$id' $cond";
			// echo $sql;
			$result = $this->update($sql);
			return $result;
		}
		else
		{
			return false;
		}
	}
	
	
	//delete qery
	
	function delete_query($TableName,$Field,$id)
	{
		if(isset($TableName)){
		
		    $sql = "delete from `$TableName` ";
		    $sql .= " where `$Field` = '$id'";
			//echo $sql;
			$result = $this->delete($sql);
			return $result;
		}
		else
		{
			return false;
		}
	}


}//End of class