<?php
/**
 * @title            QR Code
 * @author           Mahesh Saraf
 */

class QRCode
{

    const API_URL = 'http://chart.apis.google.com/chart?chs=';

    private $_sData;

    /**
     * Constructor.
     */
    public function __construct()
    {
        //$this->_sData = 'BEGIN:VCARD' . "\n";
        //$this->_sData .= 'VERSION:4.0' . "\n";
        return $this;
    }

    /**
     * The supplemental information or a comment that is associated with the vCard.
     *
     * @param string $sText
     * @return object this
     */
    public function content($sText)
    { 
        $this->_sData = $sText;
        return $this;
    }

    /**
     * Generate the QR code.
     *
     * @return object this
     */
    public function finish()
    {
        //$this->_sData .= 'END:VCARD';
        //return $this;
    }

    /**
     * Get the URL of QR Code.
     *
     * @param integer $iSize Default 150
     * @param string $sECLevel Default L
     * @param integer $iMargin Default 1
     * @return string The API URL configure.
     */
    public function get($iSize = 150, $sECLevel = 'L', $iMargin = 1)
    {
        $this->_sData = urlencode($this->_sData);
        return self::API_URL . $iSize . 'x' . $iSize . '&cht=qr&chld=' . $sECLevel . '|' . $iMargin . '&chl=' . $this->_sData;
    }

    /**
     * The HTML code for displaying the QR Code.
     *
     * @return void
     */
    public function display()
    {
        echo '<p class="center"><img src="' . $this->_cleanUrl($this->get()) . '" alt="QR Code" /></p>';
    }

    /**
     * Clean URL.
     *
     * @param string $sUrl
     * @return string
     */
    private function _cleanUrl($sUrl)
    {
        return str_replace('&', '&amp;', $sUrl);
    }

}
