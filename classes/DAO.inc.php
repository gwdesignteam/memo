<?php
	class DAO {
		
		var $conn;
		/**
			 constructor 
		*/
		function __construct() {
			$this->conn = ADONewConnection(DB_DRIVER); 
			$this->conn->debug = false;
			//$this->conn->debug = true;
			$this->conn->connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE) or die ('Cannot make connection!');
			$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		}

		function set_fetch_assoc(){
			$this->conn->SetFetchMode(ADODB_FETCH_ASSOC);
		}

		function getConnection() {
			return $this->conn;
		}
		
		/**
			executeQuery() : generic DAO function to execute the SQL query passed
		*/
		function executeQuery($sql) {
			# execute the query and return the recordSet
			$recordSet = $this->conn->Execute($sql);
			if (!$recordSet)  {
				error_log($sql, 0, QUERY_LOG_PATH);
				error_log('Error: '.$this->conn->ErrorMsg(), 0, QUERY_LOG_PATH);
			} else {
				return $recordSet->GetArray();
			}
		}
		
		
		/**
			executeUpdate() : generic DAO function to execute a DML statement like INSERT or UPDATE
		*/
		function executeUpdate($sql) {
			if ($this->conn->Execute($sql) === false) {
				error_log($sql, 0, QUERY_LOG_PATH);
				error_log('Error: '.$this->conn->ErrorMsg(), 0, QUERY_LOG_PATH);
				return false;
			} else {
				return true;
			}
		}
		
		/** 
			executePrepared() : generic DAO function to execute a prepared SQL statement
			i.e. where parameters are given using question marks (?)
			$parameters is the array of paramters to be passed to the SQL
			
			NOTE: Prepared SQL statements can be INSERTs or UPDATEs only, no SELECTs!
		*/
		function executePrepared($sql, $parameters) {
			# prepare the SQL statement
			$statement = $this->conn->Prepare($sql);

			# execute
			if ($this->conn->Execute($statement, $parameters) === false) {
				error_log($sql, 0, QUERY_LOG_PATH);
				//echo $this->conn->ErrorMsg();
				error_log('Error: '.$this->conn->ErrorMsg(), 0, QUERY_LOG_PATH);
				return false;
			} else {
				return true;
			}
		}

		function selectSQL($sql, $parameters, $pagerows = 0, $pageno = 0, $pageid = 'adodb', $showPageLinks = false, $pagepath = '', $showPagi = 0) {
			# prepare the SQL statement
			$statement = $this->conn->Prepare($sql);

			# execute
			if($pagerows != 0)
			{
				//$recordSet = $this->conn->PageExecute($statement, $pager, $pageno, $parameters);
				$pager = new ADODB_Pager($this->conn,$statement, $pageid, $showPageLinks, $pagepath);
				$pager->htmlSpecialChars = false;
				$pager->linksPerPage = 3;
				$offset = $pagerows*($pageno-1);
				if($showPagi != 0)
				{
					$pagi = $pager->Render($pagerows,$parameters);
					return $pagi; die;
				}
				$recordSet = $this->conn->SelectLimit($statement, $pagerows, $offset, $parameters);//($sql,$nrows=-1,$offset=-1, $inputarr=false,$secs2cache=0)
			}
			else
			{
				$recordSet = $this->conn->Execute($statement, $parameters);
			}
			if (!$recordSet)  {
				error_log($sql, 0, QUERY_LOG_PATH);
				error_log('Error: '.$this->conn->ErrorMsg(), 0, QUERY_LOG_PATH);
			} else {
				return $recordSet->GetArray();
			}

		}
		
		
		/**
		  function getInsertid() to get the last insert id #Malathi#
		*/
		function getInsertid() {
			$insertid = '';
		   	$insertid = $this->conn->Insert_ID();
			if (!$insertid)  {
				error_log('Error: '.$this->conn->ErrorMsg(), 0, QUERY_LOG_PATH);
			} else {
				return $insertid;
				 
			}				
		}

		function Affected_Rows() {
			$rows = $this->conn->Affected_Rows();
			if ($rows === false)  {
				error_log('Error: '.$this->conn->ErrorMsg(), 0, QUERY_LOG_PATH);
			} else {
				return $rows;
				 
			}				
		}
	}
?>
