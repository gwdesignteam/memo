<?php
	global $queries;
	$queries = array(
		0 => 'SELECT User_id from user_info where Session_id = ? LIMIT 1',
		1 => 'SELECT User_id,Email,Passcode,Session_id from user_info where binary(Email)=? AND binary(Passcode)=? LIMIT 1',
		2 => 'SELECT Content_id,Text,Image,Fav_flag from Content_info where User_id  = ? and Flag = ?
    order by Content_id desc ',
		3 => 'SELECT * FROM user_info WHERE binary (Email) =? LIMIT 1',
		4 => 'INSERT INTO user_info (Name, Email,Passcode,User_type,Session_id) VALUES  (?,?,?,?,?)',
		5 => 'INSERT INTO Content_info (User_id,Text,Image) VALUES  (?,?,?)',
		6 => 'UPDATE Content_info  SET Text = ? , Image = ? where Content_id = ?',
		7 => 'UPDATE Content_info  SET Text = ?  where Content_id = ?',
		8 => 'SELECT Content_id,Text,Image from Content_info where Content_id  = ?', 
		9 => 'UPDATE Content_info  SET Flag = ?  where Content_id = ?',
		10 =>'SELECT Content_id,Text,Image from Content_info where Flag = ? order by Content_id desc',
		11 =>'SELECT Image from Content_info where Content_id  = ? and Flag = ?',
		12 =>'DELETE from Content_info where Content_id  = ?',
		13 =>'UPDATE Content_info SET Fav_flag = ?  where Content_id = ?',
		14 =>'UPDATE user_info SET Name = ? where User_id = ?', 
		15 =>'SELECT Name,Email,User_type from user_info where User_id  = ?', 
		16 =>'SELECT User_id,Passcode from user_info where User_id  = ?', 
		17 =>'UPDATE user_info SET Passcode = ? where User_id = ?',
		18 =>'SELECT Email from user_info where binary(Email) =?',
		19 =>'UPDATE user_info SET Passcode = ? where binary(Email)=?', 
		/*Admin queries*/
		1000 => 'SELECT * FROM tbl_admin WHERE username=? AND password=? LIMIT 1',
		1001 => 'SELECT * FROM tbl_users WHERE user_type=?'
		); 
?>
