function delPackage(packageid,BASE_URL)
{
	var confirmdel = confirm("Are you sure, you want to delete this package?");
	if(confirmdel)
	{
		$.ajax({
			url: BASE_URL+"ajax/delPackage.php",
			type: "post",
			data:{ packageid:packageid },
			datatype: 'json',
			success: function(data){
				if($.trim(data) == 1){
					$("#row"+packageid).fadeOut(3000);
				}
				else
				{
					$("#errorrow").html('Error in deleting package, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
}

function confirmAdd(BASE_URL)
{
	$.ajax({
		url: BASE_URL+"ajax/editPackage.php",
		type: "post",
		data:{ removeId:1 },
		datatype: 'json',
		success: function(data){
			location.href=data;
		}
	});
}

function myfav(pid,uid,fev,stat,BASE_URL)
{
	$.ajax({
		url: BASE_URL+"ajax/addFavorite.php",
		type: "post",
		data:{ pid:pid, uid:uid, fev:fev, stat:stat },
		datatype: 'json',
		success: function(data){
			var sv;
			var borcls;
			if(stat == 1) { stat = 0; sv = "Saved"; borcls = 'with-bor'; } else  { stat = 1; sv = "Save"; borcls = 'not-bor'; }
			$(".favimage"+pid).attr("src", data);
			var opts = 'myfav('+pid+','+uid+',"'+fev+'",'+stat+',"'+BASE_URL+'")';
			if(fev == 'fev')
			{
				$("#btnfav"+pid).attr("onclick", opts);
				$("#savetxt"+pid).html(sv);
				if(borcls == 'not-bor')
				{
					$('.with-bor').addClass('not-bor');
					$('.with-bor').removeClass('with-bor');
				}
				else
				{
					$('.not-bor').addClass('with-bor');
					$('.not-bor').removeClass('not-bor');
				}
			}
			else
			{
				$(".favimage"+pid).attr("onclick", opts);
			}
		}
	});
}

function checkUserStatus(pageName,pid,onpid,BASE_URL)
{
	//var pageName = 'package_details';
	//var pid= <?php //echo $pid;?>;
	$.ajax({
		url: BASE_URL+"ajax/checkLogin.php",
		type: "post",
		data:{action:3,pagename:pageName,pid:pid},
		datatype: 'json',
		success: function(data){
			if(data == 2)
			{
				$('.bs-example-modal-sm').modal('show');
			} 
		}
	});
}

function removemyfav(pid,uid,BASE_URL)
{
	$.ajax({
		url: BASE_URL+"ajax/addFavorite.php",
		type: "post",
		data:{ pid:pid, uid:uid, remove:1 },
		datatype: 'json',
		success: function(data){
			$("#favorites-list"+pid).fadeOut(1000);
			$("html, body").animate({ scrollTop: 0 }, 1);
		}
	});
}
function removemyselfav(pid,uid,BASE_URL)
{
	$.ajax({
		url: BASE_URL+"ajax/addFavorite.php",
		type: "post",
		data:{ pid:pid, uid:uid, selremove:1 },
		datatype: 'json',
		success: function(data){
			var pids = pid.split(",");
			var i;
			for (i = 0; i < pids.length; ++i) {
				$("#favorites-list"+pids[i]).fadeOut(300);
			}
			
		}
	});
}

function removemyrev(rid,uid,BASE_URL)
{
	$.ajax({
		url: BASE_URL+"ajax/addReview.php",
		type: "post",
		data:{ rid:rid, uid:uid, remove:1 },
		datatype: 'json',
		success: function(data){
			$("#rev_"+rid).fadeOut(1000);
		}
	});
}

function editPackage(packageid,BASE_URL)
{
	$.ajax({
		url: BASE_URL+"ajax/editPackage.php",
		type: "post",
		data:{ packageid:packageid },
		datatype: 'json',
		success: function(data){
			if($.trim(data) != ""){
				location.href=data;
			}
			else
			{
				$("#errorrow").html('Error in editing package, please try later.');
				$("#errorrow").fadeIn();
			}
		}
	});
}

function copyPackage(packageid,BASE_URL)
{
	$.ajax({
		url: BASE_URL+"ajax/editPackage.php",
		type: "post",
		data:{ packageid:packageid, copypack:1 },
		datatype: 'json',
		success: function(data){
			if($.trim(data) != ""){
				location.href=data;
			}
			else
			{
				$("#errorrow").html('Error in editing package, please try later.');
				$("#errorrow").fadeIn();
			}
		}
	});
}

function addActive(idname,classname)
{
	var idnidname = "#"+idname;
	var idnclassname = "."+classname;
	$(idnclassname).removeClass(classname);
	$(idnidname).addClass(classname);
}

function showActive(idname,classname)
{
	var idnidname = "#"+idname;
	var idnclassname = "."+classname;
	$(idnidname).addClass(classname);
}

function addnights()
{
	var pth = window.location.origin+"/";
	var selectcity = $("#selectcity option:selected").text();
	var selectcityid = $("#selectcity").val();
	var nights = $("#nights").val();
	var contenthtml = $("#appendnights").html();
	var insstring = 'value="'+selectcityid+'|';
	if (contenthtml.indexOf(insstring) >= 0)
	{
		alert('You already added this city');
	}
	else
	{
		insstring = "<div class='butt1' id='div"+selectcityid+"_"+nights+"'><a href='javascript:;' onclick='removeDiv(\"div"+selectcityid+"_"+nights+"\")'><img src='"+pth+"images/x.png' width='15' height='15' /></a> "+nights+"N "+selectcity+"<input type='hidden' name='citynight[]' value='"+selectcityid+"|"+nights+"' /></div>";
		$("#appendnights").append(insstring);
	}
	var adstr = $("#appendnights").html();
	var adcn = adstr.split("<div").length - 1;
	$('#addedcncnt').val(adcn);
}

function selectseason(seasonid)
{
	var season = "#season"+seasonid;
	$(season).toggleClass("butt2");
}

function removeDiv(divid)
{
	divid = "#"+divid;
	$(divid).remove();
	var adstr = $("#appendnights").html();
	var adcn = adstr.split("<div").length - 1;
	$('#addedcncnt').val(adcn);
}

$(document).ready(function(){
	var pth = window.location.origin+"/";
	if(pth == "http://localhost/"){ pth = pth+"tripdaddy/"; }
	$("#selectdesti").fcbkcomplete({
		json_url: pth+"ajax/data.php?fordata=destination",
		//addontab: true,                   
		//maxitems: 10,
		input_min_size: 0,
		height: 10,
		cache: true,
		filter_selected: true,
		filter_begin: true
	});
	$("#selectdepart").fcbkcomplete({
		json_url: pth+"ajax/data.php?fordata=departure",
		//addontab: true,                   
		//maxitems: 10,
		input_min_size: 0,
		height: 10,
		cache: true,
		filter_selected: true,
		filter_begin: true
	});
	$("#selectattr").fcbkcomplete({
		json_url: pth+"ajax/data.php?fordata=attraction",
		//addontab: true,                   
		//maxitems: 10,
		input_min_size: 0,
		height: 10,
		cache: true,
		filter_selected: true,
		filter_begin: true
	});
	$(".holder").css("width","auto");
	$(".contentholder").css("width","auto");
});

$(function() {
    $("#uploadFile").on("change", function()
    {
		$("#imagePreview").html('');
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 		for(var i=0;i<files.length;i++)
		{
			if (/^image/.test( files[i].type)){ // only image file
				var reader = new FileReader(); // instance of the FileReader
				reader.readAsDataURL(files[i]); // read the local file
				reader.onloadend = function(){ // set image data as background of div
					$("#imagePreview").append('<div class="imagePreview glyphicon glyphicon-pencil" id="imagePreview'+i+'" onclick="cropImage(\''+this.result+'\',\''+i+'\')" style="background-image:url('+this.result+')"></div>');
					//$("#imagePreview").css("background-image", "url("+this.result+")");
				}
			}
		}
    });
});

function cropImage(imagePrv,imgid)
{
	alert(imgid);
}

function removeImg(imgname,picid,pid,BASE_URL)
{
	var confirmstat = confirm("Are you sure, you want to delete this image?");
	var imageval = "#pht_"+picid;
	if(confirmstat)
	{
		$.ajax({
			url: BASE_URL+"ajax/delImage.php",
			type: "post",
			data:{ imgname:imgname, pid:pid },
			datatype: 'json',
			success: function(data){
				if($.trim(data) == 1){
					$(imageval).fadeOut(500);
				}
				else
				{
					$("#errorrow").html('Error in chnage status, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
}

function changeStatus(stat,packageid,BASE_URL)
{
	var confirmstat = confirm("Are you sure, you want to change status?");
	var btn = "#btn"+packageid;
	
	if(confirmstat)
	{
		$.ajax({
			url: BASE_URL+"ajax/changePackage.php",
			type: "post",
			data:{ stat:stat, packageid:packageid },
			datatype: 'json',
			success: function(data){
				if($.trim(data) == 1){
					if(stat == 1)
					{
						var btnhtml = '<button onclick="changeStatus(0,'+packageid+',\''+BASE_URL+'\')" class="btn btn-primary act-butt">Active</button>';
						$(btn).html(btnhtml);
					}
					else
					{
						var btnhtml = '<button onclick="changeStatus(1,'+packageid+',\''+BASE_URL+'\')" class="btn btn-primary exp-butt">Inactive</button>';
						$(btn).html(btnhtml);
					}
				}
				else
				{
					$("#errorrow").html('Error in chnage status, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
}

function changeUtype(utype)
{
	if(utype == 1)
	{
		$("#full_name").attr('placeholder','Travel Agency Name');
		$("#full_name1").attr('placeholder','Travel Agency Name');
	}
	else
	{
		$("#full_name").attr('placeholder','Full Name');
		$("#full_name1").attr('placeholder','Full Name');
	}
}
function addagents(currentval,BASE_URL)
{
	var nextval = parseInt(currentval)+1;
	var nextdiv = ".div"+nextval;
	var currdiv = ".div"+currentval;
	var removebut = "#addagents"+currentval;
	var divcont = '<div id="row_'+currentval+'"><div class="col-md-3 col-md-offset-1 mt7 col-sm-4"> Agent '+currentval+'</div><div class="form-out"><label>Agent '+currentval+'\'s Name<span class="mandate">*</span></label><input type="text" class="text-are2" placeholder="Enter name" required="true" id="agentname'+currentval+'" name="agentname[]" value=""><div class="col-md-offset-8"></div></div><div class="clear mb15"></div><div class="col-md-3 col-md-offset-1"><img src="'+BASE_URL+'images/dele.png" width="29" height="30" onclick="delthisagent(\'\','+currentval+',\''+BASE_URL+'\');" class="cursor-hand"/> Delete </div><div class="form-out"><label>Country<span class="mandate">*</span></label><span id="cctlist'+currentval+'"></span><div class="col-md-offset-8"></div></div><div class="clear mb15"></div><div class="col-md-3 col-md-offset-1"></div><div class="form-out"><label>Phone<span class="mandate">*</span></label><input class="country_code text-are2" style="width:60px;" placeholder="+91" type="text" name="agentcountrycode[]" id="country_code'+currentval+'" value="" readonly /><input type="number" class="text-are2" style="width:182px;" placeholder="Enter phone" maxlength="10" required="true" id="agentphone'+currentval+'" name="agentphone[]" value=""><div class="col-md-offset-8"></div></div><div class="clear mb15"></div><div class="col-md-3 col-md-offset-1"></div><div class="form-out"><label>Email<span class="mandate">*</span></label><input type="email" class="text-are2" placeholder="Enter email" required="true" id="agentemail'+currentval+'" name="agentemail[]" value=""><div class="col-md-offset-8"></div></div><div class="clear mb25"></div></div><div id="addagents'+nextval+'"><div class="col-md-3 col-md-offset-1"></div><div class="form-out" onclick="addagents('+nextval+',\''+BASE_URL+'\');"><label class="cursor-hand"><img src="'+BASE_URL+'images/pla.png" width="30" height="29" alt=""/> <div style="display:inline-block;">Add Agent</div></label></div></div><div class="div'+nextval+'"></div>';
	$(removebut).remove();
	$(currdiv).append(divcont);
	$.ajax({
		url: BASE_URL+"ajax/data.php",
		type: "post",
		data:{ fordata:'countrylist', aid:currentval },
		datatype: 'json',
		success: function(datatxt){
			if($.trim(datatxt) != ''){
				$("#cctlist"+currentval).html(datatxt);
			}
		}
	});
}

function delthisagent(aid,cid,BASE_URL)
{
	var confirmdel = 1;//confirm("Are you sure, you want to delete this agent?");
	if(confirmdel)
	{
		if(aid == "")
		{ $("#row_"+cid).fadeOut(3000); setTimeout(function(){ $("#row_"+cid).remove(); }, 2000); }
		else
		{
			$.ajax({
				url: BASE_URL+"ajax/data.php",
				type: "post",
				data:{ fordata:'delagent', aid:aid },
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
						$("#row_"+cid).fadeOut(3000);
						setTimeout(function(){ $("#row_"+cid).remove(); }, 2000);
					}
					else
					{
						$("#errorrow").html('Error in deleting package, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
		}
	}
}

function showHightlights(highlightduration,uid,BASE_URL)
{
	$.ajax({
		url: BASE_URL+"ajax/data.php",
		type: "post",
		data:{ hilightdur:highlightduration, fordata:'highlight', uid:uid },
		datatype: 'json',
		success: function(data){
			if($.trim(data) != ""){
				var retcontent = data.split('~!~');
				$('#actlst').html(retcontent[0]);
				$('#inqs').html(retcontent[1]);
				$('#favs').html(retcontent[2]);
				$('#vws').html(retcontent[3]);
				$('#shrs').html(retcontent[4]);
			}
			else
			{
				$('#actlst').html(0);
				$('#inqs').html(0);
				$('#favs').html(0);
				$('#vws').html(0);
				$('#shrs').html(0);
			}
			var hiid = '#hiid'+highlightduration;
			$('.hiliactive').removeClass('hiliactive');
			$(hiid).addClass('hiliactive');
		}
	});
}

function changeTheVal(cid,cval,ctype,BASE_URL)
{
	if(ctype == "star")
	{
		var star = "#star_"+cid;
		$.ajax({
			url: BASE_URL+"ajax/changeVal.php",
			type: "post",
			data:{ cid:cid, cval:cval,ctype:ctype },
			datatype: 'json',
			success: function(data){
				if($.trim(data) != ""){
					$(star).html(data);
				}
				else
				{
					$("#errorrow").html('Error in starred, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
	if(ctype == "archive")
	{
		var star = "#archive_"+cid;
		$.ajax({
			url: BASE_URL+"ajax/changeVal.php",
			type: "post",
			data:{ cid:cid, cval:cval,ctype:ctype },
			datatype: 'json',
			success: function(data){
				if($.trim(data) != ""){
					$(star).html(data);
				}
				else
				{
					$("#errorrow").html('Error in starred, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
	if(ctype == "contact")
	{
		var star = "#contact_"+cid;
		$.ajax({
			url: BASE_URL+"ajax/changeVal.php",
			type: "post",
			data:{ cid:cid, cval:cval,ctype:ctype },
			datatype: 'json',
			success: function(data){
				if($.trim(data) != ""){
					$(star).html(data);
					if(cval ==1) { $("#tr_"+cid).removeClass('adtobold'); }
					//else { $("#tr_"+cid).addClass('adtobold'); }
				}
				else
				{
					$("#errorrow").html('Error in starred, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
}

function expandDiv(ex, id)
{
	var id1 = "#desc_"+id;
	var id2 = "#desc1_"+id;
	if(ex == 1)
	{
		$(id1).hide();
		$(id2).show();
	}
	else
	{
		$(id1).show();
		$(id2).hide();
	}
}
function expandDetDiv(ex, id)
{
	var id1 = "#det_"+id;
	var id2 = "#det1_"+id;
	if(ex == 1)
	{
		$(id1).hide();
		$(id2).show();
	}
	else
	{
		$(id1).show();
		$(id2).hide();
	}
}
function showReply(rid)
{
	var rid1 = "#btn_"+rid;
	var rid2 = "#btn1_"+rid;
	$(rid1).hide();
	$(rid2).show();
}
function getReply(rid,email,BASE_URL)
{
	var rpl = "#reply_"+rid;
	var rpltxt = $(rpl).val();
	var rplbtn = "#replydiv_"+rid;
	if(rpltxt.trim() == "")
	{
		alert('Please enter reply message');
	}
	else
	{
		$.ajax({
			url: BASE_URL+"ajax/data.php",
			type: "post",
			data:{ fordata:'replyuser',rid:rid,email:email,rpltxt:rpltxt },
			datatype: 'json',
			success: function(data){
				if($.trim(data) == "1"){
					$("#tr_"+rid).removeClass('adtobold');
					$(rplbtn).html('<button type="button" class="center-block btn bor-red cursor-default">Replied</button>');
				}
				else
				{
					$("#errorrow").html('Error in starred, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
}

function addSubscriber(BASE_URL)
{
	var suscriber = "#suscriber";
	var suscriberemail = $(suscriber).val();
	if(suscriberemail.trim() == "")
	{
		$("#subscriber_feedback").html('<span class="dang-err">Please enter email address</span>');
	}
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(suscriberemail)) {
		$.ajax({
			url: BASE_URL+"ajax/data.php",
			type: "post",
			data:{ fordata:'subscribe',email:suscriberemail },
			datatype: 'json',
			success: function(data){
				if($.trim(data) == "1"){
					$("#subscriber_feedback").html('<span class="suc-err">Thank you for subscribing!</span>');
				}
				else
				{
					$("#subscriber_feedback").html('<span class="dang-err">Error in subscription , please try later.</span>');
				}
			}
		});
	}
	else
	{
		$("#subscriber_feedback").html('<span class="dang-err">Please enter correct email address</span>');
	}
}