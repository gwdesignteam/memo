// JavaScript Document

var sc = jQuery.noConflict(true);
sc(document).ready(function() {
	var speed = 'normal';
	var min_size = 0;
	var max_size = 300;

	sc('#sc-top > a').click(function(event) {
		event.preventDefault();

		var clicked = sc(this);
		var myClass = sc(this).attr('class').split(' ')[0];
		//console.log(myClass);

		sc('#sc-top > a').removeClass('active');
		sc(clicked).addClass('active');

		sc('#sc-slider').removeClass()
		sc('#sc-slider').addClass(myClass);

		sc('#sc-middle #sc-slide-wrap > div:visible').fadeOut(speed, function() {
			//sc('#sc-middle .'+myClass).fadeIn(speed);
		});
		sc('#sc-middle .'+myClass).fadeIn(speed);
	});

	var slides = sc('#sc-middle #sc-slide-wrap > div');
	var links = sc('#sc-top > a');
	sc(slides).first().show()
	sc('#right-arrow-wrap').click(function(event) {
		event.preventDefault();

		var currentSlide = sc(slides).filter(':visible');
		var currentLink = sc(links).filter('.active');
		var nextSlide = sc(currentSlide).next().is('div') ? sc(currentSlide).next() : sc(slides).first();
		var nextLink = sc(currentLink).next().is('a') ? sc(currentLink).next() : sc(links).first();
		var myClass = sc(nextLink).attr('class');

		sc('#sc-slider').removeClass();
		sc('#sc-slider').addClass(myClass);
		sc(currentSlide).animate({'right': '-100%'}, speed, function() {
			sc(this).css({'display': '', 'left': '', 'right': ''});
			
		});
		sc(nextSlide).css({display: 'block', right: '100%'}).animate({'right': '0%'}, speed, function() {
			sc(this).css({'right': ''});
		});
		sc(currentLink).removeClass('active');
		sc(nextLink).addClass('active');
	});
	sc('#left-arrow-wrap').click(function(event) {
		event.preventDefault();

		var currentSlide = sc(slides).filter(':visible');
		var currentLink = sc(links).filter('.active');
		var prevSlide = sc(currentSlide).prev().is('div') ? sc(currentSlide).prev() : sc(slides).last();
		var prevLink = sc(currentLink).prev().is('a') ? sc(currentLink).prev() : sc(links).last();
		var myClass = sc(prevLink).attr('class');

		sc('#sc-slider').removeClass();
		sc('#sc-slider').addClass(myClass);
		sc(currentSlide).animate({'left': '-100%'}, speed, function() {
			sc(this).css({'display': '', 'left': '', 'right': ''});
			
		});
		sc(prevSlide).css({display: 'block', left: '100%'}).animate({'left': '0%'}, speed, function() {
			sc(this).css({'left': ''});
		});
		sc(currentLink).removeClass('active');
		sc(prevLink).addClass('active');
	});

	sc('.plus, .minus').disableSelection();
	sc('.plus').click(function(event) {
		event.preventDefault();
		var input = sc(this).parent().find('input');
		var val = parseInt(sc(input).val());
		if (isNaN(val)) {
			val = 0
		}
		val += 1;
		sc(input).val(val);
		sc(input).keyup();
	});
	sc('.minus').click(function(event) {
		event.preventDefault();
		var input = sc(this).parent().find('input');
		var val = parseInt(sc(input).val());
		if (isNaN(val)) {
			val = 0
		}
		val -= 1;
		if (val < 0) {
			val = 0
		}
		sc(input).val(val);
		sc(input).keyup();
	});

	var inputs = sc('input');
	inputs.off();
	inputs.keyup(function(event) {
		event.preventDefault();
		//console.log(event);
		var total = 0;
		sc(inputs).each(function() {

			var num = parseInt( sc(this).val() );
			if (isNaN(num) || num < 0) {
				num = 0;
			}

			if ( sc(this).is(':focus') ) {
				if(event.keyCode == 38) {
					num += 1;
				}
				else if (event.keyCode == 40) {
					num -= 1;
					if (num < 0) {
						num = 0;
					}
				}
				sc(this).val(num);
			}

			var value = sc(this).data('value');
			if (isNaN(value)) {
				value = 0;
			}

			total += ( num * value );

			if ( sc(this).is(':focus') ) {
				//console.log(num);
				//console.log(value);
				//console.log(total);
			}
		});
		total = Math.round( total * 1.15 );

		var size;
		var section;
	    if (total <= 250 && min_size <= 25 && max_size >= 25) {
	       size = '5x5';
	       total /= 2.5;
	       section = 0;
	    }
	    else if (total <= 500 && min_size <= 50 && max_size >= 50) {
	       size = '5x10';
	       total /= 5;
	       section = 0;
	    }
	    else if (total <= 1000 && min_size <= 75 && max_size >= 75) {
	       size = '10x10';
	       total /= 10;
	       section = 1;
	    }
	    else if (total <= 1500 && min_size <= 150 && max_size >= 150) {
	       size = '10x15';
	       total /= 15;
	       section = 1;
	    }
	    else if (total <= 2000 && min_size <= 200 && max_size >= 200) {
	       size = '10x20';
	       total /= 20;
	       section = 2;
	    }
	    else if (total <= 3000 && min_size <= 300 && max_size >= 300) {
	       size = '10x30';
	       total /= 30;
	       section = 2;
	    }
	    else if (total > 3000 || total > max_size * 10) {
	       size = "Call Us!";
	       total = 100;
	       section = 2;
	    }
	    total = Math.round(total);
	    //console.log({'total': total, 'size': size});
	    sc('#sc-bottom #sc-size-fill').css({'height': (100-total)+'px', 'margin-bottom': total+'px'});
	    sc('#sc-bottom #sc-size, #sc-bottom #sc-size-fill').html(size);
	    sc('#sc-bottom #sc-button').attr('data-section', section);
	});
});