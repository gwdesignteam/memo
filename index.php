<?php
include_once("config/config.php");
if(isset($global_req['pagename']))
{
	$pagename = $global_req['pagename'];
}
else
{
	$pagename = "home";
}
include_once(BASE_PATH."php/header.php");
include_once(BASE_PATH."html/header.php");
$exists = 0;
$phppage = BASE_PATH."php/".$pagename.".php";
if(file_exists($phppage))
{
	include_once($phppage);
	$exists = 1;
}
$htmlpage = BASE_PATH."html/".$pagename.".php";
if(file_exists($htmlpage))
{
	include_once($htmlpage);
	$exists = 1;
}
if($exists == 0)
{
	$htmlpage = BASE_PATH."html/error.php";
	include_once($htmlpage);
}
include_once(BASE_PATH."php/footer.php");
include_once(BASE_PATH."html/footer.php");
?>
