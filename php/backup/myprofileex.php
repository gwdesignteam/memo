<?php
if(!isset($sessuid) || trim($sessuid) == "")
{
	$path = BASE_URL."login";
	header("Location:".$path);
}
if($sessutype == 1)
{
	$agnid = $sessuid;//$global_req['agnid'];
	$usql = $queries[1006];
	$selusers = $conn->selectSQL($usql, array($agnid));
	$full_name = $selusers[0]['full_name'];
	$pic = $selusers[0]['pic'];
	if(trim($pic) == "")
	{
		$picpath = BASE_URL."images/default.png";
	}
	else
	{
		if(file_exists(BASE_PATH."userpic/".$pic))
		{
			$picpath = BASE_URL."userpic/".$pic;
		}
		else
		{
			$picpath = BASE_URL."images/default.png";
		}
	}
	$added_date = $selusers[0]['added_date'];
	$showdate = date('M Y',strtotime($added_date));
	$rate_total = $selusers[0]['rate_total'];
	$rate_cust = $selusers[0]['rate_cust'];
	$rate_service = $selusers[0]['rate_service'];
	$rate_recommend = $selusers[0]['rate_recommend'];
	$address = $selusers[0]['address'];
	$address2 = $selusers[0]['address2'];
	$city = $selusers[0]['city'];
	$country = $funObj->getDestinationName($selusers[0]['country']);
	$website = $selusers[0]['website'];
	$email = $selusers[0]['email'];
	
	$agn_sql = $queries[1030];
	$selagents = $conn->selectSQL($agn_sql, array($agnid));
	
	$sel_qry = $queries[48];
	$selpackages = $conn->selectSQL($sel_qry, array($agnid));

	if(isset($global_req['sortby']) && trim($global_req['sortby']) != "")
		{
			$sortby = $global_req['sortby'];
			$sortbyorder = explode("-",$global_req['sortby']);
			$orderby = "ORDER BY rev.".$sortbyorder[0]." ".$sortbyorder[1];
		}
		else
		{
			$orderby = "ORDER BY rev.id DESC";
		}

		$revsql = 'SELECT rev.id as revid, rev.review as revreview, rev.user_id as revuserid, rev.modified_date as revdate, rev.review_for as revfor, rev.respond as revrespond, rev.rate as revrate, rev.rate_cust as revrate_cust, rev.rate_service as revrate_service, rev.rate_recommend as revrate_recommend, usr.full_name as userfullname, usr.email as useremail, usr.pic as usrpic, usr.fbpic as usrfbpic FROM tbl_reviews rev LEFT JOIN tbl_users usr ON rev.user_id=usr.id WHERE rev.review_for=? GROUP BY rev.id '.$orderby;//$queries[44];
	$pager = (!isset($global_req['showrev']) && $global_req['showrev'] != "")?25:$global_req['showrev'];//No of rows
	$pageid = 'rev';
	$pageno = 1;//(trim($global_req[$pageid.'_next_page']) != "")?$global_req[$pageid.'_next_page']:1;
	$showPageLinks = true;
	$pagepath = '';
	
	$selrev = $conn->selectSQL($revsql, array($agnid));

	unset($_SESSION['editpackageid']);
}
else
{
	echo $errormsg = "This page is restricted for you.";
}
?>