<?php
/*$acc_qry = $queries[41];
$selacc = $conn->selectSQL($acc_qry, array($sessuid));
$pic = $selacc[0]['pic'];
$picpath = (empty($selacc[0]['pic']))?BASE_URL."images/default.png":BASE_URL."userpic/".$selacc[0]['pic'];
if (!getimagesize($picpath)) { $picpath = BASE_URL."images/default.png"; }
$full_name = $selacc[0]['full_name'];
$added_date = $selacc[0]['added_date'];
$showdate = date('M Y',strtotime($added_date));
$email = $selacc[0]['email'];
$website = $selacc[0]['website'];
$address = $selacc[0]['address'];
$address2 = $selacc[0]['address2'];
$city = $selacc[0]['city'];
$country = $selacc[0]['country'];
$rate_total = $selacc[0]['rate_total'];
$acc = $selacc[0]['pic'];
$acc = $selacc[0]['pic'];
$acc = $selacc[0]['pic'];
$acc = $selacc[0]['pic'];
$acc = $selacc[0]['pic'];
$acc = $selacc[0]['pic'];
$rev_acc_qry = $queries[42];
$selrevacc = $conn->selectSQL($rev_acc_qry, array($sessuid));
$total_reviews_by = count($selrevacc);

*/
$usr = "";
$email = "";
$contact = "";
$address = "";
$address2 = "";
$city = "";
$country = "";
$pic = "";
$userpic = "";
$website = "";
$error = '';
$errorp = '';
$errorc = '';
$errorinf = '';

if(isset($_FILES['upld']) && $_FILES['upld']['name'] != "")
{
	$uploadFile = $_FILES['upld'];
	$values = "";
	  //Get the temp file path
	  $tmpFilePath = $uploadFile['tmp_name'];
	
	  //Make sure we have a filepath
	  if ($tmpFilePath != ""){
		//Setup our new file path
		$filename = time().$uploadFile['name'];
		$newFilePath = BASE_PATH."userpic/" . $filename;
	
		//Upload the file into the temp dir
		if(move_uploaded_file($tmpFilePath, $newFilePath)) {
	
		  	$ins_qry = $queries[67];//update query
			$insuserid = $sessuid;
			$ins = $conn->executePrepared($ins_qry, array("$filename",$insuserid));
		}
	  }
}
if(isset($global_req['loginfrmsubmit']) && $global_req['loginfrmsubmit'] == "save")
{
	if(!isset($global_req['username']) || empty($global_req['username']))
	{
		$error = '<div role="alert" class="alert text-danger">Please insert name.</div>';
	}
	else
	{
		$username = $global_req['username'];
		$setname = '';
		if($sessutype == 2){ $last_name = $global_req['last_name']; $setname = ', last_name="'.$last_name.'"'; }
		
		$ins_qry = 'UPDATE tbl_users SET full_name=? '.$setname.' WHERE id=?';
		$insuserid = $sessuid;
		$ins = $conn->executePrepared($ins_qry, array("$username",$insuserid));
		$error = '<div role="alert" class="alert text-success">Login Information saved successfully</div>';
	}
}
if(isset($global_req['passfrmsubmit']) && $global_req['passfrmsubmit'] == "save")
{
	if(!isset($global_req['oldpass']) || empty($global_req['oldpass']))
	{
		$errorp = '<div role="alert" class="alert text-danger">Please insert old password.</div>';
	}
	else if(!isset($global_req['newpass']) || empty($global_req['newpass']))
	{
		$errorp = '<div role="alert" class="alert text-danger">Please insert new password.</div>';
	}
	else if(strlen($global_req['newpass']) > 9)
	{
		$errorp = '<div role="alert" class="alert text-danger">Please insert password max 9 characters.</div>';
	}
	else if($global_req['newpass'] != $global_req['confirmpass'])
	{
		$errorp = '<div role="alert" class="alert text-danger">Passwords are not matching.</div>';
	}
	else
	{
		$usql = $queries[1006];
		$selusers = $conn->selectSQL($usql, array($sessuid));
		$password = $selusers[0]['password'];
		$oldpass = md5($global_req['oldpass']);
		if($password == $oldpass)
		{
			$newpass = md5($global_req['newpass']);
			$ins_qry = 'UPDATE tbl_users SET password=? WHERE id=?';
			$insuserid = $sessuid;
			$ins = $conn->executePrepared($ins_qry, array("$newpass",$insuserid));
			$errorp = '<div role="alert" class="alert text-success">Password updated successfully.</div>';
		}
		else
		{
			$errorp = '<div role="alert" class="alert text-danger">Old password is incorrect.</div>';
		}
	}
}
if(isset($global_req['infofrmsubmit']) && $global_req['infofrmsubmit'] == "save")
{
	if((!isset($global_req['country']) || empty($global_req['country'])) && $_SESSION['usertype'] == 1)
	{
		$errorinf = '<div role="alert" class="alert text-danger">Please select country.</div>';
	}
	else if((!isset($global_req['contact']) || empty($global_req['contact'])) && $_SESSION['usertype'] == 1)
	{
		$errorinf = '<div role="alert" class="alert text-danger">Please insert phone.</div>';
	}
	else
	{
		$country = $global_req['country'];
		$country_code = $global_req['country_code'];
		$contact = $global_req['contact'];
		$setname = '';		
		$ins_qry = 'UPDATE tbl_users SET country=?, country_code=?, phone=?  WHERE id=?';
		$insuserid = $sessuid;
		$ins = $conn->executePrepared($ins_qry, array($country,$country_code,$contact,$insuserid));
		$errorinf = '<div role="alert" class="alert text-success">Contact Information saved successfully</div>';
	}
}
if(isset($global_req['contactfrmsubmit']) && $global_req['contactfrmsubmit'] == "save")
{
	/*if(!isset($global_req['usr']) || empty($global_req['usr']))
	{
		$error = '<div role="alert" class="alert alert-danger">Please insert Full name.</div>';
	}
	if(!isset($global_req['email']) || empty($global_req['email']))
	{
		$error .= '<div role="alert" class="alert alert-danger">Please insert Email.</div>';
	}*/
	/*if(!isset($global_req['website']) || empty($global_req['website']))
	{
		$errorc .= '<div role="alert" class="alert text-danger">Please insert Website.</div>';
	}
	else if (filter_var($global_req['website'], FILTER_VALIDATE_URL) === false)
	{
		$errorc .= '<div role="alert" class="alert text-danger">Please enter correct Website address.</div>';
	}*/
	if (isset($global_req['website']) && trim($global_req['website']) != '' && (filter_var($global_req['website'], FILTER_VALIDATE_URL) === false))
	{
		$errorc .= '<div role="alert" class="alert text-danger">Please enter correct Website address.</div>';
	}
	if(!isset($global_req['address']) || empty($global_req['address']) || !isset($global_req['city']) || empty($global_req['city']) || !isset($global_req['country']) || empty($global_req['country']))
	{
		$errorc .= '<div role="alert" class="alert text-danger">Please insert complete Address.</div>';
	}
	/*if(isset($global_req['userid']) && trim($global_req['userid']) != "")
	{
		if($global_req['pwd'] != $global_req['cpwd'])
		{
			$error .= '<div role="alert" class="alert alert-danger">Passwords are not matching.</div>';
		}
	}
	else
	{
		if(!isset($_FILES['pic']) || empty($_FILES['pic']))
		{
			$error = '<div role="alert" class="alert alert-danger">Please upload profile pic.</div>';
		}
		if(!isset($global_req['pwd']) || empty($global_req['pwd']))
		{
			$error .= '<div role="alert" class="alert alert-danger">Please insert Password.</div>';
		}
		else if(!isset($global_req['cpwd']) || empty($global_req['cpwd']) || $global_req['cpwd'] != $global_req['pwd'])
		{
			$error .= '<div role="alert" class="alert alert-danger">Passwords are not matching.</div>';
		}
	}*/
	//$usr = $global_req['usr'];
	//$pic = $_FILES['pic'];
	//$userpic = $global_req['userpic'];
	//$email = $global_req['email'];
	$contact = "";//$global_req['contact'];
	$address = $global_req['address'];
	$address2 = $global_req['address2'];
	$city = $global_req['city'];
	$country = $global_req['country'];
	$website = $global_req['website'];
	$agentname = $global_req['agentname'];
	$agentcountry = $global_req['agentcountry'];
	$agentcountrycode = $global_req['agentcountrycode'];
	$agentphone = $global_req['agentphone'];
	$agentemail = $global_req['agentemail'];
	$agentid = $global_req['agentid'];
	if(trim($errorc) == "")
	{
		/*if(isset($_FILES['pic']))
		{
			$max_file_size = 1024*2000; // 2Mb
			$valid_exts = array('jpeg', 'jpg', 'png', 'gif');
			// thumbnail sizes
			$sizes = array(200 => 200);
			if( $_FILES['pic']['size'] < $max_file_size ){
				// get file extension
				$ext = strtolower(pathinfo($_FILES['pic']['name'], PATHINFO_EXTENSION));
				if (in_array($ext, $valid_exts)) {
			*/		/* resize image */
			/*		foreach ($sizes as $w => $h) {
						$uplFilename = $funObj->resize($w, $h,BASE_PATH."userpic");
						if(trim($userpic) != "") { $userpicchk = BASE_PATH."userpic/".$userpic; unlink($userpicchk); }
						$userpic = $uplFilename;
					}
		
				} else {
					$_SESSION['error'] = '<div role="alert" class="alert alert-success">Unsupported file</div>';
				}
			} else{
				$_SESSION['error'] = '<div role="alert" class="alert alert-success">Please upload image smaller than 2MB</div>';
			}
		}*/
		
		//$pwd = md5($global_req['pwd']);
		//$utype = $global_req['utype'];
		//if(isset($global_req['userid']) && trim($global_req['userid']) != "")
		//{
			$ins_qry = 'UPDATE tbl_users SET website=?, address=?, address2=?, city=?, country=? WHERE id=?';//$queries[1005];//update query
			$insuserid = $sessuid;
			$ins = $conn->executePrepared($ins_qry, array("$website","$address","$address2","$city","$country",$insuserid));//full_name, pic, email, password, phone, website, address, address2, city, country, user_type, modified_by
			$errorc = '<div role="alert" class="alert text-success">Contact Information saved successfully</div>';
			//$actionqry = "update";
		/*}
		else
		{
			$ins_qry = $queries[1004];
			$ins = $conn->executePrepared($ins_qry, array("$usr","$userpic","$email","$pwd","$contact","$website","$address","$address2","$city","$country","$utype",0));//full_name, pic, email, password, phone, website, address, address2, city, country, user_type, modified_by
			$_SESSION['error'] = '<div role="alert" class="alert alert-success">User saved successfully.</div>';
			$actionqry = "insert";
			$insuserid = $conn->getInsertid();
		}*/
		if(!$ins)
		{
			$errorc = '<div role="alert" class="alert text-danger">Something went wrong, please try again later.</div>';
		}
		if(count($agentname) > 0)
		{
			for($i=0; $i<count($agentname); $i++)
			{
				$aname = $agentname[$i];
				$acountry = $agentcountry[$i];
				$acountrycode = empty($agentcountrycode[$i])?'':$agentcountrycode[$i];
				$aphone = $agentphone[$i];
				$aemail = $agentemail[$i];
				$aid = $agentid[$i];
				if(isset($aid) || !empty($aid))
				{
					$cnt_qry = $queries[1029];
					$cnt = $conn->executePrepared($cnt_qry, array("$aname","$acountry","$acountrycode","$aphone","$aemail","$insuserid",0,"$aid"));//cname, ccountry, cphone, cemail, userid, modified_by
				}
				else
				{
					$cnt_qry = $queries[1028];
					$cnt = $conn->executePrepared($cnt_qry, array("$aname","$acountry","$acountrycode","$aphone","$aemail","$insuserid",0));//cname, ccountry, cphone, cemail, userid, modified_by
				}
			}
		}
	}
}

$usql = $queries[1006];
$selusers = $conn->selectSQL($usql, array($sessuid));
$full_name = $selusers[0]['full_name'];
$last_name = $selusers[0]['last_name'];
$pic = $selusers[0]['pic'];
$topfbpic = $selusers[0]['fbpic'];
if(trim($pic) == "")
{
	$picpath = BASE_URL."images/default.png";
	if($sessfbuser ==1)
	{
		$picpath = $topfbpic;
	}
}
else
{
	if(file_exists(BASE_PATH."userpic/".$pic))
	{
		$picpath = BASE_URL."timthumb/timthumb.php?src=".BASE_URL."userpic/".$pic."&q=100&w=150";
	}
	else
	{
		$picpath = BASE_URL."timthumb/timthumb.php?src=".BASE_URL."images/default.png&q=100&w=150";
	}
}
$added_date = $selusers[0]['added_date'];
$showdate = date('M Y',strtotime($added_date));
$email = $selusers[0]['email'];
$contact = $selusers[0]['phone'];
$country_code = '+'.$selusers[0]['country_code'];
$address = $selusers[0]['address'];
$address2 = $selusers[0]['address2'];
$city = $selusers[0]['city'];
$country = $selusers[0]['country'];
$website = $selusers[0]['website'];
$utype = $global_req['utype'];
$rate_total = $selusers[0]['rate_total'];

$rev_acc_qry = $queries[42];
$selrevacc = $conn->selectSQL($rev_acc_qry, array($sessuid));
$total_reviews_by = count($selrevacc);
$getCityOptions = $funObj->getCityOptions(0,1);

$agn_sql = $queries[1030];
$selagents = $conn->selectSQL($agn_sql, array($sessuid));
$i = 1;
if(count($selagents) > 0)
{
	foreach($selagents as $agnrows)
	{
		$cctopt = '<select name="agentcountry[]" required="true" id="agentcountry'.$i.'" class="turnintodropdown1" onchange="countrychange(this.value,'.$i.')"><option value="">Select Country</option>';
		foreach($getCityOptions as $country1)
		{
			if($agnrows['ccountry'] == $country1['id']) { $selected = 'selected="selected"'; } else { $selected = ''; }
			$cctopt .= '<option value="'.$country1['id'].'" '.$selected.'>'.$country1['location'].'</option>';
		}
		$cctopt .= '</select>';
		$delimg = '<img src="'.BASE_URL.'images/dele.png" width="29" height="30" onclick="delthisagent('.$agnrows['id'].','.$i.',\''.BASE_URL.'\');" class="cursor-hand"/> Delete ';
		if($i == 1) { $delimg = ''; }
		$agentdetails .= '<div id="row_'.$i.'" class="addAgentRow"><div class="col-md-3 col-md-offset-1 mt7 col-sm-4">Agent '.$i.'</div><div class="form-out"><label>Agent '.$i.'\'s Name<span class="mandate">*</span></label>
			<input type="text" class="text-are2" placeholder="Enter name" required="true" id="agentname'.$i.'" name="agentname[]" value="'.$agnrows['cname'].'">
			<div class="col-md-offset-8"></div></div><div class="clear mb15"></div><div class="col-md-3 col-md-offset-1">'.$delimg.'</div><div class="form-out"><label>Country<span class="mandate">*</span></label>
			'.$cctopt.'
			<div class="col-md-offset-8"></div></div><div class="clear mb15"></div><div class="col-md-3 col-md-offset-1"></div><div class="form-out"><label>Phone<span class="mandate">*</span></label>
			<input class="country_code text-are2" style="width:60px;" placeholder="+91" type="text" name="agentcountrycode[]" id="country_code'.$i.'" value="'.$agnrows['ccountrycode'].'" readonly />
			<input type="number" class="text-are2" style="width:182px;" placeholder="Enter phone" maxlength="16" required="true" id="agentphone'.$i.'" name="agentphone[]" value="'.$agnrows['cphone'].'">
			<div class="col-md-offset-8"></div></div><div class="clear mb15"></div><div class="col-md-3 col-md-offset-1"></div><div class="form-out"><label>Email<span class="mandate">*</span></label>
			<input type="email" class="text-are2" placeholder="Enter email" required="true" id="agentemail'.$i.'" name="agentemail[]" value="'.$agnrows['cemail'].'">
			<div class="col-md-offset-8"></div></div><div class="clear mb25"></div>
		  <input type="hidden" id="agentid'.$i.'" name="agentid[]" value="'.$agnrows['id'].'"></div>
		';//<input type="text" class="text-are2" placeholder="Enter country" required="true" id="agentcountry'.$i.'" name="agentcountry[]" value="'.$agnrows['ccountry'].'">
		
		$i++;
	}
}
else
{
	$cctopt = '<select name="agentcountry[]" required="true" id="agentcountry'.$i.'" class="turnintodropdown1" onchange="countrychange(this.value,'.$i.')"><option value="">Select Country</option>';
	foreach($getCityOptions as $country1)
	{
		$cctopt .= '<option value="'.$country1['id'].'">'.$country1['location'].'</option>';
	}
	$cctopt .= '</select>';
	$agentdetails .= '<div id="row_'.$i.'"><div class="col-md-3 col-md-offset-1 mt7 col-sm-4">Agent '.$i.'</div><div class="form-out"><label>Agent '.$i.'\'s Name<span class="mandate">*</span></label>
		<input type="text" class="text-are2" placeholder="Enter name" required="true" id="agentname'.$i.'" name="agentname[]" value="">
		<div class="col-md-offset-8"></div></div><div class="clear mb15"></div><div class="col-md-3 col-md-offset-1"></div><div class="form-out"><label>Country<span class="mandate">*</span></label>
		'.$cctopt.'
		<div class="col-md-offset-8"></div></div><div class="clear mb15"></div><div class="col-md-3 col-md-offset-1"></div><div class="form-out"><label>Phone<span class="mandate">*</span></label>
		<input class="country_code text-are2" style="width:60px;" placeholder="+91" type="text" name="agentcountrycode[]" id="country_code'.$i.'" value="" readonly />
		<input type="number" class="text-are2" style="width:182px;" placeholder="Enter phone" maxlength="10" required="true" id="agentphone'.$i.'" name="agentphone[]" value="">
		<div class="col-md-offset-8"></div></div><div class="clear mb15"></div><div class="col-md-3 col-md-offset-1"></div><div class="form-out"><label>Email<span class="mandate">*</span></label>
		<input type="email" class="text-are2" placeholder="Enter email" required="true" id="agentemail'.$i.'" name="agentemail[]" value="">
		<div class="col-md-offset-8"></div></div><div class="clear mb25"></div>
	';
	$i++;
}
$agentdetails .= '<div id="addagents'.$i.'"><div class="col-md-3 col-md-offset-1"></div><div class="form-out" onclick="addagents('.$i.',\''.BASE_URL.'\');"><label class="cursor-hand"><img src="'.BASE_URL.'images/pla.png" width="30" height="29" alt=""/> <div style="display:inline-block;">Add Agent</div></label></div></div><div class="div'.$i.'"></div>';//
				
			

?>