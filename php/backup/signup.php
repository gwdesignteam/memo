<?php
if(isset($sessuid) && trim($sessuid) != "")
{
	$path = BASE_URL;
	header("Location:".$path);
}
$error = "";
if(isset($global_req['submit']) && $global_req['submit'] == "signup")
{
	$utype = $global_req['utype'];
	$full_name = trim($global_req['full_name']);
	if($utype == 1)
	{
		$first_name = $full_name;
		$last_name = '';
	}
	else
	{
		$exp_name = explode(" ",$global_req['full_name']);
		$first_name = $exp_name[0];
		$last_name = $exp_name[1];
	}
	$inputEmail = $global_req['inputEmail'];
	$inputPassword = $global_req['inputPassword'];
	$inputcPassword = $global_req['inputcPassword'];
	$utypeval = ($utype == 1) ? 'Travel Agency' : 'User';
	$usub = $global_req['usub'];
	if(empty($full_name) || empty($inputEmail) || empty($inputPassword))
	{
		$error = '<div role="alert" class="alert alert-danger">All fields are mandatory.</div>';
	}
	if($inputPassword != $inputcPassword)
	{
		$error = '<div role="alert" class="alert alert-danger">Passwords are not matching.</div>';
	}
	if($error == "")
	{
		$sel_qry = $queries[37];
		$sel = $conn->selectSQL($sel_qry, array($inputEmail));
		if(count($sel) > 0)
		{
			$error = '<div role="alert" class="alert alert-danger">'.$inputEmail.' This email is already registered.<br/><a href="login">Please Log In</a></div>';
		}

	}
	if($error == "")
	{
		/*Here Start Mailchimp code*/
		if(isset($usub) && $usub == 1)
		{
			include_once 'mailchimp/src/Mailchimp.php';

			$name = filter_var($first_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$lname = filter_var($last_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$email = filter_var($inputEmail, FILTER_SANITIZE_EMAIL);
			/*
			 * Place here your validation and other code you're using to process your contact form.
			 * For example: mail('your@mail.com', 'Subject: contact form', $_POST['message']);
			 * Don't forget to validate all your form input fields!
			 */
			$mc = new Mailchimp('596d9e2250170cb7271476bb40ef89d6-us10');
			$mvars = array('optin_ip'=> $_SERVER['REMOTE_ADDR'], 'FNAME' => $name, 'LNAME' => $lname, 'UTYPE' => $utypeval);
			$result = $mc->call('lists/subscribe', array(
					'id'                => 'e31f8bbd64',
					'email'             => array('email'=>$email),
					'merge_vars'        => $mvars,
					'double_optin'      => true,
					'update_existing'   => false,
					'replace_interests' => false,
					'send_welcome'      => false
				)
			);
			if (!empty($result['euid'])) {
				//echo 'Thanks, please check your mailbox and confirm the subscription.';
			} else {
				if (isset($result['status'])) {
					switch ($result['code']) {
						case 214:
						//echo 'You\'re already a member of this list.';
						break;
						// check the MailChimp API docs for more codes
						default:
						//echo 'An unknown error occurred.';
						break;
					}
				}
			}
		}
		/*End Mailchimp code*/
		$inputPassword = md5($inputPassword);
		$added_date = date('Y-m-d H:i:s');
		$hash = md5(time()).rand(1,5).$inputPassword;
		$hash = substr($hash,15,20);
		$hash = trim($hash,' ');
		$hash = trim($hash,'%20');
		$hash = str_replace(' ','',$hash);
		$hash = str_replace('%20','',$hash);
		$sel_qry = $queries[38];//full_name, email, password, user_type, modified_by
		$sel = $conn->executePrepared($sel_qry, array("$first_name","$last_name","$inputEmail","$inputPassword",$utype,"$hash","$added_date",0));
		$path = BASE_URL."?signupdone=$inputEmail";
		$verifylink = BASE_URL.'signup?hash='.$hash;
		$funObj->sendemail($inputEmail,"registration",$verifylink,$full_name."|-|".$utype);
		header("Location:".$path);
	}
}
if(isset($global_req['hash']) && $global_req['hash'] != "")
{
	$hash = $global_req['hash'];
	$hash = trim($hash,' ');
	$hash = trim($hash,'%20');
	$hash = str_replace(' ','',$hash);
	$hash = str_replace('%20','',$hash);
	$hash_qry = "SELECT id, full_name, last_name, user_type, email FROM tbl_users WHERE verification_code='".$hash."' AND status=0";//$queries[39];
	$hashverify = $conn->selectSQL($hash_qry, array());
	
	if(count($hashverify) > 0)
	{
		$uid = $hashverify[0]['id'];
		$name = $hashverify[0]['full_name'].' '.$hashverify[0]['last_name'];
		$user_type = $hashverify[0]['user_type'];
		$email = $hashverify[0]['email'];
		$sel_qry = $queries[40];
		$sel = $conn->executePrepared($sel_qry, array($uid));
		$path = BASE_URL."?verifydone=1&nm=".$name."&tp=".$user_type;
		if($user_type == 1) {
			$funObj->sendemail($email,"registration-verify",'',$name);
		}
		header("Location:".$path);
	}
	else
	{
		$path = BASE_URL."?verifydone=2";
		header("Location:".$path);
	}
}
?>
