<?php
$usr = "";
$email = "";
$contact = "";
$address = "";
$address2 = "";
$city = "";
$country = "";
$pic = "";
$userpic = "";
$website = "";

$usql = $queries[1006];
$selusers = $conn->selectSQL($usql, array($sessuid));
$full_name = $selusers[0]['full_name'];
$pic = $selusers[0]['pic'];
if(trim($pic) == "")
{
	$picpath = BASE_URL."images/default.png";
}
else
{
	if(file_exists(BASE_PATH."userpic/".$pic))
	{
		$picpath = BASE_URL."userpic/".$pic;
	}
	else
	{
		$picpath = BASE_URL."images/default.png";
	}
}
$added_date = $selusers[0]['added_date'];
$showdate = date('M Y',strtotime($added_date));
$email = $selusers[0]['email'];
$contact = $selusers[0]['phone'];
$address = $selusers[0]['address'];
$address2 = $selusers[0]['address2'];
$city = $selusers[0]['city'];
$country = $selusers[0]['country'];
$website = $selusers[0]['website'];
$utype = $global_req['utype'];
$rate_total = $selusers[0]['rate_total'];

$viewsql = "SELECT vws.modified_date,vws.package_id, count(*) as viewcount FROM tbl_views as vws LEFT JOIN tbl_packages pks ON vws.package_id=pks.id WHERE pks.userid=$sessuid AND vws.modified_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) GROUP BY DATE(vws.modified_date), vws.package_id HAVING count(*) > 0 ORDER BY DATE(vws.modified_date)";
$selviews = $conn->selectSQL($viewsql, array());
foreach($selviews as $rowviews)
{
	$viewcount = $rowviews['viewcount'];
	//echo "aaa".date('Y-m-d',strtotime($rowviews['modified_date'])).date('Y-m-d').(date('Y-m-d') - date('Y-m-d',strtotime($rowviews['modified_date'])));
	//echo "mmm".date_diff(date_create(date('Y-m-d',strtotime($rowviews['modified_date']))),date_create(date('Y-m-d')));
	$days = round(abs(strtotime(date('Y-m-d',strtotime($rowviews['modified_date']))) - strtotime(date('Y-m-d')))/86400);
	
}

$inqsql = $queries[43];
$agnid = $sessuid;//$global_req['agnid'];
$pager = 3;//No of rows
$pageid = 'inq';
$pageno = 1;//(trim($global_req[$pageid.'_next_page']) != "")?$global_req[$pageid.'_next_page']:1;
$showPageLinks = true;
$pagepath = '';
$selinq = $conn->selectSQL($inqsql, array($agnid), $pager, $pageno, $pageid, $showPageLinks, $pagepath);

$revsql = $queries[44];
$agnid = $sessuid;//$global_req['agnid'];
$pager = 3;//No of rows
$pageid = 'rev';
$pageno = 1;//(trim($global_req[$pageid.'_next_page']) != "")?$global_req[$pageid.'_next_page']:1;
$showPageLinks = true;
$pagepath = '';
$selrev = $conn->selectSQL($revsql, array($agnid), $pager, $pageno, $pageid, $showPageLinks, $pagepath);

/*Calculation for graph Starts*/
		$uid = $sessuid;
		//For 1 month for graph
        $hilightdur = 7;
		$startdate = date('Y-m-d H:i:s');
		$enddate = date('Y-m-d H:i:s', strtotime( "$startdate -$hilightdur day" ));
		$week_qry = "SELECT count(vw.id) as cnt, pk.package_title FROM tbl_views vw LEFT JOIN tbl_packages pk ON pk.id=vw.package_id WHERE pk.status=1 AND pk.userid=$uid AND vw.modified_date BETWEEN '$enddate' AND '$startdate' GROUP BY(vw.package_id) ORDER BY pk.views DESC LIMIT 3";//$week_qry = "SELECT count(vw.id) as cnt, pk.package_title, DATE(vw.modified_date) as vwdt FROM tbl_views vw LEFT JOIN tbl_packages pk ON pk.id=vw.package_id WHERE pk.status=1 AND pk.userid=$uid AND vw.modified_date BETWEEN '$enddate' AND '$startdate' GROUP BY DATE(vw.modified_date) , vw.package_id ORDER BY pk.views DESC";//
		$sel_week = $conn->selectSQL($week_qry, array());
		$pkviews1 = array();
		if($sel_week)
		{
			foreach($sel_week as $week)
			{
				//if($week['vwdt'])
				$pkviews = $week['cnt'];
				$pkviews1[] = ($pkviews >= 0)?$pkviews:0;
			}
		}
		else
		{
			$pkviews1[] = 0;
			$pkviews1[] = 0;
			$pkviews1[] = 0;
		}
		/*$week_qry = "SELECT * FROM tbl_graph WHERE uid=$uid";
		$sel_week = $conn->selectSQL($week_qry, array());
		if($sel_week)
		{
			foreach($sel_week as $week)
			{
				$package_name1 = $week['package_name1'];
				$package_name2 = $week['package_name2'];
				$package_name3 = $week['package_name3'];
				$graph_week = $week['graph_week'];
				$graph_month = $week['graph_month'];
				$graph_year = $week['graph_year'];
			}
		}*/
		//For 1 month for graph
		$hilightdur = 30;
		$startdate = date('Y-m-d H:i:s');
		$enddate = date('Y-m-d H:i:s', strtotime( "$startdate -$hilightdur day" ));
		$week_qry = "SELECT count(vw.id) as cnt, pk.package_title FROM tbl_views vw LEFT JOIN tbl_packages pk ON pk.id=vw.package_id WHERE pk.status=1 AND pk.userid=$uid AND vw.modified_date BETWEEN '$enddate' AND '$startdate' GROUP BY(vw.package_id) ORDER BY pk.views DESC LIMIT 3";
		$sel_week = $conn->selectSQL($week_qry, array());
		$pkviews2 = array();
		if($sel_week)
		{
			foreach($sel_week as $week)
			{
				$pkviews = $week['cnt'];
				$pkviews2[] = ($pkviews >= 0)?$pkviews:0;
			}
		}
		else
		{
			$pkviews2[] = 0;
			$pkviews2[] = 0;
			$pkviews2[] = 0;
		}
		//For 1 year for graph
		$hilightdur = 365;
		$startdate = date('Y-m-d H:i:s');
		$enddate = date('Y-m-d H:i:s', strtotime( "$startdate -$hilightdur day" ));
		$week_qry = "SELECT count(vw.id) as cnt, pk.package_title FROM tbl_views vw LEFT JOIN tbl_packages pk ON pk.id=vw.package_id WHERE pk.status=1 AND pk.userid=$uid AND vw.modified_date BETWEEN '$enddate' AND '$startdate' GROUP BY(vw.package_id) ORDER BY pk.views DESC LIMIT 3";
		$sel_week = $conn->selectSQL($week_qry, array());
		$pkviews3 = array();
		$pktitle = array();
		if($sel_week)
		{
			foreach($sel_week as $week)
			{
				$pkviews = $week['cnt'];
				$pkviews3[] = ($pkviews >= 0)?$pkviews:0;
				$pktitle[] = $week['package_title'];
			}
		}
		else
		{
			$pkviews3[] = 0;
			$pkviews3[] = 0;
			$pkviews3[] = 0;
		}
/*Calculation for graph Ends*/
?>