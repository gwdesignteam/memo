<?php
if(isset($sessuid) && trim($sessuid) != "")
{
	$path = BASE_URL;
	header("Location:".$path);
}
$error = "";
$errorinner = '';
if(isset($global_req['submit']) && $global_req['submit'] == "Resend")
{
	$useremail = $global_req['inputEmail'];
	$sel_qry = $queries[37];
	$sel = $conn->selectSQL($sel_qry, array($useremail));
	
	$hash = rand().time();
	if(count($sel) > 0)
	{
		$full_name = $sel[0]['full_name'];
		$uid = $sel[0]['id'];
		$hash = trim($hash,' ');
		$hash = trim($hash,'%20');
		$hash = str_replace(' ','',$hash);
		$hash = str_replace('%20','',$hash);
		$verifylink = BASE_URL.'login?resendhash='.$hash;
		$funObj->sendemail($useremail,"resend",$verifylink,$full_name);
		$upd_qry = $queries[62];
		$upd = $conn->executePrepared($upd_qry, array($hash,$uid));
		$error = '<br><br><div role="alert" class="alert alert-success">Email sent on registered email.</div>';
	}
	else
	{
		$error = '<br><br><div role="alert" class="alert alert-danger">Enter a valid email.</div>';
	}
}
if(isset($global_req['resendhash']) && $global_req['resendhash'] != "")
{
	$hash = $global_req['resendhash'];
	$hash = trim($hash,' ');
	$hash = trim($hash,'%20');
	$hash = str_replace(' ','',$hash);
	$hash = str_replace('%20','',$hash);
	$hash_qry = "SELECT id FROM tbl_users WHERE verification_code='".$hash."'";//$queries[39];
	$hashverify = $conn->selectSQL($hash_qry, array());
	
	if(count($hashverify) > 0)
	{
		$uid = $hashverify[0]['id'];
		$sel_qry = $queries[40];
		$sel = $conn->executePrepared($sel_qry, array($uid));
		$path = BASE_URL."?verifydone=1";
		header("Location:".$path);
	}
	else
	{
		$path = BASE_URL."?verifydone=2";
		header("Location:".$path);
	}
}


if(isset($global_req['submit']) && $global_req['submit'] == "Forgot1")
{
	$upassword = $global_req['uinputPassword'];
	$cpassword = $global_req['cinputPassword'];
	
	$hash = $global_req['forgothash'];
	if($upassword == $cpassword)
	{
		$upassword = md5($upassword);
		$upd_qry = $queries[63];
		$upd = $conn->executePrepared($upd_qry, array($upassword,$hash));
		header("Location:".BASE_URL."login?forgotsuccess=1");
		$error = '<br><br><div role="alert" class="alert alert-success">Passwrod reset successfully, please login.</div>';
	}
	else
	{
		$error = '<br><br><div role="alert" class="alert alert-danger">Passwords does not match.</div>';
		$forgoterror = 1;
	}
}
if(isset($global_req['submit']) && $global_req['submit'] == "Forgot")
{
	$useremail = $global_req['inputEmail'];
	$sel_qry = $queries[37];
	$sel = $conn->selectSQL($sel_qry, array($useremail));
	
	$hash = rand().time();
	if(count($sel) > 0)
	{
		if($sel[0]['status'] == 1)
		{
			$uid = $sel[0]['id'];
			$full_name = $sel[0]['full_name'];
			$hash = trim($hash,' ');
			$hash = trim($hash,'%20');
			$hash = str_replace(' ','',$hash);
			$hash = str_replace('%20','',$hash);
			$verifylink = BASE_URL.'login?forgothash='.$hash;
			$funObj->sendemail($useremail,"forgot",$verifylink,$full_name);
			$upd_qry = $queries[62];
			$upd = $conn->executePrepared($upd_qry, array($hash,$uid));
			$error = '<br><br><div role="alert" class="alert alert-success">We`ve sent you a link to change your password</div>';
		}
		else
		{			
			$full_name = $sel[0]['full_name'];
			$uid = $sel[0]['id'];
			$hash = trim($hash,' ');
			$hash = trim($hash,'%20');
			$hash = str_replace(' ','',$hash);
			$hash = str_replace('%20','',$hash);
			$verifylink = BASE_URL.'login?resendhash='.$hash;
			$funObj->sendemail($useremail,"resend",$verifylink,$full_name);
			$upd_qry = $queries[62];
			$upd = $conn->executePrepared($upd_qry, array($hash,$uid));

			$error = '<br><br><div role="alert" class="alert alert-danger">Click on the activation link in your registered email to activate your account</div>';
			$errorinner = '<br><br><a href="'.BASE_URL.'login?reverification=1">Resend verification link</a>';
		}
	}
	else
	{
		$error = '<br><br><div role="alert" class="alert alert-danger">Enter a valid email.</div>';
	}
}
if(isset($global_req['forgotsuccess']) && trim($global_req['forgotsuccess']) == 1)
{
	$error = '<br><br><div role="alert" class="alert alert-success">Passwrod reset successfully, please login.</div>';
}
if(isset($global_req['submit']) && $global_req['submit'] == "Login")
{
	$sel_qry = $queries[0];
	$inputEmail = $global_req['inputEmail'];
	if(empty($inputEmail) && isset($_COOKIE['remember_me']) && !empty($_COOKIE['remember_me'])) { $inputEmail = $_COOKIE['remember_me']; }
	$inputPassword = md5($global_req['inputPassword']);
	$remme = $global_req['remme'];
	if($remme == 1)
	{
		$year = time() + 31536000;
		setcookie('remember_me', $global_req['inputEmail'], $year);
	}
	else
	{
		$past = time() - 3600;
		setcookie('remember_me', '', $past);
	}
	$sel = $conn->selectSQL($sel_qry, array($inputEmail,$inputPassword));
	if(count($sel) > 0)
	{
		if(($sel[0]['status'] == 1 && $sel[0]['isblocked'] == 0) &&( ($sel[0]['user_type']== 1 && $sel[0]['verified'] ==1) || ($sel[0]['user_type']== 2)))
		{
			$_SESSION['userid'] = $sel[0]['id'];
			$_SESSION['useremail'] = $sel[0]['email'];
			$_SESSION['usertype'] = $sel[0]['user_type'];
			$_SESSION['verified'] = $sel[0]['verified'];
			$_SESSION['fbuser'] = 0;
			if($_SESSION['usertype'] == 1 && $_SESSION['verified'] == 1)
			{
				$path = BASE_URL."myaccount";
				$funObj->userLog($_SESSION['userid'],"Login","User Login go to myaccount page");
			}
			elseif (isset($_SESSION['redirected_url'])) {	
				$path = $_SESSION['redirected_url'];//BASE_URL."index.php?".$_SESSION['redirected_url'];
				unset($_SESSION['redirected_url']);
			}
			else
			{
				$htpref = $_SERVER['HTTP_REFERER'];
				$path = BASE_URL;
				$newpath = strpos($htpref,$path);
				if(trim($newpath) != '')
				{
					$path = $htpref;
				}
				$funObj->userLog($_SESSION['userid'],"Login","User Login go to home page");
			}
			header("Location:".$path);
		}
		elseif($sel[0]['isblocked'] == 1)
		{
			$error = '<br><br><div role="alert" class="alert alert-danger">Your account is Blocked.</div>';
			$errorinner = '<br><br>Your account is Blocked. - Please contact to our technical assist.';
		}
		elseif($sel[0]['user_type'] == 1 && $sel[0]['verified'] == 0)
		{
			$error = '<br><br><div role="alert" class="alert alert-danger">Your account is not verified by Admin.</div>';
			$errorinner = '<br><br>Please be have patience; Till admin approve your Account.';
		}
		else
		{
			$error = '<br><br><div role="alert" class="alert alert-danger">Your account has been temporarily deactivated.  Please email info@tripdaddy.co</div>';
			$errorinner = '<br><br><a href="'.BASE_URL.'login?reverification=1">Resend verification link</a>';
		}
	}
	else
	{
		$error = '<br><br><div role="alert" class="alert alert-danger">Invalid email or password.</div>';
	}
}
?>
