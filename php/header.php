<?php
header('Cache-Control: max-age=900');
$conn = new DAO();

$sitestatus = $queries[77];
$status = $conn->selectSQL($sitestatus, array('1'));

if($status[0]['maintenance'] != 0){ // redirection  on status
 header("Location: maintenance.php");
 exit; }

$topusql = $queries[1006];
$topselusers = $conn->selectSQL($topusql, array($sessuid));
$topfull_name = $topselusers[0]['full_name'];
$toppic = $topselusers[0]['pic'];
$topfbpic = $topselusers[0]['fbpic'];
if(trim($toppic) == "")
{
	$toppicpath = BASE_URL."images/default.png";
	if($sessfbuser ==1)
	{
		$toppicpath = $topfbpic;
	}
}
else
{
	if(file_exists(BASE_PATH."userpic/".$toppic))
	{
		$toppicpath = BASE_URL."timthumb/timthumb.php?src=".BASE_URL."userpic/".$toppic."&q=100&w=50";
	}
	else
	{
		$toppicpath = BASE_URL."timthumb/timthumb.php?src=".BASE_URL."images/default.png&q=100&w=50";
	}
}
$_SERVER['REQUEST_URI'];
$request_uri = explode('?', $_SERVER['REQUEST_URI'],2);
$page_full_url = $request_uri[0];

// Remove or change below code according to server requirement
$page_full_url = str_replace('/tripdaddy/', '', $page_full_url);
if(substr($page_full_url,-1,1) == "/") { $page_full_url = substr($page_full_url,0,-1); }
if(substr($page_full_url,0,1) == "/") { $page_full_url = substr($page_full_url,1); }

$selpagename = $queries[82];
$currentpagename = $conn->selectSQL($selpagename, array($page_full_url));
if(count($currentpagename) > 0 && trim($page_full_url) != "")
{
	$pageTitle = $currentpagename[0]['page_title'];
	$pageKeyword = $currentpagename[0]['keyword'];
	$pageDesc = $currentpagename[0]['description'];
	$pageAuthor = $currentpagename[0]['author'];
	$pagename = $currentpagename[0]['page_name'];
	$pageid = $currentpagename[0]['page_id'];
	$pageurl = $currentpagename[0]['page_url'];
}
else
{
	$selpagename = $queries[80];
	$currentpagename = $conn->selectSQL($selpagename, array($pagename));
	if(count($currentpagename) > 0)
	{
		$pageTitle = $currentpagename[0]['page_title'];
		$pageKeyword = $currentpagename[0]['keyword'];
		$pageDesc = $currentpagename[0]['description'];
		$pageAuthor = $currentpagename[0]['author'];
		$pagename = $currentpagename[0]['page_name'];
		$pageid = $currentpagename[0]['page_id'];
		$pageurl = $currentpagename[0]['page_url'];
	}
}
if($pagename == 'package_details'){
	$pid = (trim($global_req['pid'])=="")?$pageid:$global_req['pid'];
	$action = $global_req['action'];
	$sel_qry = $queries[20];
	$selpackages = $conn->selectSQL($sel_qry, array($pid));
	$discount = $selpackages[0]['discount'];
	$destinationadded = $selpackages[0]['destination'];
	$countrydesti = $funObj->getDestinationCountryName($destinationadded, 1);
	if(trim($countrydesti[0]['cntlocation']) != "") { $contryorcontinent = $countrydesti[0]['cntlocation']; } else { $contryorcontinent = "World"; }
	$discounted_price = $selpackages[0]['cost'] - ($selpackages[0]['cost'] * $discount / 100);
	$pictures = $funObj->getPictures($pid);
	$pktitle = str_replace("{country}",$contryorcontinent,$pageTitle);
    $pageTitle = $pktitle;
	$pkdesc = str_replace("{country}",$contryorcontinent,$pageDesc);
	$pageDesc = $pkdesc;
	$departure_cities = $selpackages[0]['departure_cities'];
	$departcitynames = $funObj->getDestinationName($departure_cities);
	foreach($departcitynames as $depcit)
	{
		if(trim($departcitynames1) != "") { $departcitynames1 .= ","; }
		$departcitynames1 .= $depcit['location'];
	}
	$desticitynames = $funObj->getDestinationName($destinationadded);
	foreach($desticitynames as $descit)
	{
		if(trim($desticitynames1) != "") { $desticitynames1 .= ","; }
		$desticitynames1 .= $descit['location'];
	}
	$attractions = $selpackages[0]['attractions'];
	$attractionnames = $funObj->getSelectedattr($attractions,1);
	foreach($attractionnames as $attrnm)
	{
		if(trim($attractionnames1) != "") { $attractionnames1 .= ","; }
		$attractionnames1 .= $attrnm['attraction'];
	}
	$pkkey = $selpackages[0]['agency'].", ".$departcitynames1.", ".$desticitynames1.", ".$attractionnames1.", package honeymoon packages, vacation packages, holiday packages, vacation deals, tour packages";
	$pageKeyword = $pkkey;
}
if($pagename == 'agentprofile'){
	$agnid = (trim($global_req['agnid'])=="")?$pageid:$global_req['agnid'];
	$usql = $queries[1006];
	$selusers = $conn->selectSQL($usql, array($agnid));
	$tafull_name = $selusers[0]['full_name'];
	$tacity = $selusers[0]['city'];
	$pktitle = str_replace("{agency}",$tafull_name,$pageTitle);
	$pageTitle = $pktitle;
	$pkkey = $tafull_name.", ".$tacity.", honeymoon packages, vacation packages, holiday packages, vacation deals, tour packages";
	$pageKeyword = $pkkey;
}
?>
