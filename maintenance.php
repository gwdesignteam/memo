<?php 
include_once("config/config.php");
error_reporting(E_ALL);
ini_set("display_errors",0);
$conn = new DAO();
$sql = $queries[1050];
$selcntr = $conn->selectSQL($sql,array());

// redirect to home page if site is alive
$sitestatus = $queries[77];
$status = $conn->selectSQL($sitestatus, array('1'));

if($status[0]['maintenance'] == 0){ // redirection  on status
 header("Location: ".BASE_URL);
 exit; }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Coming Soon</title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="tools/style.css" rel="stylesheet" type="text/css" />
<link href="tools/960.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Oswald:700,300|PT+Sans+Caption' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Crete+Round|Parisienne' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="shim"></div>
<div id="content">
	<div class="watch"></div>
	<div class="rest">
		<h1><span></span></h1>
		<div class="clear"></div>			
		<h2><span>Updating Website </span><br/>Be Back Soon</h2>
		<p>In the meantime, connect with us</p>
		
		<ul class="social">
             <li>  <a target="_blank" href="https://www.facebook.com/tripdaddy.co"><img width="75" height="75" src="images/facebook500.png" alt=""></a></li>
             <li>   <a class="ml15" target="_blank" href="https://twitter.com/tripdaddyco"><img width="75" height="75" src="images/twitter.png" alt=""></a></li>
             <li>  <a class="ml15" target="_blank" href="https://plus.google.com/+Tripdaddyco/about"><img width="75" height="75" src="images/g+.png" alt=""></a></li>
             <li>  <a class="ml15" target="_blank" href="https://instagram.com/tripdaddy.co/"><img width="75" height="75" src="images/insta.png" alt=""></a></li>
        </ul>
		
		<ul class="info">
			<li class="ph"><?php echo $selcntr[0]['contact_number']; ?></li>
			<li class="ad"><?php echo $selcntr[0]['address'] ; ?></li>
			<li class="mail"><a href="#"><?php echo $selcntr[0]['contact_email'] ; ?></a></li>
		</ul>
	</div>
</div>

</body>
</html>
