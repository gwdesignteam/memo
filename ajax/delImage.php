<?php
include_once("../config/config.php");
$conn = new DAO();
$error = "";
$pid = $global_req['pid'];
$imgname = $global_req['imgname'];
if(isset($pid) && $pid != 0)
{
	unlink(BASE_PATH.'packagepic/'.$imgname);
	unlink(BASE_PATH.'packagepic/big/'.$imgname);
	$del_qry = $queries[73];
	$del = $conn->executePrepared($del_qry, array($pid,$imgname));
	if($del)
	{
		$error = "1";
	}
	else
	{
		$error = "0";
	}
	echo $error;
}
?>