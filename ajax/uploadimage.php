<?php
include_once("../config/config.php");
$conn = new DAO();
?>
<link rel="stylesheet" href="<?php echo BASE_URL?>css/jquery.Jcrop.min.css" type="text/css" media="screen" charset="utf-8" />
<style>
.btn {
    -moz-user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}
.btn:focus, .btn:active:focus, .btn.active:focus, .btn.focus, .btn.focus:active, .btn.active.focus {
    outline: thin dotted;
    outline-offset: -2px;
}
.btn-primary {
    background-color: #337ab7;
    border-color: #2e6da4;
    color: #fff;
	margin-bottom: 5px;
    /*margin-left: 86%;*/
}
</style>
<script src="<?php echo BASE_URL?>js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo BASE_URL?>js/jquery.Jcrop.min.js" type="text/javascript" charset="utf-8"></script>
<?php
$sessuid = empty($sessuid)?0:$sessuid;
if(isset($_FILES['upld']) && $_FILES['upld']['name'] != "")
{
	$uploadFile = $_FILES['upld'];
	$values = "";
	  //Get the temp file path
	  $tmpFilePath = $uploadFile['tmp_name'];
	
	  //Make sure we have a filepath
	  if ($tmpFilePath != ""){
		//Setup our new file path
		$filename = time().str_replace(" ","-",$uploadFile['name']);
		$newFilePath = BASE_PATH."packagepic/" . $filename;
	
		//Upload the file into the temp dir
		if(move_uploaded_file($tmpFilePath, $newFilePath)) {
			$values .= "(".$_SESSION['editpackageid'].",'".$filename."',".$sessuid.")";
			$ins_info = "INSERT INTO tbl_pictures(package_id, pic, modified_by) VALUES ".$values;
			$ins_inf = $conn->executePrepared($ins_info, array());
			
			if($ins_inf)
			{
				$_SESSION['lastinsertedstep'] = 5;
				$_SESSION['phocls'] = 1;
				//$_SESSION['error5'] = '<div role="alert" class="alert alert-success">Photo saved successfully.</div>';
			}
			else
			{
				$error = '<div role="alert" class="alert alert-danger">Something went wrong, please try again later.</div>';
			}
		}
	  }
?>
<form role="form" class="form-signin" action="" method="post">
	<div align="right"><div class="col-md-1 pull-right"><button class="btn btn-primary" type="button" name="submit" value="reviewbtn" onclick="cropimage()">Crop & Done</button></div></div><!--onclick="location.reload();"<button class="btn btn-primary" type="button" name="submit" value="reviewbtn" onclick="cropimage()">Crop & Done</button>-->
    <div class="clear"></div>
    <input type="hidden" name="x" value="" id="x" />
    <input type="hidden" name="y" value="" id="y" />
    <input type="hidden" name="w" value="" id="w" />
    <input type="hidden" name="h" value="" id="h" />
    <input type="hidden" name="filepath" value="<?php echo BASE_PATH;?>packagepic/<?php echo $filename;?>" id="filepath" />
    <input type="hidden" name="uploadfilepath" value="<?php echo BASE_PATH;?>packagepic/big/<?php echo $filename;?>" id="uploadfilepath" />
    <?php $flvr = BASE_URL.'packagepic/'.$filename; $imgsz = getimagesize($flvr);?>
    <center><img id="cropbox" class="cropbox1" src="<?php echo BASE_URL;?>packagepic/<?php echo $filename;?>" /></center>
    
    
    <div id="msg-rev"></div>
</form>
<script>
var ma = 800;
var ma1 = 400;
var ma2 = <?php echo $imgsz[0];?>;
var maw = ma-200;
var mah = ma1-100;
var wdth = $( window ).width();
if(wdth <= 400)
{
	ma = ma/4;
	ma1 = ma1/4;
	ma2 = ma2/4;
	maw = ma*1.8;
	mah = ma1*1.8;
	$('#cropbox').attr('width',ma2);
}
else if(wdth <= 800)
{
	ma = ma/2;
	ma1 = ma1/2;
	ma2 = ma2/2;
	maw = ma*1.8;
	mah = ma1*1.8;
	$('#cropbox').attr('width',ma2);
}
	$('#cropbox').Jcrop({
      aspectRatio: 2/1,
	  MinSize: 500/500,
      onSelect: updateCoords,
	  setSelect: [0, 0, ma, ma1],
	  boxWidth: maw, boxHeight: mah,
	  allowResize: false
    });
function cropimage()
{
	var x = $('#x').val();
    var y = $('#y').val();
    var w = $('#w').val();
    var h = $('#h').val();
	if(wdth <= 400)
	{
		x = x*4;
		y = y*4;
		w = w*4;
		h = h*4;
	}
	else if(wdth <= 800)
	{
		x = x*2;
		y = y*2;
		w = w*2;
		h = h*2;
	}
	var filepath = $('#filepath').val();
	var uploadfilepath = $('#uploadfilepath').val();
	$.ajax({
		url: "cropPicture.php",
		type: "post",
		data:{ filepath:filepath, uploadfilepath:uploadfilepath, x:x, y:y, w:w, h:h },
		datatype: 'json',
		success: function(data){
			if(data == 1)
			{
				$('#msg-rev').html('<div class="alert-success text-center">Cropped Successfully</div>');
				$('#msg-rev').fadeOut(2000);
				setTimeout(function(){ window.location.href="<?php echo BASE_URL?>ajax/uploadimage.php"; }, 2000);
				parent.location.reload();
			}
		}
	});
}


  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function checkCoords()
  {
    if (parseInt($('#w').val())) return true;
    alert('Please select a crop region then press submit.');
    return false;
  };
</script>
<?php
}
else
{
?>
	<?php if(trim($error) != "") { echo $error; }?>
    <?php if(isset($_SESSION['error4']) || trim($_SESSION['error4']) != ""){ echo $_SESSION['error4']; unset($_SESSION['error4']); } ?>
    <div  class="box-photo" align="center">
			<form action="" name="propic" id="propic" method="post" enctype="multipart/form-data">
            	<input type="file" name="upld" id="upld" style="display:none" onchange="$('#propic').submit();" />
            </form>
			<div class="logochange" style="font-family: "Conv_HelveticaNeueLTStd-Thin",sans-serif !important"><a href="javascript:;" onclick="uploadpic()">Upload New Picture</a></div>
        <p class="help-block" style="font-family: "Conv_HelveticaNeueLTStd-Thin",sans-serif !important">Please upload image 800X400.</p>
        <p class="ldr" style="display:none;margin-top: 150px;"><img src="../images/loader.gif" /></p>
    </div>
    <div class="clear"></div><br />
    <script>
	function uploadpic()
	{
		$("#upld").trigger('click');
		$(".ldr").show();
	}
	</script>
<?php
}
?>