<?php
/*
V5.19  23-Apr-2014  (c) 2000-2014 John Lim (jlim#natsoft.com). All rights reserved.
  Released under both BSD license and Lesser GPL library license.
  Whenever there is any discrepancy between the two licenses,
  the BSD license will take precedence.
  Set tabs to 4 for best viewing.

  Latest version is available at http://adodb.sourceforge.net
*/

error_reporting(E_ALL);


include_once('../adodb.inc.php');
include_once('../adodb-pager.inc.php');

$driver = '';
$sql = 'select  id from tbl_users  order  by  id';
//$sql = 'select count(*),firstname from adoxyz group by firstname order by 2 ';
//$sql = 'select distinct firstname, lastname from adoxyz  order  by  firstname';

if ($driver == 'mssql') {
	$db = NewADOConnection('mssql');
	$db->Connect('JAGUAR\vsdotnet','adodb','natsoft','northwind');
}

if (empty($driver) or $driver == 'mysql') {
	$db = NewADOConnection('mysqli');
	$db->Connect('localhost','root','','trip_daddy');
}

//$db->pageExecuteCountRows = false;

$db->debug = true;

if (0) {
$rs = $db->Execute($sql);
//include_once('../toexport.inc.php');
//print "<pre>";
//print rs2csv($rs); # return a string

print '<hr />';
//$rs->MoveFirst(); # note, some databases do not support MoveFirst
//print rs2tab($rs); # return a string

print '<hr />';
//$rs->MoveFirst();
//rs2tabout($rs); # send to stdout directly
print "</pre>";
}

$pager = new ADODB_Pager($db,$sql);
$pager->showPageLinks = true;
$pager->linksPerPage = 3;
//$pager->cache = 60;
$pager->Render($rows=2);
