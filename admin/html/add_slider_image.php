<script>
$( document ).ready(function() {
 setActiveMenu("#homec","#ulhomec","#addslid","","");
});
</script>
<!-- Form Start -->       
<div class="col-md-10"> 
    <div class="row">
		<div id="errorrow"><?php if(isset($_SESSION['error']) || trim($_SESSION['error']) != ""){ echo $_SESSION['error']; unset($_SESSION['error']); } ?><?php if(isset($error) || trim($error) != ""){ echo $error; } ?></div>
	</div>
  <div class="panel panel-default">
  <div class="panel-heading"><h4><?php echo $pagetype." Theme";?></h4></div>
  <div class="panel-body">
	<form role="form" name="userfrm" action="" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="slider_image">Slider Image :</label>
          <input type="file" class="form-control" id="slider_image" name="slider_image" value="<?php echo $slider_image;?>" placeholder="Upload Slider image" />
          [Please upload image in ratio of 900X450 (2:1)]
          <br/>&nbsp;<br/>
          <input type="text" class="form-control" name="link_url" placeholder="http://tripdaddy.co" value="<?php echo $edit_inf[0]['link_url']; ?>" /> 
        </div>
        <input type="submit" class="btn btn-default" name="submit" value="Submit" />
      </form>
    </div>
  </div>
</div>
<!-- Form End -->