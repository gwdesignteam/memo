<script>
$( document ).ready(function() {
 setActiveMenu("#homec","#ulhomec","#mngslid","","");
});
</script>
<!-- Form Start -->
<div class="col-md-10">
	<div class="row">
		<div id="errorrow"><?php if(isset($_SESSION['error']) || trim($_SESSION['error']) != ""){ echo $_SESSION['error']; unset($_SESSION['error']); } ?></div>
	</div>
    <div class="panel panel-default">
        <div class="panel-heading"><h4>Manage Slider</h4></div>
        <div class="panel-body">
        	<!--<form class="form-search">
            <div class="pull-left">
                Filter By :
                <select class="hinput">
                    <option>All</option>
                    <option>Active</option>
                    <option>Inactive</option>
                </select>
            </div>
            <div class="pull-right">
                    <input type="text" class="input-medium search-query">
                    <button type="submit" class="btn">Search</button>
            </div>
            </form>-->
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Slider Image</th>
                        <th> Link Url</th>
                        <th>Date Uploaded</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if(count($selslider) > 0)
				{
					foreach($selslider as $rows)
					{
						$sliderid = $rows['id'];
						$slider_image = $rows['slider_image'];
						$sliderdate = $rows['modified_date'];
						$status = $rows['status'];
						$acttype = "'slider'";
						if($status == 0)
						{
							$statclass = '<span class="glyphicon glyphicon-remove-circle globalmargin" aria-hidden="true" title="Inactive" onclick="changeTheStat('.$sliderid.',1,'.$acttype.')"></span>';
						}
						else
						{
							$statclass = '<span class="glyphicon glyphicon-ok-circle globalmargin" aria-hidden="true" title="Active" onclick="changeTheStat('.$sliderid.',0,'.$acttype.')"></span>';
						}
						?>
						<tr id="tr_<?php echo $sliderid;?>">
                            <td><img src="<?php echo BASE_URL."slider/thumbs/".$slider_image;?>" height="50" width="100" /></td>
                            <td><?php echo $rows['link_url']; ?></td>
                            <td><?php echo $sliderdate;?></td>
                            <td>
                                <a href="<?php echo BASE_ADM_URL;?>index.php?pagename=add_slider_image&sliderid=<?php echo $sliderid;?>"><span class="glyphicon glyphicon-edit globalmargin" aria-hidden="true" title="Edit"></span></a>
                            	<a href="javascript:;" id="stat_<?php echo $sliderid;?>"><?php echo $statclass;?></a>
                            	<a href="javascript:;" id="del_<?php echo $sliderid;?>"><span class="glyphicon glyphicon-trash globalmargin" aria-hidden="true" onclick="delThis(<?php echo $sliderid;?>,'slider')" title="Delete"></span></a>
                            </td>
                        </tr>
						<?php
					}
				}
				?>
                </tbody>
            </table>
        <!-- Form End -->
            <!-- Pagination Start -->
            <nav><?php //echo $inqpagi;?></nav>
            <!-- Pagination End -->
        </div>
    </div>
</div>