<script>
$( document ).ready(function() {
 setActiveMenu("#settings","#ulsettings","#chpwd","","");
});
</script>
<!-- Form Start -->       
<div class="col-md-10"> 
    <div class="row">
		<div id="errorrow"><?php if(isset($_SESSION['error']) || trim($_SESSION['error']) != ""){ echo $_SESSION['error']; unset($_SESSION['error']); } ?><?php if(isset($error) || trim($error) != ""){ echo $error; } ?></div>
	</div>
  <div class="panel panel-default">
  <div class="panel-heading"><h4>Contact Info</h4></div>
  <div class="panel-body">
	<form method="post" action="" id="passwordForm">
		
		<div class="col-md-8 col-md-offset-2">
		<label>Old Password:</label>
		<div style="clear:both;  margin-bottom:10px"></div>
		<input type="password" class="input-lg form-control" name="old_password"  placeholder="Old Password" autocomplete="off" required>		
		</div>
		<div style="clear:both;  margin-bottom:20px"></div>
		<div class="col-md-8 col-md-offset-2">
		<input type="password" class="input-lg form-control" name="password1" id="password1" placeholder="New Password" autocomplete="off" required>		
		<div class="col-sm-6">
		<span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> 8 Characters Long<br>
		<span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> One Uppercase Letter
		</div>
		<div class="col-sm-6">
		<span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> One Lowercase Letter<br>
		<span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> One Number
		</div>
		</div>
		<div style="clear:both;  margin-bottom:20px"></div>
		<div class="col-md-8 col-md-offset-2">
		<input type="password" class="input-lg form-control" name="password2" id="password2" placeholder="Repeat Password" autocomplete="off" required>		
		<div class="col-sm-12">
		<span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Passwords Match
		</div>
		</div>
		<div style="clear:both;  margin-bottom:20px"></div>
		<div class="col-md-8 col-md-offset-2">
		<input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Changing Password..." value="Change Password">
		</div>
	</form>
          <form method="post" action="" id="passwordForm">
              <div style="clear:both;  margin-bottom:20px"></div>
            <div class="col-md-8 col-md-offset-2">
		<label>Change Email:</label>
		<div style="clear:both;  margin-bottom:10px"></div>
                <input type="email" class="input-lg form-control" name="email" value="<?php echo $admin_details[0]['email']; ?>"   autocomplete="off" required>		
            </div>
              <div style="clear:both;  margin-bottom:20px"></div>
              <div class="col-md-8 col-md-offset-2">
		<input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Change Email Id" value="Change EmailId">
		</div>
        </form>
    </div>
  </div>
</div>
<!-- Form End -->
<script>
$("input[type=password]").keyup(function(){
    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");
	
	if($("#password1").val().length >= 8){
		$("#8char").removeClass("glyphicon-remove");
		$("#8char").addClass("glyphicon-ok");
		$("#8char").css("color","#00A41E");
	}else{
		$("#8char").removeClass("glyphicon-ok");
		$("#8char").addClass("glyphicon-remove");
		$("#8char").css("color","#FF0004");
	}
	
	if(ucase.test($("#password1").val())){
		$("#ucase").removeClass("glyphicon-remove");
		$("#ucase").addClass("glyphicon-ok");
		$("#ucase").css("color","#00A41E");
	}else{
		$("#ucase").removeClass("glyphicon-ok");
		$("#ucase").addClass("glyphicon-remove");
		$("#ucase").css("color","#FF0004");
	}
	
	if(lcase.test($("#password1").val())){
		$("#lcase").removeClass("glyphicon-remove");
		$("#lcase").addClass("glyphicon-ok");
		$("#lcase").css("color","#00A41E");
	}else{
		$("#lcase").removeClass("glyphicon-ok");
		$("#lcase").addClass("glyphicon-remove");
		$("#lcase").css("color","#FF0004");
	}
	
	if(num.test($("#password1").val())){
		$("#num").removeClass("glyphicon-remove");
		$("#num").addClass("glyphicon-ok");
		$("#num").css("color","#00A41E");
	}else{
		$("#num").removeClass("glyphicon-ok");
		$("#num").addClass("glyphicon-remove");
		$("#num").css("color","#FF0004");
	}
	
	if($("#password1").val() == $("#password2").val()){
		$("#pwmatch").removeClass("glyphicon-remove");
		$("#pwmatch").addClass("glyphicon-ok");
		$("#pwmatch").css("color","#00A41E");
	}else{
		$("#pwmatch").removeClass("glyphicon-ok");
		$("#pwmatch").addClass("glyphicon-remove");
		$("#pwmatch").css("color","#FF0004");
	}
});
</script>