<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>tripdaddy :: Admin</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-switch.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/highlight.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery-latest.min.js"></script>
    <script src="js/script.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/functions.js"></script>
  </head>
  <body>
  <!-- Header --> 
  <?php
	if(isset($admsessuid) && trim($admsessuid) != "")
	{
	?>
    <div class="container-fluid">
        <div class="panel panel-warning fbox main">
            <div class="panel-body">
                <header>
                    <p class="pull-left hleft"><img src="../images/logo.png"></p>
                    <div class="pull-right topmargin">
                        <ul class="list-inline">
                            <li><a href="#" class="non-curser btn btn-danger btn-sm"><span class="glyphicon glyphicon-user"></span> Admin</a></li>
                            <li><a href="<?php echo BASE_ADM_URL;?>index.php?pagename=logout" class="btn btn-success btn-sm hright"><span class="glyphicon glyphicon-off"></span> Log Out</a></li>
                        </ul>
                    </div>
                </header>
            </div>
        </div>
<!-- Header End -->
<div class="container-fluid">
<div class="panel panel-warning main">
<div class="panel-body">
<?php include_once("left_links.php");
	}
	else if($global_req['pagename'] != "login")
	{
		$path = BASE_ADM_URL."index.php?pagename=login";
		header("Location:".$path);
	}
?>