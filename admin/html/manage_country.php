<script>
$( document ).ready(function() {
 setActiveMenu("#package","#ulpack","#mngcntr","","");
});
</script>
<!-- Form Start -->
<div class="col-md-10">
	<div class="row">
		<div id="errorrow"><?php if(isset($_SESSION['error']) || trim($_SESSION['error']) != ""){ echo $_SESSION['error']; unset($_SESSION['error']); } ?></div>
	</div>
    <div class="panel panel-default">
        <div class="panel-heading"><h4>Manage Country</h4></div>
        <div class="panel-body">
        	<!--<form class="form-search">
            <div class="pull-left">
                Filter By :
                <select class="hinput">
                    <option>All</option>
                    <option>Active</option>
                    <option>Inactive</option>
                </select>
            </div>
            <div class="pull-right">
                    <input type="text" class="input-medium search-query">
                    <button type="submit" class="btn">Search</button>
            </div>
            </form>-->
			<div>
<!--			<div class="add_scroller">-->
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th><a href="<?php echo $path = BASE_ADM_URL."index.php?pagename=manage_country&sort=location&orderby=";($ordbylocation == "ASC")? print("DESC"): print("ASC"); ?>">Country&nbsp;<i class="glyphicon <?php ($ordbylocation == "ASC")? print("glyphicon-chevron-down"): print("glyphicon-chevron-up"); ?> "></i></a></th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if(count($selcntr) > 0)
				{
					foreach($selcntr as $rows)
					{
						$cntrid = $rows['id'];
						$countryid = $rows['country'];
						$country = $rows['location'];
						$status = $rows['status'];
						$acttype = "'cntr'";
						if($status == 0)
						{
							$statclass = '<span class="glyphicon glyphicon-remove-circle globalmargin" aria-hidden="true" title="Inactive" onclick="changeTheStat('.$cntrid.',1,'.$acttype.')"></span>';
						}
						else
						{
							$statclass = '<span class="glyphicon glyphicon-ok-circle globalmargin" aria-hidden="true" title="Active" onclick="changeTheStat('.$cntrid.',0,'.$acttype.')"></span>';
						}
						?>
						<tr>
                            <td><?php echo $country;?></td>
                            <td>
                                <a href="<?php echo BASE_ADM_URL;?>index.php?pagename=add_country&cntrid=<?php echo $cntrid;?>"><span class="glyphicon glyphicon-edit globalmargin" aria-hidden="true" title="Edit"></span></a>
								<a href="javascript:;" id="del_<?php echo $cntrid;?>"><span class="glyphicon glyphicon-trash globalmargin" aria-hidden="true" onclick="delThis(<?php echo $cntrid;?>,'country')" title="Delete"></span></a>
                                <!--<a href="javascript:;" id="stat_<?php echo $cntrid;?>"><?php echo $statclass;?></a>-->
                                <!--<a href="javascript:;" id="blk_<?php //echo $userid;?>"><?php //echo $blkclass;?></a>
                                <a href="http://dev.geekyworks.com/designer/adminw/umview.html"><span class="glyphicon glyphicon-eye-open globalmargin" aria-hidden="true" title="View"></span></a>
                                <a href=""><span class="glyphicon glyphicon-trash globalmargin" aria-hidden="true" title="Delete"></span></a>
                                <a href=""><span class="glyphicon glyphicon-star globalmargin" aria-hidden="true" title="Active"></span></a>
                                <a href="http://dev.geekyworks.com/designer/adminw/ummsg.html"><span class="glyphicon glyphicon-envelope globalmargin" aria-hidden="true" title="Message"></span></a>
                                <a href=""><span class="glyphicon glyphicon-edit globalmargin" aria-hidden="true" title="Create Request"></span></a>-->
                            </td>
                        </tr>
						<?php
					}
				}
				?>
                </tbody>
            </table>
			</div>
        <!-- Form End -->
            <!-- Pagination Start -->
            <nav><?php echo $cntrpagi;?></nav>
            <!-- Pagination End -->
        </div>
    </div>
</div>