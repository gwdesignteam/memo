<div class="col-md-2">
	<div id='cssmenu'>
    <ul>
        
        <li class="active has-sub" id="package"><a href="#">Options</a>
        	<ul id="ulpack">
            	<li id="addcntr"><a href="<?php echo $path = BASE_ADM_URL."index.php?pagename=add_country";?>">Add Country</a></li>
                <li id="mngcntr"><a href="<?php echo $path = BASE_ADM_URL."index.php?pagename=manage_country";?>">Manage Country</a></li>
            </ul>
        </li>
        <li class="active has-sub" id="homec"><a href="#">Home Content</a>
                <li id="addslid"><a href="<?php echo $path = BASE_ADM_URL."index.php?pagename=add_slider_image";?>">Add Slider Image</a></li>
                <li id="mngslid"><a href="<?php echo $path = BASE_ADM_URL."index.php?pagename=manage_slider";?>">Manage Slider</a></li>
            </ul>
        </li>
	<li class="active has-sub" id="settings"><a href="#">Settings</a>
		<ul id="ulsettings">
		<li class="active" id="cntinf"><a href="<?php echo $path = BASE_ADM_URL."index.php?pagename=contact_info";?>">Contact Info</a></li>
		<li class="active" id="sitmaint"><a href="<?php echo $path = BASE_ADM_URL."index.php?pagename=site_maintenance";?>">Site Maintenance</a></li>
		<li class="active" id="chpwd"><a href="<?php echo $path = BASE_ADM_URL."index.php?pagename=admin_change_pwd";?>">Change Admin Pwd</a></li>
		</ul>
	</li>
    </ul>
    </div>
</div>
<script>
function setActiveMenu(level1,ul1,level2,ul2,level3)
{
	if(level1 != "")
	{
		$(level1).addClass("open");
	}
	if(ul1 != "")
	{
		$(ul1).show();
	}
	if(level2 != "")
	{
		$(level2).addClass("open");
	}
	if(ul2 != "")
	{
		$(ul2).show();
	}
	if(level3 != "")
	{
		$(level3).addClass("active");
	}
}
</script>
