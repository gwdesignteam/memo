<script>
$( document ).ready(function() {
 setActiveMenu("#settings","#ulsettings","#cntinf","","");
});
</script>
<!-- Form Start -->       
<div class="col-md-10"> 
    <div class="row">
		<div id="errorrow"><?php if(isset($_SESSION['error']) || trim($_SESSION['error']) != ""){ echo $_SESSION['error']; unset($_SESSION['error']); } ?><?php if(isset($error) || trim($error) != ""){ echo $error; } ?></div>
	</div>
  <div class="panel panel-default">
  <div class="panel-heading"><h4>Contact Info</h4></div>
  <div class="panel-body">
	<form role="form" name="userfrm" action="" method="post">
        <div class="form-group">
          <label for="cemail">Email :</label>
          <input type="email" class="form-control" id="cemail" name="cemail" value="<?php echo $cemail;?>" placeholder="Enter contact email" required />
        </div>
        <div class="form-group">
          <label for="cname">Name :</label>
          <input type="text" class="form-control" id="cname" name="cname" value="<?php echo $cname;?>" placeholder="Enter contact name" required />
        </div>
        <div class="form-group">
          <label for="cnumber">Number :</label>
          <input type="tel" pattern="[0-9\s,+]+"  class="form-control" id="cnumber" name="cnumber" value="<?php echo $cnumber;?>" placeholder="Enter contact number" required />
        </div>
        <div class="form-group">
          <label for="caddress">Address :</label>
          <textarea class="form-control" id="caddress" name="caddress" placeholder="Enter contact address"><?php echo $caddress;?></textarea>
        </div>        
        <input type="submit" class="btn btn-default" name="submit" value="Submit" />
      </form>
    </div>
  </div>
</div>
<!-- Form End -->