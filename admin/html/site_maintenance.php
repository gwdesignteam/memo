<script>
$( document ).ready(function() {
 setActiveMenu("#settings","#ulsettings","#sitmaint","","");
});
</script>
<!-- Form Start -->       
<div class="col-md-10"> 
    <div class="row">
		<div id="errorrow"><?php if(isset($_SESSION['error']) || trim($_SESSION['error']) != ""){ echo $_SESSION['error']; unset($_SESSION['error']); } ?><?php if(isset($error) || trim($error) != ""){ echo $error; } ?></div>
	</div>
  <div class="panel panel-default">
  <div class="panel-heading"><h4>Website Maintenance</h4></div>
  <div class="panel-body">
	<form role="form" name="userfrm" action="" method="post">
        <div class="form-group">          
		  <label for="mstate">Set Maintenance State: &nbsp;  </label>
		  <input id="switch-state" name="mstate" value="1" type="checkbox" <?php ($chked == 1) ? print("checked") : "" ?>>
        </div>          
        <input type="submit" class="btn btn-default" name="submit" value="Submit" />
      </form>
    </div>
  </div>
</div>
<script src="js/bootstrap-switch.js"></script>
<script src="js/highlight.js"></script>
<script src="js/main.js"></script>
<!-- Form End -->

<!--<div class="col-sm-6 col-lg-4">
            <h2 class="h4">State</h2>
            <p>
              <input id="switch-state" type="checkbox" checked>
            </p>
            <div class="btn-group">
              <button type="button" data-switch-toggle="state" class="btn btn-default">Toggle</button>
              <button type="button" data-switch-set="state" data-switch-value="true" class="btn btn-default">Set true</button>
              <button type="button" data-switch-set="state" data-switch-value="false" class="btn btn-default">Set false</button>
              <button type="button" data-switch-get="state" class="btn btn-default">Get</button>
            </div>
          </div>-->
