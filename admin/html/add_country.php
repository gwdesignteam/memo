<script>
$( document ).ready(function() {
 setActiveMenu("#package","#ulpack","#addcntr","","");
});
</script>
<!-- Form Start -->       
<div class="col-md-10"> 
    <div class="row">
		<div id="errorrow"><?php if(isset($_SESSION['error']) || trim($_SESSION['error']) != ""){ echo $_SESSION['error']; unset($_SESSION['error']); } ?><?php if(isset($error) || trim($error) != ""){ echo $error; } ?></div>
	</div>
  <div class="panel panel-default">
  <div class="panel-heading"><h4><?php echo $pagetype." Country";?></h4></div>
  <div class="panel-body">
	<form role="form" name="userfrm" action="" method="post">
        <div class="form-group">
          <label for="country">Country :</label>
          <input type="text" class="form-control" id="country" name="country" value="<?php echo $country;?>" placeholder="Enter country" required="true">
        </div>
        <input type="submit" class="btn btn-default" name="submit" value="Submit" />
      </form>
    </div>
  </div>
</div>
<!-- Form End -->