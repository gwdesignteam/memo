<center><img class="loginlogo" src="../images/logo.png"></center>
<div class="container-fluid">
    <div class="panel panel-warning main2">
        <div class="panel-body">
            <?php
	 if((isset($global_req['forgothash']) && trim($global_req['forgothash']) != "") || $forgoterror == 1)
	{
	?>
            <form role="form" name="pwdreset" action="" method="post">
        <?php echo $error;?>
        <h4 class="form-signin-heading">Forgot Password?</h4>
        <div class="form-group">
            <label  for="inputPassword">Password</label>
            <input type="password" required="" placeholder="Password" class="form-control" id="uinputPassword" name="uinputPassword" />
        </div>
        <div class="form-group">
            <label for="inputPassword">Confirm Password</label>
            <input type="password" required="" placeholder="Confirm Password" class="form-control" id="cinputPassword" name="cinputPassword" />
        </div>
		<br />
        <button type="submit" name="submit" value="Forgot1" class="btn btn-lg btn-primary btn-block">Submit</button>
    </form>
    <?php
	}
	else
	{
	?>
            <form action="" name="loginfrm" method="post">
            <?php echo $error;?>
            <div class="form-group">
                <label for="usr">Username:</label>
                <input type="text" class="form-control" id="username" name="admusername" required="required">
            </div>
            <div class="form-group">
                <label for="usr">Password:</label>
                <input type="password" class="form-control" id="userpwd" name="admuserpwd" required="required">
            </div>
            <center><button type="submit" class="btn btn-danger buttonwidth" name="submit" value="Login">Login</button></center>
            <br/>            
            </form>
            <form id="forgetpwd" action="" name="forgetpwd" method="post">
                <input type="hidden" name="forget" value="pwd"/>
                <span><a href="javascript:{}" onclick="document.getElementById('forgetpwd').submit(); return false;">Forget Password</a></span>
            
            </form>
        <?php } ?>
        </div>
    </div>
</div>