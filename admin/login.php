<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>tripdaddy :: Log In</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <center><img class="loginlogo" src="../images/logo.png"></center>
    <!-- Header --> 
    <div class="container-fluid">
    	<div class="panel panel-warning main2">
    		<div class="panel-body">
            	<form action="login.php" name="loginfrm" method="post">
    			<div class="form-group">
                    <label for="usr">Username:</label>
                    <input type="text" class="form-control" id="usr">
    			</div>
                <div class="form-group">
                    <label for="usr">Password:</label>
                    <input type="text" class="form-control" id="usr">
                </div>
    			<center><button type="submit" class="btn btn-danger buttonwidth">Log In</button></center>
                </form>
    		</div>
    	</div>
    </div>
    <!-- Header End -->
    <div class="clearfix visible-lg"></div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery-latest.min"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
  </body>
</html>