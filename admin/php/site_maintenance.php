<?php
//Select data from  database
$sql = $queries[1050];
$selcntr = $conn->selectSQL($sql,array());
$chked = $selcntr[0]['maintenance'];

if(isset($_POST['submit'])) // update maintenance status  in database 
{
	$status = ($_POST['mstate'] != "") ? $_POST['mstate'] : 0;
	//print_r($_POST);
	$ins_qry =  $queries[1049];
	$ins = $conn->executePrepared($ins_qry, array($status,'1')); 	
	$status = ($status == 1) ? " & Site is under Maintenance " : " & Site is Live";
	$_SESSION['error'] = '<div role="alert" class="alert alert-success">Maintenance status  has been updated successfully, '.$status.'</div>';
	
	if(!$ins)
	{
		$_SESSION['error'] = '<div role="alert" class="alert alert-danger">Something went wrong, please try again later.</div>';
	}
	
	//Select data from  database
	$sql = $queries[1050];
	$selcntr = $conn->selectSQL($sql,array());
	$chked = $selcntr[0]['maintenance'];
}
	
?>