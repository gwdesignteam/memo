<?php
$error = "";
$country = "";
$pagetype = "Add";
if(isset($global_req['submit']) && $global_req['submit'] == "Submit")
{
	if(!isset($global_req['country']) || empty($global_req['country']))
	{
		$error = '<div role="alert" class="alert alert-danger">Please insert Country.</div>';
	}
	$country = $global_req['country'];
	if(trim($error) == "")
	{
		if(isset($global_req['cntrid']) && trim($global_req['cntrid']) != "")
		{
			$ins_qry = $queries[1007];
			$ins = $conn->executePrepared($ins_qry, array(0,"$country","$admsessuid",$global_req['cntrid']));
			$_SESSION['error'] = '<div role="alert" class="alert alert-success">Country saved successfully.</div>';
		}
		else
		{
			$ins_qry = $queries[1009];
			$ins = $conn->executePrepared($ins_qry, array(0,"$country","$admsessuid"));
			$_SESSION['error'] = '<div role="alert" class="alert alert-success">Country saved successfully.</div>';
			$path = BASE_ADM_URL."index.php?pagename=manage_country";
			header("Location:".$path);
			die;
		}
		if(!$ins)
		{
			$_SESSION['error'] = '<div role="alert" class="alert alert-danger">Something went wrong, please try again later.</div>';
		}
	}
}
if(isset($global_req['cntrid']) && trim($global_req['cntrid']) != "")
{
	$pagetype = "Edit";
	$cntrid = $global_req['cntrid'];
	$dsql = $queries[1010];
	$selusers = $conn->selectSQL($dsql, array($cntrid));
	$country = $selusers[0]['location'];
}
?>