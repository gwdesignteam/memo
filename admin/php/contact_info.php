<?php
$error = "";
$cemail = "";
$cname = "";
$cnumber = "";
$caddress = "";
if(isset($global_req['submit']) && $global_req['submit'] == "Submit")
{
	$cemail = $global_req['cemail'];
	$cname = $global_req['cname'];
	$cnumber = $global_req['cnumber'];
	$caddress = $global_req['caddress'];

	$ins_qry = $queries[1027];
	$ins = $conn->executePrepared($ins_qry, array("$cemail","$cname","$cnumber","$caddress"));
	$_SESSION['error'] = '<div role="alert" class="alert alert-success">Contact info saved successfully.</div>';
	if(!$ins)
	{
		$_SESSION['error'] = '<div role="alert" class="alert alert-danger">Something went wrong, please try again later.</div>';
	}
}
	$cisql = $queries[1026];
	$selci = $conn->selectSQL($cisql, array());
	$cemail = $selci[0]['contact_email'];
	$cname = $selci[0]['contact_name'];
	$cnumber = $selci[0]['contact_number'];
	$caddress = $selci[0]['address'];
?>