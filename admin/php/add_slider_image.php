<?php
$error = "";
$theme = "";
$pagetype = "Add";
$sliderid = $global_req['sliderid'];
if(isset($sliderid)){
    //$sql = $queries[1035];
    $sql = 'SELECT * FROM tbl_slider WHERE id=?';
    $edit_inf = $conn->selectSQL($sql,array($sliderid));
    //print_r($edit_inf);
}
if(isset($global_req['submit']) && $global_req['submit'] == "Submit")
{       $link_url = $global_req['link_url'];
      
	if(empty($_FILES['slider_image']['name']) && empty($sliderid))
	{
		$error = '<div role="alert" class="alert alert-danger">Please Upload image.</div>';
               
	}
	$slider_image = $_FILES['slider_image'];
        
        if(isset ($sliderid) && isset ($link_url) && empty($_FILES['slider_image']['name'])) {
            $updat_qry = $queries[1070];
            $update = $conn->executePrepared($updat_qry, array($link_url,$sliderid));
            $_SESSION['error'] = $error = '<div role="alert" class="alert alert-danger">Data Updated Sucessfully.</div>';
            $path = BASE_ADM_URL."index.php?pagename=manage_slider";
            header("Location:".$path);
            exit();
        }
        
	if(trim($error) == "")
	{
		if(isset($_FILES['slider_image']))
		{
			$max_file_size = 1024*50000; // 2Mb
			$valid_exts = array('jpeg', 'jpg', 'png', 'gif');
			// thumbnail sizes
			$sizes = array(100 => 50);
			//if( $_FILES['slider_image']['size'] < $max_file_size ){
				// get file extension
				$ext = strtolower(pathinfo($_FILES['slider_image']['name'], PATHINFO_EXTENSION));
				if (in_array($ext, $valid_exts)) {
					/* resize image */
					foreach ($sizes as $w => $h) {
						$uplFilename = $funObj->resize($w, $h,BASE_PATH."slider/thumbs",'slider_image');
						//if(trim($userpic) != "") { $userpicchk = BASE_PATH."slider/".$userpic; unlink($userpicchk); }
						$userpic = $uplFilename;
					}
		
				} else {
					$_SESSION['error'] = '<div role="alert" class="alert alert-success">Unsupported file</div>';
				}
			//} else{
			//	$_SESSION['error'] = '<div role="alert" class="alert alert-success">Please upload image smaller than 2MB</div>';
			//}

			$sizes1 = array(900 => 450);
			//if( $_FILES['slider_image']['size'] < $max_file_size ){
				// get file extension
				$ext = strtolower(pathinfo($_FILES['slider_image']['name'], PATHINFO_EXTENSION));
				if (in_array($ext, $valid_exts)) {
					/* resize image */
					foreach ($sizes1 as $w => $h) {
						$uplFilename = $funObj->resize($w, $h,BASE_PATH."slider",'slider_image');
						//if(trim($userpic) != "") { $userpicchk = BASE_PATH."slider/".$userpic; unlink($userpicchk); }
						$userpic = $uplFilename;
					}
		
				} else {
					$_SESSION['error'] = '<div role="alert" class="alert alert-success">Unsupported file</div>';
				}
			//} else{
			//	$_SESSION['error'] = '<div role="alert" class="alert alert-success">Please upload image smaller than 2MB</div>';
			//}
		}
                if(empty($sliderid)){
		$ins_qry = $queries[1034];
		$ins = $conn->executePrepared($ins_qry, array("$uplFilename",$link_url,"$admsessuid"));
		$_SESSION['error'] = '<div role="alert" class="alert alert-success">Slider image added successfully.</div>';
                } else if(!empty($sliderid) && !empty($_FILES['slider_image']['name'])){
                $updt_qry = $queries[1098];
		$ins = $conn->executePrepared($updt_qry, array("$uplFilename",$link_url,$sliderid));
		$_SESSION['error'] = '<div role="alert" class="alert alert-success">Slider image Update successfully.</div>';
                }
		if(!$ins)
		{
			$_SESSION['error'] = '<div role="alert" class="alert alert-danger">Something went wrong, please try again later.</div>';
		}
		$path = BASE_ADM_URL."index.php?pagename=manage_slider";
		header("Location:".$path);
		
	}  else {
            $error = '<div role="alert" class="alert alert-danger">Please give proper informatiom.</div>';
        }
}
?>