function changeUstat(uid,stat,verfy,verstat)
{
        var confirmstat = '';
        if(verstat == 0){ 
            confirmstat= confirm("Are you sure, you want to Verify and Active agent?");
        } else {
            confirmstat= confirm("Are you sure, you want to change status?");
        }
	 
	var btn = "#stat_"+uid;
	
	if(confirmstat)
	{
		$.ajax({
			url: "ajax/useractions.php",
			type: "post",
			data:{ stat:stat,verify:verfy, uid:uid, action:'stat' },
			datatype: 'json',
			success: function(data){
				if($.trim(data) == 1){
					if(stat == 1)
					{
						var btnhtml = '<span class="glyphicon glyphicon-ok-circle globalmargin" aria-hidden="true" title="Active" onclick="changeUstat('+uid+',0,1,1)"></span>';
						$(btn).html(btnhtml);
					}
					else
					{
						var btnhtml = '<span class="glyphicon glyphicon-remove-circle globalmargin" aria-hidden="true" title="Inactive" onclick="changeUstat('+uid+',1,1,1)"></span>';
						$(btn).html(btnhtml);
					}
				}
				else
				{
					$("#errorrow").html('Error in chnage status, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
}

function changeTheStat(uid,stat,acttype)
{
	var confirmstat = confirm("Are you sure, you want to change status?");
	var btn = "#stat_"+uid;
	var acttype1 = "'"+acttype+"'";
	if(acttype == "depart")
	{
		btn = "#depart_"+uid;
	}
	
	if(confirmstat)
	{
		$.ajax({
			url: "ajax/allactions.php",
			type: "post",
			data:{ stat:stat, uid:uid, action:'stat', acttype:acttype },
			datatype: 'json',
			success: function(data){
				if($.trim(data) == 1){
					if(stat == 1)
					{
						var btnhtml = '<span class="glyphicon glyphicon-ok-circle globalmargin" aria-hidden="true" title="Active" onclick="changeTheStat('+uid+',0,'+acttype1+')"></span>';
						$(btn).html(btnhtml);
					}
					else
					{
						var btnhtml = '<span class="glyphicon glyphicon-remove-circle globalmargin" aria-hidden="true" title="Inactive" onclick="changeTheStat('+uid+',1,'+acttype1+')"></span>';
						$(btn).html(btnhtml);
					}
				}
				else
				{
					$("#errorrow").html('Error in chnage status, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
}

function changeTheDepart(uid,stat,acttype)
{
	var confirmstat = confirm("Are you sure, you want to change departure?");
	var btn = "#depart_"+uid;
	var acttype1 = "'"+acttype+"'";
	
	if(confirmstat)
	{
		$.ajax({
			url: "ajax/allactions.php",
			type: "post",
			data:{ stat:stat, uid:uid, action:'depart', acttype:acttype },
			datatype: 'json',
			success: function(data){
				if($.trim(data) == 1){
					if(stat == 1)
					{
						var btnhtml = '<span class="glyphicon glyphicon-plane globalmargin" aria-hidden="true" title="Active" onclick="changeTheDepart('+uid+',0,'+acttype1+')"></span>';
						$(btn).html(btnhtml);
					}
					else
					{
						var btnhtml = '<span class="glyphicon glyphicon-remove globalmargin" aria-hidden="true" title="Inactive" onclick="changeTheDepart('+uid+',1,'+acttype1+')"></span>';
						$(btn).html(btnhtml);
					}
				}
				else
				{
					$("#errorrow").html('Error in chnage departure, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
}


function delThis(uid,type)
{
	var confirmstat = confirm("Are you sure, you want to delete this "+type+"?");
	var btn = "#tr_"+uid;
	var act = "#act_"+uid;
	
	if(confirmstat)
	{
		if(type == "user")
		{
			$.ajax({
				url: "ajax/useractions.php",
				type: "post",
				data:{ uid:uid, action:'del',type:type},
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
							$(btn).fadeOut();
					}
					else
					{
						$("#errorrow").html('Error in deletion, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
		}
		if(type == "agent")
		{
			$.ajax({
				url: "ajax/useractions.php",
				type: "post",
				data:{ uid:uid, action:'del',type:type},
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
							$(btn).fadeOut();
					}
					else
					{
						$("#errorrow").html('Error in deletion, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
		}
		if(type == "subscriber")
		{
			$.ajax({
				url: "ajax/allactions.php",
				type: "post",
				data:{ uid:uid, action:'del', acttype:type },
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
							$(btn).fadeOut();
					}
					else
					{
						$("#errorrow").html('Error in deletion, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
		}
		if(type == "slider")
		{
			$.ajax({
				url: "ajax/allactions.php",
				type: "post",
				data:{ uid:uid, action:'del', acttype:type },
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
							$(btn).fadeOut();
					}
					else
					{
						$("#errorrow").html('Error in deletion, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
		}
		if(type == "inclusion")
		{ 
			$.ajax({
				url: "ajax/allactions.php",
				type: "post",
				data:{ uid:uid, action:'del', acttype:type },
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
							//$(btn).fadeOut();
							$(act).fadeOut();
					}
					else
					{
						$("#errorrow").html('Error in deletion, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
			
		}
                if(type == "review")
		{
			$.ajax({
				url: "ajax/allactions.php",
				type: "post",
				data:{ uid:uid, action:'del', acttype:type },
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
							$(btn).fadeOut();
					}
					else
					{
						$("#errorrow").html('Error in deletion, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
		}
                if(type == "country")
		{   
                    var second_conf = confirm("This delete can affect on Cities,Attraction,Packages,Inclusion; Please double check! before go conform");
                    if(second_conf){                    
                    
			$.ajax({
				url: "ajax/allactions.php",
				type: "post",
				data:{ uid:uid, action:'del', acttype:type },
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
							$(btn).fadeOut();
					}
					else
					{
						$("#errorrow").html('Error in deletion, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
                    }
		}
                if(type == "inquiri")
		{   
                                       
                    
			$.ajax({
				url: "ajax/allactions.php",
				type: "post",
				data:{ inqid:uid, action:'del', acttype:type },
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
							$(btn).fadeOut();
					}
					else
					{
						$("#errorrow").html('Error in deletion, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
                    
		}
                
	}
}

function changeUblk(uid,blk)
{
	var btn = "#blk_"+uid;
	$.ajax({
		url: "ajax/useractions.php",
		type: "post",
		data:{ blk:blk, uid:uid, action:'blk' },
		datatype: 'json',
		success: function(data){
			if($.trim(data) == 1){
				if(blk == 0)
				{
					var btnhtml = '<span class="glyphicon glyphicon-record globalmargin" aria-hidden="true" title="Unblocked" onclick="changeUblk('+uid+',1)"></span>';
					$(btn).html(btnhtml);
				}
				else
				{
					var btnhtml = '<span class="glyphicon glyphicon-ban-circle globalmargin" aria-hidden="true" title="Blocked" onclick="changeUblk('+uid+',0)"></span>';
					$(btn).html(btnhtml);
				}
			}
			else
			{
				$("#errorrow").html('Error in chnage status, please try later.');
				$("#errorrow").fadeIn();
			}
		}
	});
}

function addagents(currentval,BASE_URL)
{
	var nextval = parseInt(currentval)+1;
	var nextdiv = ".div"+nextval;	
	var currdiv = ".div"+currentval;
	var removebut = "#addagents"+currentval;
	var divcont = '<label for="agentname'+currentval+'"> Agent '+currentval+' :</label><input type="text" class="form-control" id="agentname'+currentval+'" name="agentname[]" value="" placeholder="Enter name" required="true"><span id="cctlist'+currentval+'"></span><input class="country_code form-control" style="width:10%;float:left;" placeholder="+91" type="text" name="agentcountrycode[]" id="country_code'+currentval+'" value="" style="width:10%;float:left;" readonly /><input type="text" class="form-control" id="agentphone'+currentval+'" name="agentphone[]" value="" placeholder="Enter phone" style="width:90%;" required="true"><input type="text" class="form-control" id="agentemail'+currentval+'" name="agentemail[]" value="" placeholder="Enter email" required="true"><input type="button" class="btn btn-default" id="addagents'+nextval+'" name="addagent" value="Add More" onclick="addagents('+nextval+',\''+BASE_URL+'\');" /> <div class="div'+nextval+'"></div>';//<input type="text" class="form-control" id="agentcountry'+currentval+'" name="agentcountry[]" value="" placeholder="Enter country" required="true">
	$(removebut).remove();
	$(currdiv).append(divcont);
	$.ajax({
		url: BASE_URL+"ajax/data.php",
		type: "post",
		data:{ fordata:'countrylist', aid:currentval, adm:1 },
		datatype: 'json',
		success: function(datatxt){
			if($.trim(datatxt) != ''){
				$("#cctlist"+currentval).html(datatxt);
			}
		}
	});
}

function editPackage(packageid)
{
	$.ajax({
		url: "ajax/editPackage.php",
		type: "post",
		data:{ packageid:packageid },
		datatype: 'json',
		success: function(data){
			if($.trim(data) != ""){
				location.href=data;
			}
			else
			{
				$("#errorrow").html('Error in editing package, please try later.');
				$("#errorrow").fadeIn();
			}
		}
	});
}

function delPackage(packageid)
{
	var confirmdel = confirm("Are you sure, you want to delete this package?");
	if(confirmdel)
	{
		$.ajax({
			url: "ajax/delPackage.php",
			type: "post",
			data:{ packageid:packageid },
			datatype: 'json',
			success: function(data){
				if($.trim(data) == 1){
					$("#row"+packageid).fadeOut(3000);
				}
				else
				{
					$("#errorrow").html('Error in deleting package, please try later.');
					$("#errorrow").fadeIn();
				}
			}
		});
	}
}
 
 function approveAgent(uid,type)
{
    var btn = "#tr_"+uid;	
    if(type == "user")
		{
			$.ajax({
				url: "ajax/useractions.php",
				type: "post",
				data:{ uid:uid, action:'verify' },
				datatype: 'json',
				success: function(data){
					if($.trim(data) == 1){
							$(btn).fadeOut();
					}
					else
					{
						$("#errorrow").html('Error in deletion, please try later.');
						$("#errorrow").fadeIn();
					}
				}
			});
		}      
 }
