-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 05, 2015 at 05:48 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `memo_database`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_userdata`(IN `p_user_id` INT(20))
BEGIN
     DELETE from user_info where User_id = p_user_id;
     DELETE from Content_info where User_id = p_user_id;
     END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Content_info`
--

CREATE TABLE IF NOT EXISTS `Content_info` (
  `Content_id` int(255) NOT NULL AUTO_INCREMENT,
  `User_id` int(255) NOT NULL,
  `Text` varchar(500) CHARACTER SET utf8 NOT NULL,
  `Image` varchar(150) CHARACTER SET utf8 NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Flag` int(5) NOT NULL,
  `Fav_flag` int(5) NOT NULL,
  PRIMARY KEY (`Content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `Content_info`
--

INSERT INTO `Content_info` (`Content_id`, `User_id`, `Text`, `Image`, `Date`, `Flag`, `Fav_flag`) VALUES
(11, 4, 'memo!!!!', 'MemoSearch2_1.png', '2015-11-03 10:36:54', 1, 0),
(12, 4, 'memo', 'Memo+app.png', '2015-11-05 06:07:13', 0, 1),
(13, 4, 'Memo!!!', 'images.jpeg', '2015-11-05 08:39:23', 0, 0),
(14, 4, 'MEmo!!', 'untitled-1.jpg', '2015-11-04 10:23:58', 1, 0),
(15, 4, 'Memo....', 'index.jpeg', '2015-11-05 07:04:23', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `User_id` int(255) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Passcode` varchar(200) NOT NULL,
  `User_type` varchar(20) NOT NULL,
  `Session_id` varchar(500) NOT NULL,
  PRIMARY KEY (`User_id`),
  UNIQUE KEY `User_id` (`User_id`),
  UNIQUE KEY `Session_id` (`Session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`User_id`, `Name`, `Email`, `Passcode`, `User_type`, `Session_id`) VALUES
(4, 'shefali', 'shaifali.guogggts209@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Student', '1292931702');

--
-- Triggers `user_info`
--
DROP TRIGGER IF EXISTS `delete_user`;
DELIMITER //
CREATE TRIGGER `delete_user` AFTER DELETE ON `user_info`
 FOR EACH ROW BEGIN
  DELETE FROM Content_info where User_id = OLD.user_id;
 END
//
DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
